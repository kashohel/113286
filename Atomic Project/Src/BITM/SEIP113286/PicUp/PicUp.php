<?php

namespace App\BITM\SEIP113286\PicUp;
class PicUp
{
    public $id = '';
    public $title = '';
    public $type = '';
    public $status = '';

    public function __construct()
    {
        $connect = mysql_connect('localhost', 'root', '') or die ('Unable to connect with DB Server.');
        mysql_select_db('atomicp') or die ('Unable to connect with Database.');
    }

    public function prepare($data = '')
    {
        if (isset($data['id']) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['title']) && !empty($data['title'])) {
            $this->title = $data['title'];
        }
        if (isset($data['type']) && !empty($data['type'])) {
            $this->type = $data['type'];
        }
        if (isset($data['status']) && !empty($data['status'])) {
            $this->status = $data['status'];
        }
    }

    public function store()
    {
        $query = "INSERT INTO `atomicp`.`picup` (`id`, `title`, `type`, `status`, `created_at`)" . "VALUES (NULL, '" . $this->title . "', 'image', 'deactive', '" . date('Y-m-d') . "')";
        if (isset($this->title) && !empty($this->title)) {
            mysql_query($query);
        } else {
            session_start();
            $_SESSION['Message'] = "Please select a Picture.";
        }
        header('location:manage.php');
    }

    public function user($user = '')
    {
        $query = "SELECT * FROM `picup` WHERE (`id` = 1 OR `type` = '".$user."')";
        $run_query = mysql_query($query);
        return mysql_fetch_assoc($run_query);
    }

    public function edit_user($data = '')
    {
        $query = "UPDATE `atomicp`.`picup` SET `title` = '".$this->title."', `status` = '".$this->status."' WHERE `picup`.`id` =".$this->id;
        mysql_query($query);
        header('location:user.php');
    }

    public function manage()
    {
        $allData = array();
        $query = "SELECT * FROM `picup` WHERE (`deleted_at` IS NULL AND `type` = 'image')";
        $run_query = mysql_query($query);
        while ($oneData = mysql_fetch_assoc($run_query)) {
            $allData[] = $oneData;
        }
        return $allData;
    }

    public function index()
    {
        $query = "SELECT * FROM `picup` WHERE (`deleted_at` IS NULL AND `type` = 'image' AND `status` = 'active')";
        $run_query = mysql_query($query);
        $data = mysql_fetch_assoc($run_query);
        return $data;
    }

    public function show($id = '')
    {
        $query = "SELECT * FROM `picup` WHERE `id` =" . $id;
        $run_query = mysql_query($query);
        $data = mysql_fetch_assoc($run_query);
        return $data;
        //header('location:show.php');
    }

    public function update()
    {
        $query = "UPDATE `atomicp`.`picup` SET `title` = '" . $this->title . "' WHERE `picup`.`id` =" . $this->id;
        mysql_query($query);
        header('location:manage.php');
    }

    public function active($id = '')
    {
        $query1 = "UPDATE `atomicp`.`picup` SET `status` = 'deactive' WHERE `type` != 'user'";
        $query2 = "UPDATE `atomicp`.`picup` SET `status` = 'active' WHERE `picup`.`id` =" . $id;
//        echo $query1.";".$query2;
//        die();
        if (mysql_query($query1)) {
            mysql_query($query2);
        }
        header('location:manage.php');

    }

    public function trash($id = '')
    {
        $query = "UPDATE `atomicp`.`picup` SET `deleted_at` = '" . date('Y-m-d') . "' WHERE `picup`.`id` =" . $id;
//        echo $query;
//        die();
        mysql_query($query);
        header('location:manage.php');
    }

    public function trashed()
    {
        $allData = array();
        $query = "SELECT * FROM `picup` WHERE (`deleted_at` IS NOT NULL AND `type` = 'image')";
        $run_query = mysql_query($query);
        while ($oneData = mysql_fetch_assoc($run_query)) {
            $allData[] = $oneData;
        }
        return $allData;
    }

    public function restore($id = '')
    {
        $query = "UPDATE `atomicp`.`picup` SET `deleted_at` = NULL WHERE `picup`.`id` =" . $id;
        mysql_query($query);
        header('location:trashed.php');
    }

    public function delete($data = '')
    {
        $query = "DELETE FROM `atomicp`.`picup` WHERE `picup`.`id` = " . $data['id'];
        mysql_query($query);
        unlink('../../../../assets/img/PicUp/' . $data['title']);
        header('location:trashed.php');
    }

}