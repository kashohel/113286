<?php

namespace App\BITM\SEIP113286\Hobby;
class Hobby
{

    public $id = '';
    public $title = '';
    public $hobby = '';

    public function __construct()
    {
        $connect = mysql_connect('localhost', 'root', '') or die('Unable to connect with DB Server.');
        mysql_select_db('atomicp') or die ('Unable to connect with Database.');
    }

    public function prepare($data = '')
    {

        $this->id = $data['id'];
        $this->title = $data['title'];
        $this->hobby = $data['hobby'];
    }

    public function store()
    {
        session_start();
        if(isset($this->hobby)){
        $query = "INSERT INTO `atomicp`.`hobby` (`id`, `title`, `hobby`, `created_at`)"
        ."VALUES (NULL, '".$this->title."', '".$this->hobby."', '".date('Y-m-d H:i:s')."')";
        mysql_query($query);
            $_SESSION['Message'] = "Data save successfull.";
            header('location:manage.php');
        }else{
            $_SESSION['Message'] = "Plese check to submit data.";
            header('location:create.php');
        }

    }

    public function index()
    {
        $allData = array();
        $query = "SELECT * FROM `hobby` WHERE deleted_at IS NULL";
        $run_query = mysql_query($query);
        while($oneData = mysql_fetch_assoc($run_query))
        {
        $allData[] = $oneData;
        }
        return $allData;
    }

    public function show($id = ''){
        $query = "SELECT * FROM `hobby` WHERE id=".$id;
        $run_query = mysql_query($query);
        $raw = mysql_fetch_assoc($run_query);
        return $raw;

    }

    public function update()
    {
        $query = "UPDATE `atomicp`.`hobby` SET `title` = '".$this->title."', `hobby` = '".$this->hobby."' `updated_at` = '".date('Y-m-d H:i:s')."' WHERE `hobby`.`id` =".$this->id;
        mysql_query($query);
        header('location:index.php');
    }

    public function trashed()
    {
        $allData = array();
        $query = "SELECT * FROM `hobby` WHERE deleted_at IS NOT NULL";
        $run_query = mysql_query($query);
        while($oneData = mysql_fetch_assoc($run_query))
        {
            $allData[] = $oneData;
        }
        return $allData;
    }

    public function trash($id = '')
    {
        $query = "UPDATE `atomicp`.`hobby` SET `deleted_at` = '".date('Y-m-d H:i:s')."' WHERE `hobby`.`id` =".$id;
        mysql_query($query);
        header('location:index.php');
    }

    public function restore($id = '')
    {
        $query = "UPDATE `atomicp`.`hobby` SET `deleted_at` = NULL WHERE `hobby`.`id` =".$id;
        mysql_query($query);
        header('location:trashed.php');
    }

    public function delete($id = '')
    {
        $query = "DELETE FROM `atomicp`.`hobby` WHERE `hobby`.`id` =".$id;
        mysql_query($query);
        header('location:trashed.php');
    }


}