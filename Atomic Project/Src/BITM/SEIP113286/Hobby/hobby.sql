-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2016 at 12:20 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicp`
--

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE IF NOT EXISTS `hobby` (
`id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hobby` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `title`, `hobby`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'C', 'C', '2016-03-27 11:08:13', '2016-03-27 15:23:55', '2016-03-27 11:23:55'),
(2, 'C', 'C', '2016-03-27 11:08:38', '2016-03-27 15:23:57', '2016-03-27 11:23:57'),
(3, '', 'C,C++,Java', '2016-03-27 11:12:15', '0000-00-00 00:00:00', NULL),
(4, 'sdfdsa', 'C', '2016-03-27 11:17:26', '0000-00-00 00:00:00', NULL),
(5, 'Helal', 'C,Java,DotNet', '2016-03-27 11:17:44', '0000-00-00 00:00:00', NULL),
(6, 'gfdshfd', 'Array', '2016-03-27 11:26:23', '0000-00-00 00:00:00', NULL),
(7, 'One', 'Java', '2016-03-27 11:29:27', '0000-00-00 00:00:00', NULL),
(8, 'Two', 'C,C++,Java', '2016-03-27 11:38:19', '0000-00-00 00:00:00', NULL),
(9, 'Three', 'C++,Java,PHP', '2016-03-27 11:38:56', '0000-00-00 00:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
