<?php

namespace App\BITM\SEIP113286\Mobile;

class Mobile {

    public $id = '';
    public $title = '';
    public $deleted_at = '';

    public function __construct() {
        $conn = mysql_connect('localhost', 'root', '') or die('Unable to connect');
        mysql_select_db('atomicp') or die('Unable to connect with DB');
    }

    public function store() {
        session_start();
        if (isset($this->title) && !empty($this->title)) {
            $query = "INSERT INTO `atomicp`.`mobiles` (`id`, `title`, `created_at`) "
                    . "VALUES (NULL, '" . $this->title . "', '".date('Y-m-d H:i:s')."')";
            if (mysql_query($query)) {
                $_SESSION['Message'] = "Yes successfully submited";
            } else {
                $_SESSION['Message'] = "Not submited";
            }

            header('location:create.php');
        } else {
            $_SESSION['Message'] = "Not submited";
            header('location:create.php');
        }
        header('location:index.php');
    }

    public function prepare($data = '') {
        if (isset($data['id']) && !empty($data['id'])) {
            $this->id = $data['id'];
//            echo $this->title;
        }
        if (isset($data['title']) && !empty($data['title'])) {
            $this->title = $data['title'];
//            echo $this->title;
        }
        return $this;
    }

    public function index() {
        $allData = array();
        $query = "SELECT * FROM `mobiles` where `deleted_at` IS NULL";
        $result = mysql_query($query);
//        $onedata = mysql_fetch_assoc($result);
//        return $onedata;
        while ($onedata = mysql_fetch_assoc($result)) {
            $allData[] = $onedata;
        }
        return $allData;
    }

    public function show($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `mobiles` WHERE id=" . $this->id;
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function delete($id = '') {
        session_start();
        $this->id = $id;
        $query = "DELETE FROM `atomicp`.`mobiles` WHERE `mobiles`.`id` =" . $this->id;
        $result = mysql_query($query);
        header('location:index.php');
        $_SESSION['Message'] ="<h4>".  "Successfully Deleted" ."</h4>";
    }

    public function trash() {
        session_start();
        $this->deleted_at = time();
        $query = "UPDATE `atomicp`.`mobiles` SET `deleted_at` = '".date('Y-m-d H:i:s')."' WHERE `mobiles`.`id` =" . $this->id;
        mysql_query($query);
        header('location:index.php');
        $_SESSION['Message'] ="<h4>".  "Successfully Deleted" ."</h4>";
    }


    public function trashed($id = ''){
        $allData = array();
        $query = "SELECT * FROM `mobiles` where `deleted_at` IS NOT NULL";
        $result = mysql_query($query);
//        $onedata = mysql_fetch_assoc($result);
//        return $onedata;
        while ($onedata = mysql_fetch_assoc($result)) {
            $allData[] = $onedata;
        }
        return $allData;
    }

    public function restore(){
        $query = "UPDATE `atomicp`.`mobiles` SET `deleted_at` = NULL WHERE `mobiles`.`id` =".$this->id;
        mysql_query($query);
        header('location:trashed.php');
    }

    public function update(){
        if (isset($this->title) && !empty($this->title)) {
            $query = "UPDATE `atomicp`.`mobiles` SET `title` = '".$this->title."', `updated_at` = '".date('Y-m-d H:i:s')."' WHERE `mobiles`.`id` =".$this->id;

            if (mysql_query($query)) {
                $_SESSION['Message'] = "Yes successfully submited";
            } else {
                $_SESSION['Message'] = "Not submited";
            }

            header('location:create.php');
        } else {
            $_SESSION['Message'] = "Not submited";
            header('location:create.php');
        }
        header('location:index.php');
    }

}

