<?php

namespace App\BITM\SEIP113286\Gender;
class Gender
{

    public $id = '';
    public $title = '';
    public $gender = '';

    public function __construct()
    {
        $connect = mysql_connect('localhost', 'root', '') or die('Unable to connect with DB Server.');
        mysql_select_db('atomicp') or die ('Unable to connect with Database.');
    }

    public function prepare($data = '')
    {
        if (isset($data['id']) && !empty($data['id']) && !empty($data['title']) && !empty($data['gender']))
        {
            $this->id = $data['id'];
        }
        $this->title = $data['title'];
        $this->gender = $data['gender'];
    }

    public function store()
    {
        $query = "INSERT INTO `atomicp`.`gender` (`id`, `title`, `gender`, `created_at`)"
        ."VALUES (NULL, '".$this->title."', '".$this->gender."', '".date('Y-m-d H:i:s')."')";
        mysql_query($query);
        header('location:index.php');
    }

    public function index()
    {
        $allData = array();
        $query = "SELECT * FROM `gender` WHERE deleted_at IS NULL";
        $run_query = mysql_query($query);
        while($oneData = mysql_fetch_assoc($run_query))
        {
        $allData[] = $oneData;
        }
        return $allData;
    }

    public function show($id = ''){
        $query = "SELECT * FROM `gender` WHERE id=".$id;
        $run_query = mysql_query($query);
        $raw = mysql_fetch_assoc($run_query);
        return $raw;

    }

    public function update()
    {
        $query = "UPDATE `atomicp`.`gender` SET `title` = '".$this->title."', `gender` = '".$this->gender."', `updated_at` = '".date('Y-m-d H:i:s')."' WHERE `gender`.`id` =".$this->id;
        mysql_query($query);
        header('location:index.php');
    }

    public function trashed()
    {
        $allData = array();
        $query = "SELECT * FROM `gender` WHERE deleted_at IS NOT NULL";
        $run_query = mysql_query($query);
        while($oneData = mysql_fetch_assoc($run_query))
        {
            $allData[] = $oneData;
        }
        return $allData;
    }

    public function trash($id = '')
    {
        $query = "UPDATE `atomicp`.`gender` SET `deleted_at` = '".date('Y-m-d H:i:s')."' WHERE `gender`.`id` =".$id;
        mysql_query($query);
        header('location:index.php');
    }

    public function restore($id = '')
    {
        $query = "UPDATE `atomicp`.`gender` SET `deleted_at` = NULL WHERE `gender`.`id` =".$id;
        mysql_query($query);
        header('location:trashed.php');
    }

    public function delete($id = '')
    {
        $query = "DELETE FROM `atomicp`.`gender` WHERE `gender`.`id` =".$id;
        mysql_query($query);
        header('location:trashed.php');
    }


}