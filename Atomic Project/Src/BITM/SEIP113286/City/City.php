<?php

namespace App\BITM\SEIP113286\City;
class City
{
    public $id = '';
    public $name = '';
    public $city = '';

    public function __construct()
    {
        $conn = mysql_connect('localhost', 'root', '') or die('Unable to connect');
        mysql_select_db('atomicp') or die('Unable to connect with DB');
    }

    public function prepare($data = '') {
        if (isset($data['id']) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['name']) && !empty($data['name'])) {
            $this->name = $data['name'];
        }
        if (isset($data['city']) && !empty($data['city'])) {
            $this->city = $data['city'];
        }
        return $this;
    }

    public function store()
    {
        $query = "INSERT INTO `atomicp`.`city` (`id`, `name`, `city`, `created_at`)" . "VALUES (NULL, '" . $this->name . "', '" . $this->city . "', '" . date('Y-m-d') . "')";
        mysql_query($query);
        header('location:index.php');
    }


    public function index() {
        $allData = array();
        $query = "SELECT * FROM `city` where `deleted_at` IS NULL";
        $result = mysql_query($query);
        while ($onedata = mysql_fetch_assoc($result)) {
            $allData[] = $onedata;
        }
        return $allData;
    }

    public function show($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `city` WHERE id=" . $this->id;
        $result = mysql_query($query);
        $results = mysql_fetch_assoc($result);
        return $results;
    }

    public function delete($id = '') {
        $query = "DELETE FROM `atomicp`.`city` WHERE `city`.`id` =" . $id;
        $result = mysql_query($query);
        header('location:trashed.php');
    }

    public function trash() {
        $this->deleted_at = time();
        $query = "UPDATE `atomicp`.`city` SET `deleted_at` = '".date('Y-m-d H:i:s')."' WHERE `city`.`id` =" . $this->id;
        mysql_query($query);
        header('location:index.php');
    }


    public function trashed(){
        $allData = array();
        $query = "SELECT * FROM `city` where `deleted_at` IS NOT NULL";
        $result = mysql_query($query);
        while ($onedata = mysql_fetch_assoc($result)) {
            $allData[] = $onedata;
        }
        return $allData;
    }

    public function restore(){
        $query = "UPDATE `atomicp`.`city` SET `deleted_at` = NULL WHERE `city`.`id` =".$this->id;
        mysql_query($query);
        header('location:trashed.php');
    }

    public function update(){
        $query = "UPDATE `atomicp`.`city` SET `name` = '".$this->name."', `city` = '".$this->city."' WHERE `city`.`id` =".$this->id;
        if (mysql_query($query)){
            header('location:index.php');
        } else {
            header('location:index.php');
        }

    }


}