<?php

namespace App\BITM\SEIP113286\Books;
use PDO;

class Books
{

    public $id = '';
    public $title = '';
    public $conn = '';
    public $username = '';
    public $password = '';

    public function __construct()
    {
        try {
            $this->conn = new PDO('mysql:host=localhost; dbname=atomicp', $this->username, $this->password);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDP::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'ERROR:' . $e->getMessage();
        }
    }

    public function store()
    {
        $stmt = "INSERT INTO `atomicp`.`books` (`id`, `title`, `created_at`) "
            . "VALUES (NULL, :id, :title)";
        $q = $this->conn->prepare($stmt);
        $q->execute(array(':id' => $this->id, ':title' => $this->image));
        header('location:index.php');
    }

    public function prepare($data = '')
    {
        if (isset($data['id']) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['title']) && !empty($data['title'])) {
            $this->title = $data['title'];
        }
        return $this;
    }

    public function index()
    {
        $allData = array();
        $query = "SELECT * FROM `books` where `deleted_at` IS NULL";
        $result = mysql_query($query);
        while ($onedata = mysql_fetch_assoc($result)) {
            $allData[] = $onedata;
        }
        return $allData;
    }

    public function show($id = '')
    {
        $this->id = $id;
        $query = "SELECT * FROM `books` WHERE id=" . $this->id;
        $result = mysql_query($query);
        $results = mysql_fetch_assoc($result);
        return $results;
    }

    public function delete($id = '')
    {
        $this->id = $id;
        $query = "DELETE FROM `atomicp`.`books` WHERE `books`.`id` =" . $this->id;
        $result = mysql_query($query);
        header('location:index.php');
    }

    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicp`.`books` SET `deleted_at` = '" . date('Y-m-d H:i:s') . "' WHERE `books`.`id` =" . $this->id;
        mysql_query($query);
        header('location:index.php');
    }


    public function trashed($id = '')
    {
        $allData = array();
        $query = "SELECT * FROM `books` where `deleted_at` IS NOT NULL";
        $result = mysql_query($query);
//        $onedata = mysql_fetch_assoc($result);
//        return $onedata;
        while ($onedata = mysql_fetch_assoc($result)) {
            $allData[] = $onedata;
        }
        return $allData;
    }

    public function restore()
    {
        $query = "UPDATE `atomicp`.`books` SET `deleted_at` = NULL WHERE `books`.`id` =" . $this->id;
        mysql_query($query);
        header('location:trashed.php');
    }

    public function update()
    {
        $this->title;
        $query = "UPDATE `atomicp`.`books` SET `title` = '" . $this->title . "', `updated_at` = '" . date('Y-m-d H:i:s') . "' WHERE `books`.`id` =" . $this->id;
        mysql_query($query);
        header('location:index.php');
    }

}
