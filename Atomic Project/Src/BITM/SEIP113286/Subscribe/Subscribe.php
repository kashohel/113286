<?php

namespace App\BITM\SEIP113286\Subscribe;
class Subscribe
{
    public $id = '';
    public $email = '';
    public $active = '';


    public function __construct()
    {
        $conn = mysql_connect('localhost', 'root', '') or die ('Unable to connect with DB server.');
        mysql_select_db('atomicp') or die ('Unable to connect with Database.');
    }

    public function prepare($data = '')
    {
        if (isset($data['id']) && !empty($data['id']))
        {
            $this->id = $data['id'];
        }
        if (isset($data['email']) && !empty($data['email']))
        {
            $this->email = $data['email'];
        }
        if (isset($data['active']) && !empty($data['active']))
        {
            $this->active = $data['active'];
        }
    }

    public function store()
    {
        if (isset($this->id) && !empty($this->id))
        {
            $query = "UPDATE `atomicp`.`subscribe` SET `email` = '".$this->email."', `active` = '".$this->active."' WHERE `subscribe`.`id` =".$this->id;
        } else {
            $query = "INSERT INTO `atomicp`.`subscribe` (`id`, `email`, `active`, `created_at`) VALUES (NULL, '".$this->email."', '1', '".date('Y-m-d')."')";
        }
        mysql_query($query);
        header('location:Subscribers.php');
    }

    public function index()
    {
        $allData = array();
        $query = "SELECT * FROM `subscribe` WHERE deleted_at IS NULL";
        $run_query = mysql_query($query);
        while ($oneData = mysql_fetch_assoc($run_query))
        {
            $allData[] = $oneData;
        }
        return $allData;
    }

    public function show($id = '')
    {
        $query = "SELECT * FROM `subscribe` WHERE id=".$id;
        $run_query = mysql_query($query);
        $Data = mysql_fetch_assoc($run_query);
        return $Data;
    }

    public function active($data = '')
    {
        $query = "UPDATE `atomicp`.`subscribe` SET `active` = '".$data['active']."' WHERE `subscribe`.`id` =".$data['id'];
        mysql_query($query);
        header('location:Subscribers.php');
    }

    public function trash($id = '')
    {
        $query = "UPDATE `atomicp`.`subscribe` SET `deleted_at` = '".date('Y-m-d')."' WHERE `subscribe`.`id` =".$id;
        mysql_query($query);
        header('location:Subscribers.php');
    }
    public function trashed()
    {
        $allData = array();
        $query = "SELECT * FROM `subscribe` WHERE deleted_at IS NOT NULL";
        $run_query = mysql_query($query);
        while ($oneData = mysql_fetch_assoc($run_query))
        {
            $allData[] = $oneData;
        }
        return $allData;
    }
    public function restore($id = '')
    {
        $query = "UPDATE `atomicp`.`subscribe` SET `deleted_at` = NULL WHERE `subscribe`.`id` =".$id;
        mysql_query($query);
        header('location:trashed.php');
    }
    public function delete($id = '')
    {
        $query = "DELETE FROM `atomicp`.`subscribe` WHERE `subscribe`.`id` =".$id;
        mysql_query($query);
        header('location:trashed.php');
    }

}