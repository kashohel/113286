<?php

namespace App\BITM\SEIP113286\Slider;
class Slider
{
    public $id = '';
    public $title = '';
    public $subtitle = '';
    public $image = '';
    public $active = '';

    public function __construct()
    {
        $conn = mysql_connect('localhost', 'root', '') or die ('Unable to connect with DB server.');
        mysql_select_db('atomicp') or die ('Unable to conect with database.');
    }

    public function prepare($data = '')
    {
        if (isset($data['id']) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['title']) && !empty($data['title'])) {
            $this->title = $data['title'];
        }
        if (isset($data['subtitle']) && !empty($data['subtitle'])) {
            $this->subtitle = $data['subtitle'];
        }
        if (isset($data['image']) && !empty($data['image'])) {
            $this->image = $data['image'];
        }
        if (isset($data['active']) && !empty($data['active'])) {
            $this->active = $data['active'];
        }
    }

    public function store()
    {
        $query = "INSERT INTO `atomicp`.`slider` (`id`, `title`, `subtitle`, `image`, `active`, `created_at`) VALUES (NULL, '".$this->title."', '".$this->subtitle."',  '".$this->image."', 'no', '".date('Y-m-d')."')";
        mysql_query($query);;
        header('location:manage.php');
    }

    public function index()
    {
        $allData = array();
        $query = "SELECT * FROM `slider` WHERE (`deleted_at` IS NULL AND `active` = 'yes')";
        $run_query = mysql_query($query);
        while ($oneData = mysql_fetch_assoc($run_query)) {
            $allData[] = $oneData;
        }
        return $allData;
    }

    public function slider_list()
    {
        $allData = array();
        $query = "SELECT * FROM `slider` WHERE `deleted_at` IS NULL";
        $run_query = mysql_query($query);
        while ($oneData = mysql_fetch_assoc($run_query)) {
            $allData[] = $oneData;
        }
        return $allData;
    }

    public function show($id = '')
    {
        $query = "SELECT * FROM `slider` WHERE id =".$id;
        $run_query = mysql_query($query);
        $result = mysql_fetch_assoc($run_query);
        return $result;

    }

    public function update()
    {
        if (isset($this->image) && !empty($this->image))
        {
            $query = "UPDATE `atomicp`.`slider` SET `title` = '".$this->title."', `subtitle` = '".$this->subtitle."', `image` = '".$this->image."' WHERE `slider`.`id` =".$this->id;
            mysql_query($query);
        } else{
            $query = "UPDATE `atomicp`.`slider` SET `title` = '".$this->title."', `subtitle` = '".$this->subtitle."' WHERE `profilepic`.`id` =".$this->id;
            mysql_query($query);
        }
        header('location:manage.php');
    }

    public function trash($id = '')
    {
        $query = "UPDATE `atomicp`.`slider` SET `deleted_at` = '".date('Y-m-d')."' WHERE `slider`.`id` =".$id;
        mysql_query($query);
        header('location:manage.php');
    }

    public function active($id = '')
    {
        $query = "UPDATE `atomicp`.`slider` SET `active` = 'yes' WHERE `slider`.`id` =".$id;
        mysql_query($query);
        header('location:manage.php');
    }

    public function deactive($id = '')
    {
        $query = "UPDATE `atomicp`.`slider` SET `active` = 'no' WHERE `slider`.`id` =".$id;
        mysql_query($query);
        header('location:manage.php');
    }

    public function trashed()
    {
        $allData = array();
        $query = "SELECT * FROM `slider` WHERE `deleted_at` IS NOT NULL";
        $run_query = mysql_query($query);
        while ($oneData = mysql_fetch_assoc($run_query)) {
            $allData[] = $oneData;
        }
        return $allData;
    }

    public function restore($id = '')
    {
        $query = "UPDATE `atomicp`.`slider` SET `deleted_at` = NULL WHERE `slider`.`id` =".$id;
        mysql_query($query);
        header('location:trashed.php');
    }

    public function delete($data = '')
    {
        $query = "DELETE FROM `atomicp`.`slider` WHERE `slider`.`id`=".$data['id'];
        mysql_query($query);
        unlink('../../../../assets/img/Slider/'.$data['image']);
        header('location:trashed.php');
    }

}
