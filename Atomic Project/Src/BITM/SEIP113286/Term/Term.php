<?php

namespace App\BITM\SEIP113286\Term;
class Term
{

    public $id = '';
    public $title = '';
    public $term = '';

    public function __construct()
    {
        $connect = mysql_connect('localhost', 'root', '') or die('Unable to connect with DB Server.');
        mysql_select_db('atomicp') or die ('Unable to connect with Database.');
    }

    public function prepare($data = '')
    {

        $this->id = $data['id'];
        $this->title = $data['title'];
        $this->term = $data['term'];
    }

    public function store()
    {
        session_start();
        if(isset($this->term)){
        $query = "INSERT INTO `atomicp`.`term` (`id`, `title`, `term`, `created_at`)"
        ."VALUES (NULL, '".$this->title."', '".$this->term."', '".date('Y-m-d H:i:s')."')";
        mysql_query($query);
            $_SESSION['Message'] = "Data save successfull.";
            header('location:index.php');
        }else{
            $_SESSION['Message'] = "Plese check to submit data.";
            header('location:create.php');
        }

    }

    public function index()
    {
        $allData = array();
        $query = "SELECT * FROM `term` WHERE deleted_at IS NULL";
        $run_query = mysql_query($query);
        while($oneData = mysql_fetch_assoc($run_query))
        {
        $allData[] = $oneData;
        }
        return $allData;
    }

    public function show($id = ''){
        $query = "SELECT * FROM `term` WHERE id=".$id;
        $run_query = mysql_query($query);
        $raw = mysql_fetch_assoc($run_query);
        return $raw;

    }

    public function update()
    {
        $query = "UPDATE `atomicp`.`term` SET `title` = '".$this->title."', `term` = '".$this->term."' `updated_at` = '".date('Y-m-d H:i:s')."' WHERE `term`.`id` =".$this->id;
        mysql_query($query);
        header('location:index.php');
    }

    public function trashed()
    {
        $allData = array();
        $query = "SELECT * FROM `term` WHERE deleted_at IS NOT NULL";
        $run_query = mysql_query($query);
        while($oneData = mysql_fetch_assoc($run_query))
        {
            $allData[] = $oneData;
        }
        return $allData;
    }

    public function trash($id = '')
    {
        $query = "UPDATE `atomicp`.`term` SET `deleted_at` = '".date('Y-m-d H:i:s')."' WHERE `term`.`id` =".$id;
        mysql_query($query);
        header('location:index.php');
    }

    public function restore($id = '')
    {
        $query = "UPDATE `atomicp`.`term` SET `deleted_at` = NULL WHERE `term`.`id` =".$id;
        mysql_query($query);
        header('location:trashed.php');
    }

    public function delete($id = '')
    {
        $query = "DELETE FROM `atomicp`.`term` WHERE `term`.`id` =".$id;
        mysql_query($query);
        header('location:trashed.php');
    }


}