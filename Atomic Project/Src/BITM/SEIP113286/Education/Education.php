<?php

namespace App\BITM\SEIP113286\Education;
class Education
{
    public $id = '';
    public $name = '';
    public $education = '';

    public function __construct()
    {
        $conn = mysql_connect('localhost', 'root', '') or die('Unable to connect');
        mysql_select_db('atomicp') or die('Unable to connect with DB');
    }

    public function prepare($data = '') {
        if (isset($data['id']) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['name']) && !empty($data['name'])) {
            $this->name = $data['name'];
        }
        if (isset($data['education']) && !empty($data['education'])) {
            $this->education = $data['education'];
        }
        return $this;
    }

    public function store()
    {
        $query = "INSERT INTO `atomicp`.`education` (`id`, `name`, `education`, `created_at`)" . "VALUES (NULL, '" . $this->name . "', '" . $this->education . "', '" . date('Y-m-d') . "')";
        mysql_query($query);
        header('location:index.php');
    }


    public function index() {
        $allData = array();
        $query = "SELECT * FROM `education` where `deleted_at` IS NULL";
        $result = mysql_query($query);
        while ($onedata = mysql_fetch_assoc($result)) {
            $allData[] = $onedata;
        }
        return $allData;
    }

    public function show($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `education` WHERE id=" . $this->id;
        $result = mysql_query($query);
        $results = mysql_fetch_assoc($result);
        return $results;
    }

    public function delete($id = '') {
        $this->id = $id;
        $query = "DELETE FROM `atomicp`.`education` WHERE `education`.`id` =" . $this->id;
        $result = mysql_query($query);
        header('location:trashed.php');
    }

    public function trash() {
        $this->deleted_at = time();
        $query = "UPDATE `atomicp`.`education` SET `deleted_at` = '".date('Y-m-d H:i:s')."' WHERE `education`.`id` =" . $this->id;
        mysql_query($query);
        header('location:index.php');
    }


    public function trashed(){
        $allData = array();
        $query = "SELECT * FROM `education` where `deleted_at` IS NOT NULL";
        $result = mysql_query($query);
        while ($onedata = mysql_fetch_assoc($result)) {
            $allData[] = $onedata;
        }
        return $allData;
    }

    public function restore(){
        $query = "UPDATE `atomicp`.`education` SET `deleted_at` = NULL WHERE `education`.`id` =".$this->id;
        mysql_query($query);
        header('location:trashed.php');
    }

    public function update(){
        $query = "UPDATE `atomicp`.`education` SET `name` = '".$this->name."', `education` = '".$this->education."' WHERE `education`.`id` =".$this->id;
        if (mysql_query($query)){
            header('location:index.php');
        } else {
            header('location:index.php');
        }

    }


}