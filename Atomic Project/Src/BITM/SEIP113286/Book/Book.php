<?php

namespace App\BITM\SEIP113286\Book;

class Book {

    public $id = '';
    public $title = '';

    public function __construct() {
        $conn = mysql_connect('localhost', 'root', '') or die('Unable to connect');
        mysql_select_db('atomicp') or die('Unable to connect with DB');
    }

    public function store() {
        $this->title;
        $query = "INSERT INTO `atomicp`.`book` (`id`, `title`, `created_at`) "
            . "VALUES (NULL, '" . $this->title . "', '".date('Y-m-d H:i:s')."')";
        mysql_query($query);
        header('location:index.php');
    }

    public function prepare($data = '') {
        if (isset($data['id']) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['title']) && !empty($data['title'])) {
            $this->title = $data['title'];
        }
        return $this;
    }

    public function index() {
        $allData = array();
        $query = "SELECT * FROM `book` where `deleted_at` IS NULL";
        $result = mysql_query($query);
        while ($onedata = mysql_fetch_assoc($result)) {
            $allData[] = $onedata;
        }
        return $allData;
    }

    public function show($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `book` WHERE id=" . $this->id;
        $result = mysql_query($query);
        $results = mysql_fetch_assoc($result);
        return $results;
    }

    public function delete($id = '') {
        $this->id = $id;
        $query = "DELETE FROM `atomicp`.`book` WHERE `book`.`id` =" . $this->id;
        $result = mysql_query($query);
        header('location:index.php');
    }

    public function trash() {
        $this->deleted_at = time();
        $query = "UPDATE `atomicp`.`book` SET `deleted_at` = '".date('Y-m-d H:i:s')."' WHERE `book`.`id` =" . $this->id;
        mysql_query($query);
        header('location:index.php');
    }


    public function trashed($id = ''){
        $allData = array();
        $query = "SELECT * FROM `book` where `deleted_at` IS NOT NULL";
        $result = mysql_query($query);
//        $onedata = mysql_fetch_assoc($result);
//        return $onedata;
        while ($onedata = mysql_fetch_assoc($result)) {
            $allData[] = $onedata;
        }
        return $allData;
    }

    public function restore(){
        $query = "UPDATE `atomicp`.`book` SET `deleted_at` = NULL WHERE `book`.`id` =".$this->id;
        mysql_query($query);
        header('location:trashed.php');
    }

    public function update(){
        $this->title;
        $query = "UPDATE `atomicp`.`book` SET `title` = '".$this->title."', `updated_at` = '".date('Y-m-d H:i:s')."' WHERE `book`.`id` =".$this->id;
        mysql_query($query);
        header('location:index.php');
    }

}
