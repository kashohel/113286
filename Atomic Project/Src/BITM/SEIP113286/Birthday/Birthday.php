<?php

namespace App\BITM\SEIP113286\Birthday;
class Birthday
{
    public $id = '';
    public $title = '';
    public $date = '';


    public function __construct()
    {
        $conn = mysql_connect('localhost', 'root', '') or die('Unable to connect with DB Server.');
        mysql_select_db('atomicp') or die ('Unable to connect with DB.');
    }

    public function prepare($data = '')
    {
        if (isset($data['id']) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['title']) && !empty($data['title'])) {
            $this->title = $data['title'];
        }
        if (isset($data['date']) && !empty($data['date'])) {
            $this->date = $data['date'];
        }

    }

    public function store($data = '')
    {
//        print_r($data);
//        die();
        $this->title = $data['title'];
        $this->date = $data['date'];
        $query = "INSERT INTO `atomicp`.`birthday` (`id`, `title`, `date`)"
            . "VALUES (NULL, '" . $this->title . "', '" . $this->date . "')";
        mysql_query($query);
        header('location:index.php');
    }

    public function index()
    {
        $alldata = array();
        $query = "SELECT * FROM `birthday` WHERE `deleted_at` IS NULL";
        $result = mysql_query($query);
        while ($onedata = mysql_fetch_assoc($result)) {
            $alldata[] = $onedata;
        }
        return $alldata;
    }

    public function show($id = '')
    {
        $this->id = $id;
        $query = "SELECT * FROM `birthday` WHERE id=" . $this->id;
        $result = mysql_query($query);
        return mysql_fetch_assoc($result);
    }

    public function edit()
    {
        $query = "UPDATE `atomicp`.`birthday` SET `title` = '" . $this->title . "', `date` = '" . $this->date . "' WHERE `birthday`.`id` = " . $this->id;
        mysql_query($query);
        header('location:index.php');
    }

    public function trash($data = '')
    {
        $this->id = $data['id'];
        $date = time();
        $query = "UPDATE `atomicp`.`birthday` SET `deleted_at` = '" . $date . "' WHERE `birthday`.`id` = " . $this->id;
        mysql_query($query);
        header('location:index.php');
    }

    public function restore($data = '')
    {
        $this->id = $data['id'];;
        $query = "UPDATE `atomicp`.`birthday` SET `deleted_at` = NULL WHERE `birthday`.`id` =" . $this->id;
        mysql_query($query);
        header('location:index.php');
    }

    public function trashed($id = '')
    {
        $alldata = array();
        $query = "SELECT * FROM `birthday` WHERE `deleted_at` IS NOT NULL";
        $result = mysql_query($query);
        while ($onedata = mysql_fetch_assoc($result)) {
            $alldata[] = $onedata;
        }
        return $alldata;
    }

    public function delete()
    {
        $query = "DELETE FROM `atomicp`.`birthday` WHERE `birthday`.`id` =" . $this->id;
        mysql_query($query);
        header('location:trashed.php');
    }


}