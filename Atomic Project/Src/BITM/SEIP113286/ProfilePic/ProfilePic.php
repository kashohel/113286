<?php

namespace App\BITM\SEIP113286\ProfilePic;
class ProfilePic
{
public $id = '';
public $user_name = '';
public $image_name = '';
public $oldimg = '';

    public function __construct()
    {
    $conn = mysql_connect('localhost', 'root', '') or die ('Unable to connect with DB server.');
        mysql_select_db('atomicp') or die ('Unable to conect with database.');
    }

    public function prepare($data = '')
    {
        if (isset($data['id']) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        if (isset($data['title']) && !empty ($data['title']))
        {
            $this->user_name = $data['title'];
        }
        if (isset($data['image']) && !empty($data['image']))
        {
            $this->image_name = $data['image'];
        }
        if (isset($data['oldimg']) && !empty($data['oldimg']))
        {
            $this->oldimg = $data['oldimg'];
        }
    }

    public function store()
    {
        $query = "INSERT INTO `atomicp`.`profilepic` (`id`, `user_name`, `image_name`, `created_at`) VALUES (NULL, '".$this->user_name."', '".$this->image_name."', '".date('Y-m-d')."')";
        mysql_query($query);
        header('location:index.php');
    }

    public function index()
    {
        $allData = array();
        $query = "SELECT * FROM `profilepic` WHERE `deleted_at` IS NULL";
        $run_query = mysql_query($query);
        while ($oneData = mysql_fetch_assoc($run_query))
        {
            $allData[] = $oneData;
        }
        return $allData;
    }

    public function show($id = '')
    {
        $query = "SELECT * FROM `profilepic` WHERE id =".$id;
        $run_query = mysql_query($query);
        $result = mysql_fetch_assoc($run_query);
        return $result;
    }

    public function update()
    {
        if (isset($this->image_name) && !empty($this->image_name))
        {
            $query = "UPDATE `atomicp`.`profilepic` SET `user_name` = '".$this->user_name."', `image_name` = '".$this->image_name."' WHERE `profilepic`.`id` =".$this->id;
        mysql_query($query);
        } else{
            $query = "UPDATE `atomicp`.`profilepic` SET `user_name` = '".$this->user_name."' WHERE `profilepic`.`id` =".$this->id;
            mysql_query($query);
        }
        unlink('../../../../assets/img/'.$this->oldimg);
        header('location:index.php');
    }

    public function trash($id = '')
    {
        $query = "UPDATE `atomicp`.`profilepic` SET `deleted_at` = '".date('Y-m-d')."' WHERE `profilepic`.`id` =".$id;
        mysql_query($query);
        header('location:index.php');
    }

    public function trashed()
    {
        $allData = array();
        $query = "SELECT * FROM `profilepic` WHERE `deleted_at` IS NOT NULL";
        $run_query = mysql_query($query);
        while ($oneData = mysql_fetch_assoc($run_query))
        {
            $allData[] = $oneData;
        }
        return $allData;
    }

    public function restore($id = '')
    {
        $query = "UPDATE `atomicp`.`profilepic` SET `deleted_at` = NULL WHERE `profilepic`.`id` =".$id;
        mysql_query($query);
        header('location:trashed.php');
    }

    public function delete($data = '')
    {
        $query = "DELETE FROM `atomicp`.`profilepic` WHERE `profilepic`.`id`=".$data['id'];
        mysql_query($query);
        unlink('../../../../assets/img/'.$data['image']);
        header('location:trashed.php');
    }



}