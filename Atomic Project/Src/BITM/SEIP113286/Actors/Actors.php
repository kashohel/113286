<?php


namespace App\BITM\SEIP113286\Actors;


class Actors
{
    public $id = '';
    public $title = '';
    public $actors = '';

    public function __construct()
    {
        $conn = mysql_connect('localhost', 'root', '') or die ('Unable to connect with DB Server.');
        mysql_select_db('atomicp') or die ('Unable to connect with database.');
    }

    public function prepare($data = '')
    {
        $this->id = $data['id'];
        $this->title = $data['title'];
        $this->actors = $data['actors'];
    }

    public function store()
    {
        $query = "INSERT INTO `atomicp`.`actors` (`id`, `title`, `actors`)"."VALUES (NULL, '".$this->title."', '".$this->actors."')";
        mysql_query($query);
        header('location:index.php');
    }

    public function index()
    {
        $allData = array();
        $query = "SELECT * FROM `actors` WHERE deleted_at IS NULL";
        $run_query = mysql_query($query);
        while ($oneData = mysql_fetch_assoc($run_query)){
            $allData[] = $oneData;
        }
        return $allData;
    }

    public function show($id = '')
    {
        $query = "SELECT * FROM `actors` WHERE id=".$id;
        $run_query = mysql_query($query);
        return mysql_fetch_assoc($run_query);

    }

    public function update()
    {
        $query = "UPDATE `atomicp`.`actors` SET `title` = '".$this->title."', `actors` = '".$this->actors."' WHERE `actors`.`id` =".$this->id;
        mysql_query($query);
        header('location:index.php');
    }

    public function trash($id = '')
    {
        $query = "UPDATE `atomicp`.`actors` SET `deleted_at` = '".date('Y-m-d')."' WHERE `actors`.`id` = ".$id;
        $run_query = mysql_query($query);
        mysql_fetch_assoc($run_query);
        header('location:index.php');
    }

    public function trashed($id = '')
    {
        $allData = array();
        $query = "SELECT * FROM `actors` WHERE deleted_at IS NOT NULL";
        $run_query = mysql_query($query);
        while ($oneData = mysql_fetch_assoc($run_query)){
            $allData[] = $oneData;
        }
        return $allData;
    }

    public function restore($id = '')
    {
        $query = "UPDATE `atomicp`.`actors` SET `deleted_at` = NULL WHERE `actors`.`id` = ".$id;
        mysql_query($query);
        header('location:trashed.php');

    }

    public function delete($id = '')
    {
        $query = "DELETE FROM `atomicp`.`actors` WHERE `actors`.`id` =".$id;
        mysql_query($query);
        header('location:trashed.php');

    }

}
