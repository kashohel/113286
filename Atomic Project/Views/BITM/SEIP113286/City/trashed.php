<button><a href="index.php" type="button">Go to List page</a></button></br></br>

<?php

error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\City\City;
$myobj = new City();

$AllData = $myobj->trashed();


?>
<head>
    <title>Leaving City |  Deleted List</title>
</head>

<table border="1">
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Name</th>
        <th>City</th>
        <th>Action</th>
    </tr>
    <?php
    if(isset($AllData) && !empty($AllData)){
        $serial = 0;
        foreach ($AllData as $Data){
            $serial++
            ?>
            <tr>
                <td><?php echo $serial ?></td>
                <td>
                    <?php echo $Data['id'] ?>
                </td>
                <td>
                    <?php echo $Data['name'] ?>
                </td>
                <td>
                    <?php echo $Data['city'] ?>
                </td>
                <td>
                    <a href="restore.php?id=<?php echo $Data['id'] ?>">Restore</a> |
                    <a href="delete.php?id=<?php echo $Data['id'] ?>">Clean Permanent</a>
                </td>

            </tr>
        <?php } }else{ ?>
        <tr>
            <td colspan="4">
                <?php echo "Opps! No avilable Data here" ?>
            </td>
        </tr>
    <?php  }
    ?>
</table>
