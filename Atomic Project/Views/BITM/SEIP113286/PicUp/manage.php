<button><a href="index.php" type="button">View Slide</a></button> |
<button><a href="create.php" type="button">Create new +</a></button> |
<button><a href="trashed.php" type="button">Deleted data</a></button> |
<button><a href="user.php" type="button">User</a></button></br></br>

<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\PicUp\PicUp;

$obj = new PicUp();
$Datas = $obj->manage();
//echo "active.php?id=".$Data['id'];
//echo "deactive.php?id=".$Data['id'];


?>
<head>
    <title>Profile pic up | List</title>
</head>

<table border="1">
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Profile Picture</th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($Datas) && !empty($Datas)) {
        $serial = 0;
        foreach ($Datas as $Data) {
            $serial++
            ?>
            <tr>
                <td><?php echo $serial ?></td>
                <td>
                    <?php echo $Data['id'] ?>
                </td>
                <td>
                    <img src="<?php echo "../../../../assets/img/PicUp/" . $Data['title'] ?>" width="100" height="100"/>
                </td>
                <td>
                    <a href="edit.php?id=<?php echo $Data['id'] ?>">Edit</a> |
                    <a type="hidden" href="show.php?id=<?php echo $Data['id'] ?>">Show</a> |

                    <?php if ($Data['status'] === "deactive") {
                        ?>
                        <a href="<?php echo "active.php?id=" . $Data['id'] ?>">Active</a>
                        <?php
                    } else {
                        echo "Deactive";
                    }
                    ?> |
                    <a href="trash.php?id=<?php echo $Data['id'] ?>">Delete</a>
                </td>

            </tr>
        <?php }
    } else { ?>
        <tr>
            <td colspan="4">
                <?php echo "Opps! No avilable Data here" ?>
            </td>
            <td>
            </td>
        </tr>
    <?php }
    ?>
</table>



