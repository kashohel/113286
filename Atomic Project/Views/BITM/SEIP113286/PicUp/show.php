<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\PicUp\PicUp;
$obj = new PicUp();
$id = $_GET['id'];
$data = $obj->show($id);

?>

<head>
    <title>Profile Pic Up | Show</title>
</head>
<button><a href="create.php" type="button">Create New ++</a></button> |
<button><a href="manage.php" type="button">Back to List</a></button>
<br/><br/>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Profile Picture</th>
        <th>Created</th>
        <th>Updated</th>
        <th>Action</th>
    </tr>
    <tr>
        <td><?php echo $data['id']?></td>
        <td><img src="<?php echo "../../../../assets/img/PicUp/".$data['title']?>"  width="200" height="150"/></td>
        <td><?php echo $data['created_at']?></td>
        <td><?php echo $data['updated_at']?></td>
        <td>
            <a href="edit.php?id=<?php echo $id ?>">Edit</a> |
            <a href="trash.php?id=<?php echo $id ?>">Delete</a>
        </td>
    </tr>
</table>