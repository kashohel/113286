<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\PicUp\PicUp;

$obj = new PicUp();
$Data = $obj->index();
$User = $obj->user();


?>


<html >
<head>
    <meta charset="UTF-8">
    <title>Profile Pic</title>
    <link rel="stylesheet" href="../../../../assets/css/normalize.css">
    <link rel="stylesheet" href="../../../../assets/css/style.css">

</head>

<body>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

<div class="wrapper">
    <div class="profile-pic-cont">
        <div class="pic-crop">
            <?php
            if (isset($Data['title']) && !empty($Data['title']))
            {
                ?>
                <img class="centered-and-cropped" src="<?php echo "../../../../assets/img/PicUp/" . $Data['title']; ?>" alt="" />
                <?php
            } else {
                echo $Data['title'];
            }
            ?>
        </div>
        <a href="manage.php" class="wrap">
            <div class="partial-circle">
                <i class="fa fa-plus"></i>
            </div>
        </a><br/><br/>
        <center>
        <button><?php echo $User['title']; ?></button><br/>
        <button><?php echo $User['status']; ?></button>
        </center>
    </div>
</div>






</body>
</html>
