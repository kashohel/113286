<button><a href="create.php" type="button">Create new +</a></button> |
<button><a href="manage.php" type="button">Back to list</a></button></br></br>

<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\PicUp\PicUp;

$obj = new PicUp();
$Datas = $obj->trashed();

?>
<head>
    <title>Slider | Delete list</title>
</head>

<table border="1">
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Title</th>
        <th>Slider Picture</th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($Datas) && !empty($Datas)) {
        $serial = 0;
        foreach ($Datas as $Data) {
            $serial++
            ?>
            <tr>
            <td><?php echo $serial ?></td>
            <td>
                <?php echo $Data['id'] ?>
            </td>
            <td>
                <?php echo $Data['title'] ?>
            </td>
            <td>
            <img src="<?php echo "../../../../assets/img/PicUp/" .$Data['title'] ?>" width="100" height="100"/>
                </td>
                <td>
                    <a href="restore.php?id=<?php echo $Data['id'] ?>">Restore</a> |
                    <a href="delete.php?id=<?php echo $Data['id'] ?>">Clean</a>
                </td>

            </tr>
        <?php }
    } else{ ?>
        <tr>
            <td colspan="4">
                <?php echo "Opps! No avilable Data here" ?>
            </td>
            <td>
            </td>
        </tr>
    <?php  }
    ?>
</table>



