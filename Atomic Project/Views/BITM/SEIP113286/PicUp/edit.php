<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\PicUp\PicUp;
$obj = new PicUp();
$id = $_GET['id'];
$data = $obj->show($id);

?>

<html>
<head>
    <head><title>Profile Pic Up | Update</title></head>
<body>
<button><a href="manage.php">Back</a></button>


<?php
session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}


?>

<form action="update.php" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>Slider update form</legend>

        <label>Update slide title</label><br/>
        <input type="hidden" name="id" id="id" value="<?php echo "$id"; ?>"><br/>
        <img src="<?php echo "../../../../assets/img/PicUp/" . $data['title']; ?>" width="100" height="100"/><br/>
        <label>Please reselect picture for slide.</label><br/>
        <input type="file" name="image" id="image"><br/><br/>
        <button type="submit">Save</button> |
        <button type="back">Back</button>

    </fieldset>
</form>
</body>
</html>