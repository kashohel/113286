<button><a href="create.php" type="button">Add new model</a></button> | <button><a href="trashed.php" type="button">Deleted data</a></button></br></br>

<?php
error_reporting(E_ALL ^ E_DEPRECATED);

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Book\Book;
//use App\BITM\SEIP113286\Utility\Utility;

$books = new Book();
$AllBook = $books->index();

?>
<head>
    <title>Book Title | Book List</title>
</head>

<table border="1">
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Title</th>
        <th>Action</th>
    </tr>
    <?php 
    if(isset($AllBook) && !empty($AllBook)){
    $serial = 0;
    foreach ($AllBook as $Book){
        $serial++
    ?>
    <tr>
        <td><?php echo $serial ?></td>
        <td>
            <?php echo $Book['id'] ?>
        </td>
        <td>
            <?php echo $Book['title'] ?>
        </td>
        <td>
            <a href="edit.php?id=<?php echo $Book['id'] ?>">Edit</a> |
            <a href="show.php?id=<?php echo $Book['id'] ?>">Show</a> |
            <a href="trash.php?id=<?php echo $Book['id'] ?>">Delete</a>
        </td>
        
    </tr>
    <?php } }else{ ?>
    <tr>
        <td colspan="4">
            <?php echo "Opps! No avilable Data here" ?>
        </td>
    </tr>
  <?php  }
?>
</table>



