<button><a href="create.php" type="button">Add new model</a></button> |
<button><a href="trashed.php" type="button">Deleted data</a></button>  |
<button><a href="pdf.php" type="button">Save as PDF</a></button>  |
<button><a href="xl.php" type="button">Save as XL</a></button>  |
<button><a href="phpmailer.php" type="button">Send mail</a></button></br></br>

<?php

error_reporting(E_ALL ^ E_DEPRECATED);

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Birthday\Birthday;

$obj = new Birthday();
$AllData = $obj->index();


?>
<head>
    <title>Birthdate | View List</title>
</head>

<table border="1">
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Name</th>
        <th>Birthdate</th>
        <th>Action</th>
    </tr>
    <?php
    if (isset($AllData) && !empty($AllData)) {
        $serial = 0;
        foreach ($AllData as $Data) {
            $serial++
            ?>
            <tr>
                <td>
                    <?php echo $serial ?>
                </td>
                <td>
                    <?php echo $Data['id'] ?>
                </td>
                <td>
                    <?php echo $Data['title'] ?>
                </td>
                <td>
                    <?php echo $Data['date'] ?>
                </td>
                <td>
                    <a href="edit.php?id=<?php echo $Data['id'] ?>">Edit</a> |
                    <a href="show.php?id=<?php echo $Data['id'] ?>">Show</a> |
                    <a href="trash.php?id=<?php echo $Data['id'] ?>">Delete</a>
                </td>

            </tr>
        <?php }
    } else { ?>
        <tr>
            <td colspan="4">
                <?php echo "Opps! No avilable Data here" ?>
            </td>
        </tr>
    <?php }
    ?>
</table>



