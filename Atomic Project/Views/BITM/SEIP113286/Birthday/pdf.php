<?php

error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/mpdf/mpdf/mpdf.php';
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Birthday\Birthday;

$obj = new Birthday();
$AllData = $obj->index();
$trs = "";
$serial = 0;
foreach ($AllData as $Data):
    $serial++;
    $trs .= "<tr>";
    $trs .= "<td>" . $serial . "</td>";
    $trs .= "<td>" . $Data['id'] . "</td>";
    $trs .= "<td>" . $Data['title'] . "</td>";
    $trs .= "<td>" . $Data['date'] . "</td>";
   $trs .= "</tr>";
endforeach;
$html = <<<EOD
<html>
    <head>
        <title>
        Birthdate | View List
        </title>
    </head>
    <body>
        <h1 align="center">Birthdate | View List</h1><hr>

            <table border="1" align="center">
            <thead>
                <tr>
                    <th>SL</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Birthdate</th>
                </tr>
            </thead>
            <tbody>
                $trs;
            </tbody>
        </table>
    </body>
</html>

EOD;

$mpdf = new mPDF();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>


