<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Slider\Slider;

$obj = new Slider();
$Datas = $obj->index();

?>


<!DOCTYPE html>
<html>
<head>
    <title>Slideshow</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="description"
          content="Made by Khorshed Alam"/>

    <!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
    <link rel="stylesheet" type="text/css" href="../../../../assets/engine/style.css"/>
    <script type="text/javascript" src="../../../../assets/engine/jquery.js"></script>
    <!-- End WOWSlider.com HEAD section -->

</head>
<body style="background-color:#d7d7d7;margin:auto">


<div id="wowslider-container1">
    <div class="ws_images">
        <ul>
            <?php
            if (isset($Datas) && !empty($Datas)){
            $serial = 0;
            foreach ($Datas as $Data){
            $serial++
            ?>
            <li><img src="<?php echo "../../../../assets/img/Slider/" . $Data['image'] ?>"
                     alt="<?php echo $Data['title'] ?>"
                     title="<?php echo $Data['title'] ?>"
                     id="<?php echo "wows1_" . $serial ?>"/>
                     <?php echo $Data['subtitle'] ?></li>

        <?php }
                     } else {
                         echo " No slide element available. Ad new slide";
                     } ?>
                <!---->
                <!--            <li><img src=" ../../../../assets/img/Slider/seb.jpg" alt="wowslider" title="Two" id="wows1_1"/>Two subtitle </li> -->
                <!--            <li><img src="../../../../assets/img/Slider/tab3.png" alt="Three" title="Three" id="wows1_2"/>Three subtitle</li>-->
        </ul>
    </div>
    <div class="ws_bullets">
        <div>

            <?php
            if (isset($Datas) && !empty($Datas)) {
                $serial = 0;
                foreach ($Datas as $Data) {
                    $serial++
                    ?>

                    <a href="#" title="<?php echo $Data['title'] ?>"></span></a>

                <?php }
            } else { ?>
            <?php } ?>
            <!--            <a href="#" title="Two"><span></span></a>-->
            <!--            <a href="#" title="Three"><span></span></a>-->
        </div>
    </div>
    <div class="ws_script" style="position:absolute;left:-99%"></div>
    <div class="ws_shadow"></div>
</div>
<script type="text/javascript" src="../../../../assets/engine/wowslider.js"></script>
<script type="text/javascript" src="../../../../assets/engine/script.js"></script>
<!-- End WOWSlider.com BODY section -->

<br/><br/><br/><br/>

<center><button><a href="manage.php">Manage Slide</a></button></center>

</body>
</html>