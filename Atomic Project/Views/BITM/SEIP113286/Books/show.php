<?php 
error_reporting(E_ALL ^ E_DEPRECATED);

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Books\Books;

//
//$obj = new Utility();
//
$id = $_GET['id'];


$books = new Books();
$Onedata = $books->show($id);

//$obj->debug($Onedata);
?>
<head>
    <title>Book Title | Show</title>
</head>
<button><a href="create.php" type="button">Add new model</a></button> |
<button><a href="index.php" type="button">Back to List</a></button>
<br/><br/>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Created</th>
        <th>Updated</th>
        <th>Action</th>
    </tr>
    <tr>
        <td><?php echo $Onedata['id']?></td>
        <td><?php echo $Onedata['title']?></td>
        <td><?php echo $Onedata['created_at']?></td>
        <td><?php echo $Onedata['updated_at']?></td>
        <td>
            <a href="edit.php?id=<?php echo $id ?>">Edit</a> |
            <a href="trash.php?id=<?php echo $id ?>">Delete</a>
        </td>
    </tr>
</table>




