<button><a href="index.php" type="button">Go to List page</a></button></br></br>

<?php

error_reporting(E_ALL ^ E_DEPRECATED);

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Books\Books;

$books = new Books();
$AllBook = $books->trashed();


?>
<head>
    <title>Book title |  List</title>
</head>

<table border="1">
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Title</th>
        <th>Action</th>
    </tr>
    <?php
    if(isset($AllBook) && !empty($AllBook)){
        $serial = 0;
        foreach ($AllBook as $Book){
            $serial++
            ?>
            <tr>
                <td><?php echo $serial ?></td>
                <td>
                    <?php echo $Book['id'] ?>
                </td>
                <td>
                    <?php echo $Book['title'] ?>
                </td>
                <td>
                    <a href="restore.php?id=<?php echo $Book['id'] ?>">Restore</a> |
                    <a href="delete.php?id=<?php echo $Book['id'] ?>">Clean Permanent</a>
                </td>

            </tr>
        <?php } }else{ ?>
        <tr>
            <td colspan="4">
                <?php echo "Opps! No avilable Data here" ?>
            </td>
        </tr>
    <?php  }
    ?>
</table>
