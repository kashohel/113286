<?php

include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Hobby\Hobby;
$obj = new Hobby();
$id = $_GET['id'];
$data = $obj->show($id);

?>

<html>
<head>
    <head><title>Hobbies | Update</title></head>
<body>
<button><a href="index.php">Back</a></button>


<?php
session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

$check = explode(",", $data['hobby']);
?>

<form action="update.php" method="post">
    <fieldset>
        <legend>Term and condition form</legend>
        <input type="hidden" name="id" id="id" value="<?php echo "$id"; ?>"><br/>
        <label>Insert your name.</label><br/>
        <input type="text" name="title" id="title"><br/>
        <label>Select your Hobby.</label><br/>
        <input type="checkbox" name="hobby[]" id="hobby" value="C"<?php if (in_array("C", $check)){ echo 'checked'; }else{ echo ""; }?> />C
        <input type="checkbox" name="hobby[]" id="hobby" value="C++"<?php if (in_array("C++", $check)){ echo 'checked'; }else{ echo ""; }?>/>C++
        <input type="checkbox" name="hobby[]" id="hobby" value="Java"<?php if (in_array("Java", $check)){ echo 'checked'; }else{ echo ""; }?>/>Java
        <input type="checkbox" name="hobby[]" id="hobby" value="PHP"<?php if (in_array("PHP", $check)){ echo 'checked'; }else{ echo ""; }?>/>PHP
        <input type="checkbox" name="hobby[]" id="hobby" value="DotNet"<?php if (in_array("DotNet", $check)){ echo 'checked'; }else{ echo ""; }?>/>.Net<br/>
        <button type="submit">Save</button>
        <button type="reset">Reset</button>


    </fieldset>
</form>
</body>
</html>