<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\ProfilePic\ProfilePic;
$obj = new ProfilePic();
$id = $_GET['id'];
$data = $obj->show($id);

?>

<html>
<head>
    <head><title>Profile Picture | Update</title></head>
<body>
<button><a href="index.php">Back</a></button>


<?php
session_start();
if (isset($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}


?>

<form action="update.php" method="post" enctype="multipart/form-data">
    <fieldset>
        <legend>Profile picture update form</legend>

        <label>Update your name.</label><br/>
        <input type="hidden" name="id" id="id" value="<?php echo "$id"; ?>">
        <input type="text" name="title" id="title" value="<?php echo $data['user_name']; ?>"><br/>
        <label>Please reselect picture for upload.</label><br/>
        <input type="file" name="image" id="image">
        <button type="submit">Save</button>
        <button type="reset">Reset</button>


    </fieldset>
</form>
</body>
</html>