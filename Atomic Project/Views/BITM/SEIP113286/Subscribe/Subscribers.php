<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';
use App\BITM\SEIP113286\Subscribe\Subscribe;

$obj = new Subscribe();
$datas = $obj->index();
//print_r($datas);
//die();

?>
<!DOCTYPE html>
<html class="no-js lt-ie9">
<html class="no-js lt-ie10">
<html class="no-js">
<head>
    <meta charset="utf-8">
    <title>ProUI - Responsive Bootstrap Admin Template</title>
    <link rel="stylesheet" href="../../../../assets/new/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../assets/new/css/plugins.css">
    <link rel="stylesheet" href="../../../../assets/new/css/main.css">
    <link rel="stylesheet" href="../../../../assets/new/css/themes.css">
    <script src="../../../../assets/new/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
    <link rel="stylesheet" href="../../../../assets/new/css/bootstraps.min.css">
    <link rel="stylesheet" href="../../../../assets/new/css/pluginss.css">
    <link rel="stylesheet" href="../../../../assets/new/css/mains.css">
    <link rel="stylesheet" href="../../../../assets/new/css/themess.css">
    <script src="../../../../assets/new/js/vendor/modernizr-2.7.1.js"></script>
</head>

<body>
<div id="page-container">
    <!-- Site Header -->
    <header>
        <div class="container">
            <!-- Site Logo -->
            <a href="index.html" class="site-logo">
                <i class="gi gi-dashboard"></i> Marge <strong>Project</strong>
            </a>
            <!-- Site Logo -->

            <!-- Site Navigation -->
            <nav>
                <!-- Menu Toggle -->
                <!-- Toggles menu on small screens -->
                <a href="javascript:void(0)" class="btn btn-default site-menu-toggle visible-xs visible-sm">
                    <i class="fa fa-bars"></i>
                </a>
                <!-- END Menu Toggle -->

                <!-- Main Menu -->
                <ul class="site-nav">
                    <!-- Toggles menu on small screens -->
                    <li class="visible-xs visible-sm">
                        <a href="javascript:void(0)" class="site-menu-toggle text-center">
                            <i class="fa fa-times"></i>
                        </a>
                    </li>
                    <!-- END Menu Toggle -->
                    <li>
                        <a href="contact.html"> Home</a>
                    </li>
                    <li>
                        <a href="about.html">About</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="site-nav-sub"><i
                                class="fa fa-angle-down site-nav-arrow"></i>Projects</a>
                        <ul>
                            <li>
                                <a href="index.html">Book</a>
                            </li>
                            <li>
                                <a href="index_alt.html">Birthday</a>
                            </li>
                            <li>
                                <a href="index_parallax.html">Profile Pic</a>
                            </li>
                            <li>
                                <a href="index_boxed.html">Gender</a>
                            </li>
                            <li>
                                <a href="index_boxed_alt.html">Education</a>
                            </li>
                            <li>
                                <a href="index_boxed_parallax.html">Mobile Model</a>
                            </li>
                            <li>
                                <a href="index_boxed_parallax.html">Hobby</a>
                            </li>
                            <li>
                                <a href="index_boxed_parallax.html">City</a>
                            </li>
                            <li>
                                <a href="index.php">Email Subs</a>
                            </li>
                            <li>
                                <a href="index_boxed_parallax.html">Summery</a>
                            </li>
                            <li>
                                <a href="index_boxed_parallax.html">Term and Condition</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="contact.html">Contact</a>
                    </li>

                </ul>
                <!-- END Main Menu -->
            </nav>
            <!-- END Site Navigation -->
        </div>
    </header>

</div>
<br/><br/><br/><br/>
<div id="page-content" style="min-height: 986px;">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <div class="block-section">
                    <a href="index.php"><button type="button" class="btn btn-alt btn-primary"> Create New</button></a>
                    <a href="Subscribers.php"><button type="button" class="btn btn-alt btn-info">List of subscriber</button></a>
                    <a href="trashed.php"><button type="button" class="btn btn-alt btn-success">Deleted list</button></a>
                </div>
            </h1>
        </div>
    </div>

    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>Subscribers</strong> as List</h2>
        </div>
        <div class="table-responsive">
            <div id="example-datatable_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <div class="col-sm-6 col-xs-5">

                    </div>
                    <div class="col-sm-6 col-xs-7">
                        <div id="example-datatable_filter" class="dataTables_filter"><label>
                                <div class="input-group"><input type="search" class="form-control"
                                                                aria-controls="example-datatable"
                                                                placeholder="Search"><span class="input-group-addon"><i
                                            class="fa fa-search"></i></span></div>
                            </label></div>
                    </div>
                </div>
                <table id="example-datatable"
                       class="table table-vcenter table-condensed table-bordered dataTable no-footer" role="grid"
                       aria-describedby="example-datatable_info">
                    <thead>
                    <tr role="row">
                        <th class="text-center sorting_asc" tabindex="0" aria-controls="example-datatable" rowspan="1"
                            colspan="1" aria-sort="ascending" aria-label="ID: activate to sort column ascending"
                            style="width: 57px;">Serial
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="example-datatable" rowspan="1" colspan="1"
                            aria-label="Email: activate to sort column ascending" style="width: 219px;">Email
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="example-datatable" rowspan="1" colspan="1"
                            aria-label="Phone: activate to sort column ascending" style="width: 217px;">Status
                        </th>
                        <th class="text-center sorting_disabled" rowspan="1" colspan="1" aria-label="Actions"
                            style="width: 141px;">Actions
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    if (isset($datas) && !empty($datas)){
                    $serial = 0;
                    foreach ($datas as $data){
                    $serial++
                    ?>

                    <tr role="row" class="odd">
                        <td class="text-center sorting_1"><?php echo $serial ?></td>
                        <td><?php echo $data['email']; ?></td>
                        <td><?php if ($data['active'] == "1"){echo "Active";}else{echo "Deactive";} ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <?php if ($data['active'] == "1"){?>
                                <a href="active.php?id=<?php echo $data['id'] ?>" data-toggle="tooltip" title=""
                                   class="btn btn-xs btn-default" data-original-title="Activated"><i
                                        class="fa fa-eye-slash"></i></a>
                                <?php
                                } else {
                                   ?>
                                <a href="active.php?id=<?php echo $data['id'] ?>" data-toggle="tooltip" title=""
                                   class="btn btn-xs btn-default" data-original-title="Deactivated"><i
                                        class="fa fa-eye"></i></a>
                                <?php
                                } ?>


                                <a href="trash.php?id=<?php echo $data['id'] ?>" data-toggle="tooltip" title=""
                                   class="btn btn-xs btn-danger" data-original-title="Delete"><i
                                        class="fa fa-times"></i></a>
                            </div>
                        </td>

                        <?php } }else{ echo"nothing found"; }?>

                    </tbody>
                </table>
                <div class="row">
                    <div class="col-sm-5 hidden-xs">
                        <div class="dataTables_info" id="example-datatable_info" role="status" aria-live="polite">
                            <strong>1</strong>-<strong>10</strong> of <strong>60</strong></div>
                    </div>
                    <div class="col-sm-7 col-xs-12 clearfix">
                        <div class="dataTables_paginate paging_bootstrap" id="example-datatable_paginate">
                            <ul class="pagination pagination-sm remove-margin">
                                <li class="prev disabled"><a href="javascript:void(0)"><i
                                            class="fa fa-chevron-left"></i> </a></li>
                                <li class="active"><a href="javascript:void(0)">1</a></li>
                                <li><a href="javascript:void(0)">2</a></li>
                                <li><a href="javascript:void(0)">3</a></li>
                                <li><a href="javascript:void(0)">4</a></li>
                                <li><a href="javascript:void(0)">5</a></li>
                                <li class="next"><a href="javascript:void(0)"> <i class="fa fa-chevron-right"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Datatables Content -->
    <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

    <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
    <script src="../assets/js/vendor/bootstrap.min.js"></script>
    <script src="../assets/js/plugins.js"></script>
    <script src="../assets/js/app.js"></script>

    <!-- Load and execute javascript code used only in this page -->
    <script src="../assets/js/pages/tablesDatatables.js"></script>
    <script>$(function () {
            TablesDatatables.init();
        });</script>
</div>
</body>
</html>