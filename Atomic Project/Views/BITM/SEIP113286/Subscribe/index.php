<!DOCTYPE html>
<html class="no-js lt-ie9">
<html class="no-js lt-ie10">
<html class="no-js">
<head>
    <meta charset="utf-8">
    <title>ProUI - Responsive Bootstrap Admin Template</title>
    <link rel="stylesheet" href="../../../../assets/new/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../assets/new/css/plugins.css">
    <link rel="stylesheet" href="../../../../assets/new/css/main.css">
    <link rel="stylesheet" href="../../../../assets/new/css/themes.css">
    <script src="../../../../assets/new/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
    <link rel="stylesheet" href="../../../../assets/new/css/bootstraps.min.css">
    <link rel="stylesheet" href="../../../../assets/new/css/pluginss.css">
    <link rel="stylesheet" href="../../../../assets/new/css/mains.css">
    <link rel="stylesheet" href="../../../../assets/new/css/themess.css">
    <script src="../../../../assets/new/js/vendor/modernizr-2.7.1.js"></script>
</head>
<body>
<div id="page-container">
    <header>
        <div class="container">
            <a href="../../../../index.php" class="site-logo">
                <i class="gi gi-dashboard"></i> Marge <strong>Project</strong>
            </a>
            <nav>
                <a href="javascript:void(0)" class="btn btn-default site-menu-toggle visible-xs visible-sm">
                    <i class="fa fa-bars"></i>
                </a>
                <ul class="site-nav">
                    <li class="visible-xs visible-sm">
                        <a href="javascript:void(0)" class="site-menu-toggle text-center">
                            <i class="fa fa-times"></i>
                        </a>
                    </li>
                    <li>
                        <a href="../../../../index.php"> Home</a>
                    </li>
                    <li>
                        <a href="../../../../About.php">About</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="site-nav-sub"><i
                                class="fa fa-angle-down site-nav-arrow"></i>Projects</a>
                        <ul>
                            <li>
                                <a href="../Book">Book</a>
                            </li>
                            <li>
                                <a href="../Birthday">Birthday</a>
                            </li>
                            <li>
                                <a href="../PicUp">Profile Pic</a>
                            </li>
                            <li>
                                <a href="../Gender">Gender</a>
                            </li>
                            <li>
                                <a href="../Education">Education</a>
                            </li>
                            <li>
                                <a href="../Mobile">Mobile Model</a>
                            </li>
                            <li>
                                <a href="../Hobby">Hobby</a>
                            </li>
                            <li>
                                <a href="../City">City</a>
                            </li>
                            <li>
                                <a href="../Subscribe">Email Subs</a>
                            </li>
                            <li>
                                <a href="../Summery">Summery</a>
                            </li>
                            <li>
                                <a href="../Term">Term and Condition</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="../../../../Contact.php">Contact</a>
                    </li>

                </ul>
            </nav>
        </div>
    </header>

</div>
<br/><br/><br/><br/>
<div id="page-content" style="min-height: 1018px;">
    <div class="content-header">
        <div class="header-section">
            <h1>
                <div class="block-section">
                    <a href="index.php">
                        <button type="button" class="btn btn-alt btn-primary"> Create New</button>
                    </a>
                    <a href="Subscribers.php">
                        <button type="button" class="btn btn-alt btn-info">List of subscriber</button>
                    </a>
                    <a href="trashed.php">
                        <button type="button" class="btn btn-alt btn-success">Deleted list</button>
                    </a>
                </div>
            </h1>
        </div>
    </div

    <div class="row">
        <div class="col-md-15">
            <div class="block">
                <div class="block-title">
                    <h2><strong>Subscribe</strong> to have all latest update in time and stay connect with us.</h2>
                </div>
                <form id="form-validation" action="store.php" method="post" class="form-horizontal form-bordered"
                      novalidate="novalidate">
                    <fieldset>
                        <br/><br/><br/><br/>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="email">Email <span
                                    class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="email" id="email" name="email" class="form-control"
                                           placeholder="test@example.com" required>
                                    <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                </div>
                            </div>
                        </div>
                        <br/><br/><br/><br/>


                    </fieldset>
                    <div class="form-group form-actions" align="center">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i>
                                Subscribe Now
                            </button>
                        </div>
                    </div>
                </form>
                <!-- END Form Validation Example Content -->

                <!-- Terms Modal -->
                <div id="modal-terms" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">

                        </div>
                    </div>
                </div>
                <!-- END Terms Modal -->
            </div>
            <!-- END Validation Block -->
        </div>

    </div>
</div>


<footer class="clearfix">
    <div class="pull-right">
        Crafted with <i class="fa fa-heart text-danger"></i> by <a href="http://goo.gl/vNS3I"
                                                                   target="_blank">pixelcave</a>
    </div>
    <div class="pull-left">
        <span id="year-copy">2014-16</span> © <a href="http://goo.gl/TDOSuC" target="_blank">ProUI 2.1</a>
    </div>
</footer>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>!window.jQuery && document.write(decodeURI('%3Cscript src="../assets/js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>
<script src="../assets/js/vendor/bootstrap.min.js"></script>
<script src="../assets/js/plugins.js"></script>
<script src="../assets/js/app.js"></script>
<script src="../assets/js/pages/formsValidation.js"></script>
<script>$(function () {
        FormsValidation.init();
    });</script>
</body>
</html>