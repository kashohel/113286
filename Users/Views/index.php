<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../vendor/autoload.php';
use App\Users;

$obj = new Users();
$result = $obj->control();
$profile = $obj->profile_view();
$user = $obj->user_view();
$setting = $obj->setting();

if (!empty($profile['first_name']) || !empty($profile['last_name'])) {
    $profile['name'] = $profile['first_name'] . " " . $profile['last_name'];
} else {
    $profile['name'] = $user['username'];
}
//print_r($user);
//echo $user['username'];
//print_r($profile);
//echo $profile['name'];
//echo $result;
//die();
if ($result == "User") {
//    echo "Login as User.";
} elseif ($result == "Admin") {
//    echo "Login as Admin";
} else {
    header('location:login.php');
}

//if (isset($_SESSION['Message'])) {
//    echo $_SESSION['Message'];
//    unset ($_SESSION['Message']);
//}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $setting['title'] ;?> | Dashboard</title>

    <!--    Notification-->
    <link rel="stylesheet" type="text/css" href="../assets/notification/notification.css">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/progressbar/bootstrap-progressbar-3.3.0.css">

    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="nav-md">

<!--Notification-->
<?php
if (isset($_SESSION['Message'])) {
    $message = $_SESSION['Message'];
    ?>
    <script>onload = function () {
            $.notification.show('info', '<?php echo $message; ?>');
        } </script>
<?php
unset($_SESSION['Message']);
}elseif (isset($_SESSION['Message_Err'])){
$message = $_SESSION['Message_Err'];
?>
    <script>onload = function () {
            $.notification.show('error', '<?php echo $message; ?>');
        } </script>
    <?php
    unset($_SESSION['Message_Err']);
}
?>
<!--Notification- END -->

<div class="container body">
    <div>



    <div class="main_container">
        <!-- top navigation -->
        <?php
        include_once 'menu.php';
        ?>
        <!-- top navigation END-->


        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Deshboard</h3>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="">
                            <div class="x_content">


                                <div class="row">
                                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="tile-stats">
                                            <div class="icon"><i class="fa fa-users"></i>
                                            </div>
                                            <div class="count"><?php $obj->counter_user(); ?></div>

                                            <h3>Users</h3>
                                            <p>Total number of register users. </p>
                                        </div>
                                    </div>
                                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="tile-stats">
                                            <div class="icon"><i class="fa fa-check-square-o"></i>
                                            </div>
                                            <div class="count"><?php $obj->counter_active_user(); ?></div>

                                            <h3>Active Users</h3>
                                            <p>User who are active currently.</p>
                                        </div>
                                    </div>
                                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="tile-stats">
                                            <div class="icon"><i class="fa fa-sign-in"></i>
                                            </div>
                                            <div class="count"><?php $obj->counter_deactive_user(); ?></div>

                                            <h3>New Sign ups</h3>
                                            <p>Users are new in this site.</p>
                                        </div>
                                    </div>
                                    <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <div class="tile-stats">
                                            <div class="icon"><i class="fa fa-unlock-alt"></i>
                                            </div>
                                            <div class="count"><?php $obj->counter_user_profile(); ?></div>

                                            <h3>Premium User</h3>
                                            <p>Premium user of our site..</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                        if (!isset($profile['id']) && empty($profile['id'])) {
                        ?>

                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_content">

                                    <div class="col-md-8 col-lg-8 col-sm-7">
                                        <!-- blockquote -->
                                        <blockquote>
                                            <p>Your profile is not complete yet. Please complete your profile using
                                                following button. </p>
                                            <footer>After complete this section will hide automatically.</footer>
                                            </footer>
                                        </blockquote>
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-sm-5">
                                        <h1><a href="profilein.php"><span class="label label-primary">Complete your profile</span></a>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                        }
                        ?>
                        <div class="col-md-12">
                            <div class="x_panel">
                                <div class="x_content">

                                    <div class="col-md-8 col-lg-8 col-sm-7">
                                        <!-- blockquote -->
                                        <blockquote>
                                            <p>Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following buttonYour profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. </p>
                                            <footer>After complete this section will hide automatically.</footer>
                                            </footer>
                                        </blockquote>
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-sm-5">
                                        <blockquote>
                                            <p>Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. Your profile is not complete yet. Please complete your profile using
                                                following button. </p>
                                            <footer>After complete this section will hide automatically.</footer>
                                            </footer>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

                <!--            Fotter include-->
                <?php
                include_once 'footer.php';
                ?>
                <!--            Fotter include END-->


            </div>
        </div>
    </div>
</div>




</div>
<!-- /page content -->




<!--Notification Script-->
<script src="../assets/notification/jquery.notification.main.js"></script>
<script src="../assets/notification/jquery.notification.min.js"></script>
<!--Notification Script end-->

<script src="js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="js/icheck/icheck.min.js"></script>

<script src="js/custom.js"></script>

<!-- chart js -->
<script src="js/chartjs/chart.min.js"></script>
<!-- sparkline -->
<script src="js/sparkline/jquery.sparkline.min.js"></script>
<!-- easypie -->
<script src="js/easypie/jquery.easypiechart.min.js"></script>
<script>
    $(function () {
        $('.chart').easyPieChart({
            easing: 'easeOutElastic',
            delay: 3000,
            barColor: '#26B99A',
            trackColor: '#fff',
            scaleColor: false,
            lineWidth: 20,
            trackWidth: 16,
            lineCap: 'butt',
            onStep: function (from, to, percent) {
                $(this.el).find('.percent').text(Math.round(percent));
            }
        });
        var chart = window.chart = $('.chart').data('easyPieChart');
        $('.js_update').on('click', function () {
            chart.update(Math.random() * 200 - 100);
        });
    });
</script>

<script>
    var lineChartData = {
        labels: ["", "", "", "", "", "", ""],
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(38, 185, 154, 0.11)", //rgba(220,220,220,0.2)
                strokeColor: "rgba(38, 185, 154, 0.7)", //rgba(220,220,220,1)
                pointColor: "rgba(38, 185, 154, 0.7)", //rgba(220,220,220,1)
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(220,220,220,1)",
                data: [10, 30, 42, 23, 35, 55, 77]
            }
        ]

    }

    $(document).ready(function () {
        new Chart(document.getElementById("canvas_line").getContext("2d")).Line(lineChartData, {
            responsive: true,
            scaleShowLabels: false,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });
    });
    $(document).ready(function () {
        new Chart(document.getElementById("canvas_line1").getContext("2d")).Line(lineChartData, {
            responsive: true,
            scaleShowLabels: false,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });
    });
    $(document).ready(function () {
        new Chart(document.getElementById("canvas_line2").getContext("2d")).Line(lineChartData, {
            responsive: true,
            scaleShowLabels: false,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });
    });
    $(document).ready(function () {
        new Chart(document.getElementById("canvas_line3").getContext("2d")).Line(lineChartData, {
            responsive: true,
            scaleShowLabels: false,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });
    });
    $(document).ready(function () {
        new Chart(document.getElementById("canvas_line4").getContext("2d")).Line(lineChartData, {
            responsive: true,
            scaleShowLabels: false,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });
    });

    var sharePiePolorDoughnutData = [
        {
            value: 125,
            color: "rgba(38, 185, 154, 0.7)",
            highlight: "rgba(38, 185, 154, 0.7)",
            label: ""
        },
        {
            value: 25,
            color: "rgba(38, 185, 154, 0.01)",
            highlight: "rgba(38, 185, 154, 0.01)",
            label: ""
        }
    ];
    $(document).ready(function () {
        window.myDoughnut = new Chart(document.getElementById("canvas_doughnut").getContext("2d")).Doughnut(sharePiePolorDoughnutData, {
            responsive: true,
            scaleShowLabels: false,
            segmentStrokeWidth: 2,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });
    });
    $(document).ready(function () {
        window.myDoughnut = new Chart(document.getElementById("canvas_doughnut2").getContext("2d")).Doughnut(sharePiePolorDoughnutData, {
            responsive: true,
            scaleShowLabels: false,
            segmentStrokeWidth: 2,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });
    });
    $(document).ready(function () {
        window.myDoughnut = new Chart(document.getElementById("canvas_doughnut3").getContext("2d")).Doughnut(sharePiePolorDoughnutData, {
            responsive: true,
            scaleShowLabels: false,
            segmentStrokeWidth: 2,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });
    });
    $(document).ready(function () {
        window.myDoughnut = new Chart(document.getElementById("canvas_doughnut4").getContext("2d")).Doughnut(sharePiePolorDoughnutData, {
            responsive: true,
            scaleShowLabels: false,
            segmentStrokeWidth: 2,
            tooltipFillColor: "rgba(51, 51, 51, 0.55)"
        });
    });
</script>
<script>
    $('document').ready(function () {
        $(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
            type: 'bar',
            height: '40',
            barWidth: 9,
            colorMap: {
                '7': '#a1a1a1'
            },
            barSpacing: 2,
            barColor: '#26B99A'
        });

        $(".sparkline_two").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
            type: 'line',
            width: '200',
            height: '40',
            lineColor: '#26B99A',
            fillColor: 'rgba(223, 223, 223, 0.57)',
            lineWidth: 2,
            spotColor: '#26B99A',
            minSpotColor: '#26B99A'
        });
    })
</script>


</body>

</html>
