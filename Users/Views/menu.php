<?php //session_start(); ?>

<div class="col-md-3 left_col">
    <div class="left_col scroll-view">

        <div class="navbar nav_title" style="border: 0;">
            <a href="index.php" class="site_title"><img src="<?php echo "../assets/img/" . $setting['logo']; ?> "
                                                        width="40" height="40">
                <span><?php echo $setting['title'] ?></span></a>
        </div>
        <div class="clearfix"></div>

        <!-- menu prile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <?php
                if (isset($profile['image']) && !empty($profile['image'])) {
                    ?>
                    <img src="<?php echo "../assets/img/user/" . $profile['image']; ?>" alt="..."
                         class="img-circle profile_img">
                    <?php
                } else {
                    ?>
                    <img src="../assets/img/user/img.jpg" alt="..." class="img-circle profile_img">
                    <?php
                }
                ?>
            </div>
            <div class="profile_info">
                <span>Welcome</span>
                <h2>
                    <?php
                    echo $profile['name'];
                    ?>
                </h2>
            </div>
        </div>
        <!-- /menu prile quick info -->

        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

            <div class="menu_section">
                <h3><?php if ($result == "Admin") {
                        echo "Admin";
                    } elseif ($result == "User") {
                        echo "User";
                    } ?></h3>
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="display: none">
                            <li><a href="../index.php">Site Home</a>
                            </li>
                            <li><a href="index.php">Dashboard</a>
                            </li>
                        </ul>
                    </li>

                    <?php
                    if (isset($_SESSION['Admin'])) {
                        ?>
                        <li><a><i class="fa fa-users"></i> Users <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                                <li><a href="users.php">All Users</a>
                                </li>
                                <li><a href="users_active.php">Active Users</a>
                                </li>
                                <li><a href="users_deactive.php">Deactive Users</a>
                                </li>
                                <li><a href="create_user.php">Create New</a>
                                </li>
                            </ul>
                        </li>
                        <li><a href="settings.php"><i class="fa fa-gears"></i> Settings </a>
                        </li>
                        <li><a><i class="fa fa-recycle"></i> Recycle <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                                <li><a href="#..">Users</a>
                                </li>
                                <li><a href="#..">Contacts</a>
                                </li>
                                <li><a href="#..">Projects</a>
                                </li>
                                <li><a href="#..">Notes</a>
                                </li>
                            </ul>
                        </li>

                        <?php
                    } else {
                        ?>
                        <li><a><i class="fa fa-book"></i> Contacts <span
                                    class="label label-success pull-right">Upcoming</span> <span
                                    class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                                <li><a href="#">Groups</a>
                                </li>
                                <li><a href="#">Add Contact</a>
                                </li>
                                <li><a href="#">Contacts as List</a>
                                </li>
                                <li><a href="#">Favourit Contacts</a>
                                </li>
                                <li><a href="#">Contacts as Grouping</a>
                                </li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-flask"></i> Projects <span class="label label-success pull-right">Upcoming</span>
                                <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                                <li><a href="#">All projects</a>
                                </li>
                                <li><a href="#">Categories</a>
                                </li>
                                <li><a href="#">Finished</a>
                                </li>
                                <li><a href="#">Running</a>
                                </li>
                                <li><a href="#">Canceled</a>
                                </li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-newspaper-o"></i> Notes <span class="label label-success pull-right">Upcoming</span>
                                <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                                <li><a href="#">All Notes</a>
                                </li>
                                <li><a href="#">Create New</a>
                                </li>
                            </ul>
                        </li>
                        <?php
                    }
                    ?>
                    <li><a><i class="fa fa-user"></i> My Accounts <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu" style="display: none">
                            <li><a href="profile.php">Show Profile</a>
                            </li>
                            <li><a href="profileedit.php">Edit Profile</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

            <?php
            if (!isset($_SESSION['Admin'])) {
                ?>
                <div class="menu_section">
                    <h3>Help & Support</h3>
                    <ul class="nav side-menu">
                        <li><a><i class="fa fa-compass"></i> Tutorials <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                                <li><a href="#video">Video</a>
                                </li>
                                <li><a href="#docs">Documention</a>
                                </li>
                            </ul>
                        </li>
                        <li><a><i class="fa fa-support"></i> Help <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu" style="display: none">
                                <li><a href="#faq">FAQ</a>
                                </li>
                                <li><a href="#support">Support</a>
                                </li>
                                <li><a href="#contact">Contact us</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
                <?php
            }
            ?>

        </div>
        <!-- /sidebar menu -->

        <!-- /menu footer buttons -->
        <div class="sidebar-footer hidden-small">
            <marquee direction="left" loop="200">Under Construction Process</marquee>
        </div>
        <!-- /menu footer buttons -->
    </div>
</div>

<!-- top navigation -->
<div class="top_nav">

    <div class="nav_menu">
        <nav class="" role="navigation">
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                       aria-expanded="false">
                        <?php
                        if (isset($profile['image']) && !empty($profile['image'])) {
                            ?>
                            <img src="<?php echo "../assets/img/user/" . $profile['image']; ?>" alt="">
                            <?php
                        } else {
                            ?>
                            <img src="../assets/img/user/img.jpg" alt="">
                            <?php
                        }

                        echo $profile['name'];
                        ?>
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                        <li><a href="profile.php"> Profile</a>
                        </li>

                        <?php
                        if (isset($_SESSION['Admin'])) {
                            ?>

                            <li>
                                <a href="settings.php">
                                    <span class="badge bg-red pull-right">50%</span>
                                    <span>Settings</span>
                                </a>
                            </li>

                            <?php
                        }
                        ?>

                        <li><a href="edit_me.php"></i> Change Password</a>
                        </li>
                        <li><a href="ac_logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                        </li>
                    </ul>
                </li>


            </ul>
        </nav>
    </div>
</div>
<!-- /top navigation -->

