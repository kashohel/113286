<?php
//session_start();
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../vendor/autoload.php';
use App\Users;
$obj = new Users();
$data = $obj->profile_view();
$result = $obj->control();
$profile = $obj->profile_view();
$user = $obj->user_view();
$setting = $obj->setting();
if ($result == "Admin" ) {
} else {
    $_SESSION['Message_Err'] = "Access Forbidden.";
    header('location:../index.php');
}

if (!isset($data['id']) || empty($data['id'])) {
    header('location:profilein.php');
} else {
}




if (!empty($profile['first_name']) || !empty($profile['last_name'])) {
    $profile['name'] = $profile['first_name'] . " " . $profile['last_name'];
} else {
    $profile['name'] = $user['username'];
}


//print_r($user);
//echo $user['username'];
//print_r($profile);
//echo $profile['name'];
//echo $result;
//die();
if ($result == "User") {
//    echo "Login as User.";
} elseif ($result == "Admin") {
//    echo "Login as Admin";
} else {
    header('location:login.php');
}

//if (isset($_SESSION['Message'])) {
//    echo $_SESSION['Message'];
//    unset ($_SESSION['Message']);
//}


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $setting['title'] ;?> | Settings</title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">



    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">



        <!-- top navigation -->
        <?php
        include_once 'menu.php';
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2 align="center">Personal Information <small>form</small></h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">

                                <form action="ac_setting.php" method="post" enctype="multipart/form-data" class="form-horizontal form-label-left" novalidate>

                                    <p>Modify Setting information. <code><strong>*  is require</strong> </code></p>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Site Title <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="title" name="title" value="<?php echo $setting['title']; ?>" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2"  placeholder="First part of name" required="required" type="text">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="subtitle">Subtitle <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="subtitle" name="subtitle" value="<?php echo $setting['subtitle']; ?>" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2"  placeholder="Another part of name" required="required" type="text">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">System Email <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="email" name="email" value="<?php echo $setting['email']; ?>" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2"  placeholder="Another part of name" required="required" type="email">
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="reg_permission">Registration Permission <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <p>
                                                Enable Registration:<input type="radio" class="flat" name="reg_permission" id="reg_permission" value="1"<?php if ($setting['reg_permission'] == 1){echo 'checked';} ?> required />
                                                Disable Registration:<input type="radio" class="flat" name="reg_permission" id="reg_permission" value="0"<?php if ($setting['reg_permission'] == 0){echo 'checked';} ?> />
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_active">After Registration <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <p>
                                                Automatic approve:<input type="radio" class="flat" name="user_active" id="user_active" value="1"<?php if ($setting['user_active'] == 1){echo 'checked';}?> required />
                                                Email verification:<input type="radio" class="flat" name="user_active" id="genderF" disabled="disabled" value=""<?php if ($setting['title'] == 2){echo 'checked';}?> />
                                                Manual approve by admin:<input type="radio" class="flat" name="user_active" id="user_active" value="0"<?php if ($setting['user_active'] == 0){echo 'checked';}?> />
                                            </p>
                                        </div>
                                    </div>
                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="footer">Footer Text <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input id="footer" type="text" name="footer" value='<?php echo $setting['footer']; ?>'  placeholder="Job title" data-validate-length-range="5,20" class="optional form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>

                                    <div class="item form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="image">Site Logo <span class="required">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="hidden" id="oldimage" name="oldimage" value="<?php echo $setting['logo']; ?>" class="form-control col-md-7 col-xs-12">
                                            <img src="<?php echo "../assets/img/".$setting['logo'];?>" width="100" height="100" alt="..." class="...">
                                            <input type="file" id="logo" required="required" name="logo" value="" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <button type="reset" class="btn btn-primary">Cancel</button>
                                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- footer content -->
            <?php
            include_once 'footer.php';
            ?>
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>

</div>

<script src="js/bootstrap.min.js"></script>

<!-- bootstrap progress js -->
<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="js/icheck/icheck.min.js"></script>

<script src="js/custom.js"></script>

<!-- image cropping -->
<script src="js/cropping/cropper.min.js"></script>
<script src="js/cropping/main2.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#reservation').daterangepicker(null, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
    });
</script>


</body>

</html>