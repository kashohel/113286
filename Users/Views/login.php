<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../vendor/autoload.php';
use App\Users;
$obj = new Users();
$setting = $obj->setting();
//if (isset($_SESSION['Message'])){
//    echo $_SESSION['Message'];
//    unset($_SESSION['Message']);
//}
if (isset($_SESSION['User'])){
    header('location:index.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $setting['title'] ;?> | Login & Registration</title>

    <!--    Notification-->
    <link rel="stylesheet" type="text/css" href="../assets/notification/notification.css">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">


    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body style="background:#F7F7F7;">

<!--Notification-->
<?php
if (isset($_SESSION['Login_Notice'])){
    $message = $_SESSION['Login_Notice'];
?>
    <script>onload = function (){$.notification.show('other','<?php echo $message; ?>');} </script>
    <?php
    unset($_SESSION['Login_Notice']);
}elseif (isset($_SESSION['Login_Fail'])){
    $message = $_SESSION['Login_Fail'];
    ?>
    <script>onload = function (){$.notification.show('error','<?php echo $message; ?>');} </script>
    <?php
    unset($_SESSION['Login_Fail']);
}
?>
<!--Notification- END -->

    <div class="">
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>

        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">
                    <form action="ac_login.php" method="post">
                        <h1>Login Form</h1>
                        <div>
                            <input type="text" name="username" class="form-control" placeholder="Username" required="" />
                        </div>
                        <div>
                            <input type="password" name="password" class="form-control" placeholder="Password" required="" />
                        </div>
                        <div>
                            <input class="btn btn-default submit" type="submit" value="Log in">
                            <a class="reset_pass" href="#">Lost your password?</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <?php if ($setting['reg_permission'] == 1){ ?>

                            <p class="change_link">New to site?
                                <a href="#toregister" class="to_register"> Create Account </a>
                            </p>

                            <?php } ?>

                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <h1><i class="fa fa-paw" style="font-size: 26px;"></i> <?php echo $setting['title'] ?></h1>

                                <p><?php echo $setting['footer'] ?></p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>

            <?php if ($setting['reg_permission'] == 1){ ?>

            <div id="register" class="animate form">
                <section class="login_content">
                    <form action="ac_register.php" method="post">
                        <h1>Create Account</h1>
                        <div>
                            <input type="text" name="username" class="form-control" placeholder="Username" required="" />
                        </div>
                        <div>
                            <input type="email" name="email" class="form-control" placeholder="Email" required="" />
                        </div>
                        <div>
                            <input type="password" name="password" class="form-control" placeholder="Password" required="" />
                        </div>
                        <div>
                            <input type="submit" class="btn btn-default submit" value="Register">
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <p class="change_link">Already a member ?
                                <a href="#tologin" class="to_register"> Log in </a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <h1><i class="fa fa-paw" style="font-size: 26px;"></i> <?php echo $setting['title'] ?></h1>

                                <p><?php echo $setting['footer'] ?></p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>

            <?php }else{ ?>

                <div id="register" class="animate form">
                    <section class="login_content">
                        <form action="ac_register.php" method="post">
                            <h1>Create Account Disable</h1>
                            <p>
                                Create new account is now disable by administrator.<br/>
                                If you have already account please login.<br/>
                                Otherwise contact with us.
                            </p>
                            <div class="clearfix"></div>
                            <div class="separator">

                                <p class="change_link">Already a member ?
                                    <a href="#tologin" class="to_register"> Log in </a>
                                </p>
                                <div class="clearfix"></div>
                                <br />
                                <div>
                                    <h1><i class="fa fa-paw" style="font-size: 26px;"></i> <?php echo $setting['title'] ?></h1>

                                    <p><?php echo $setting['footer'] ?></p>
                                </div>
                            </div>
                        </form>
                        <!-- form -->
                    </section>
                    <!-- content -->
                </div>

            <?php } ?>

        </div>
    </div>


    <!--Nitification Script-->
    <script src="../assets/notification/jquery.notification.main.js"></script>
    <script src="../assets/notification/jquery.notification.min.js"></script>
</body>

</html>