<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../vendor/autoload.php';
use App\Users;

$obj = new Users();
$result = $obj->control();
$profile = $obj->profile_view();
$user = $obj->user_view();
$setting = $obj->setting();
$Users = $obj->all_deactive_users();

if (!empty($profile['first_name']) || !empty($profile['last_name'])) {
    $profile['name'] = $profile['first_name'] . " " . $profile['last_name'];
} else {
    $profile['name'] = $user['username'];
}
//print_r($user);
//echo $user['username'];
//print_r($profile);
//echo $profile['name'];
//echo $result;
//die();

if ($result != "Admin") {
    $_SESSION['Message_Err'] = "Unexpected entry request.";
    header('location:index.php');
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $setting['title'] ;?> | Deactive Users</title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">
    <link href="css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">

    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">

        <!-- top navigation -->
        <?php
        include_once 'menu.php';
        ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="clearfix"></div>

                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>All users
                                    <small>List</small>
                                </h2>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <table id="example" class="table table-striped responsive-utilities jambo_table">
                                    <thead>
                                    <tr class="headings">
                                        <th>Serial</th>
                                        <th>Full Name</th>
                                        <th>username</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Type</th>
                                        <th class=" no-link last"><span class="nobr">Actions</span>
                                        </th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <?php
                                    if (isset($Users) && !empty($Users)) {
                                        $serial = 0;
                                        foreach ($Users as $data) {
                                            $serial++

                                            ?>
                                            <tr class="even pointer">
                                                <td class="a-center ">
                                                    <?php
                                                    echo $serial;
                                                    ?>
                                                </td>
                                                <td class=" "><?php
                                                    $profile = $obj->profile_view_id($data['id']);
                                                    $name = $profile['first_name']." ".$profile['last_name'];
                                                    echo $name;
                                                    ?></td>
                                                <td class=" "><?php echo $data['username']; ?></td>
                                                <td class=" "><?php echo $data['email']; ?></td>
                                                <td class=" "><?php if ($data['is_active'] == '1') { ?>
                                                        <span
                                                            class="label label-success pull-center"><?php echo "Active"; ?></span>
                                                        <?php
                                                    } elseif ($data['is_active'] == '0') { ?>
                                                        <span
                                                            class="label label-warning pull-center"><?php echo "Deactive"; ?></span>
                                                        <?php } ?>
                                                </td>
                                                <td class=" "><?php if ($data['is_admin']) {
                                                        echo "Admin";
                                                    } else {
                                                        echo "User";
                                                    } ?></td>
                                                <td class=" last">
                                                    <?php
                                                    if (isset($profile['id']) && !empty($profile['id'])){
                                                        ?>
                                                        <a href="show-profile.php?id=<?php echo $data['uniqid'] ?>">Show</a>
                                                        <?php
                                                    }else {
                                                        ?>
                                                        <a href="user_profilein.php?id=<?php echo $data['uniqid'] ?>">Create</a>
                                                    <?php } ?>|
                                                    <a href="edit_user.php?id=<?php echo $data['uniqid'] ?>">Edit</a> |
                                                    <a href="trash_user.php?id=<?php echo $data['uniqid'] ?>">Delete</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>

                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>

                    <br/>
                    <br/>
                    <br/>

                </div>
            </div>
            <!-- footer content -->
            <?php
            include_once 'footer.php';
            ?>
            <!-- /footer content -->

        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="js/icheck/icheck.min.js"></script>

<script src="js/custom.js"></script>


<!-- Datatables -->
<script src="js/datatables/js/jquery.dataTables.js"></script>
<script src="js/datatables/tools/js/dataTables.tableTools.js"></script>
<script>
    $(document).ready(function () {
        $('input.tableflat').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass: 'iradio_flat-green'
        });
    });

    var asInitVals = new Array();
    $(document).ready(function () {
        var oTable = $('#example').dataTable({
            "oLanguage": {
                "sSearch": "Search all columns:"
            },
            "aoColumnDefs": [
                {
                    'bSortable': false,
                    'aTargets': [0]
                } //disables sorting for column one
            ],
            'iDisplayLength': 12,
            "sPaginationType": "full_numbers",
            "dom": 'T<"clear">lfrtip',
            "tableTools": {
                "sSwfPath": "<?php echo base_url('assets2/js/Datatables/tools/swf/copy_csv_xls_pdf.swf'); ?>"
            }
        });
        $("tfoot input").keyup(function () {
            /* Filter on the column based on the index of this element's parent <th> */
            oTable.fnFilter(this.value, $("tfoot th").index($(this).parent()));
        });
        $("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });
        $("tfoot input").focus(function () {
            if (this.className == "search_init") {
                this.className = "";
                this.value = "";
            }
        });
        $("tfoot input").blur(function (i) {
            if (this.value == "") {
                this.className = "search_init";
                this.value = asInitVals[$("tfoot input").index(this)];
            }
        });
    });
</script>
</body>

</html>