<?php
include_once '../vendor/autoload.php';
use App\Users;

$obj = new Users();
//$user = $obj->user_view();
//echo $user['is_active'];
if (isset($_GET['active'])) {
    $data = $_GET['active'];
    $obj->activation($data);
} elseif (isset($_GET['deactive'])) {
    $data = $_GET['deactive'];
    $obj->deactivation($data);
} else {
    header('location:users.php');
}

?>