<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../vendor/autoload.php';
use App\Users;

$obj = new Users();
$result = $obj->control();
$profile = $obj->profile_view();

if ($profile['is_public'] == '1'){
    $id = '0';
} else {
    $id = '1';
}
$obj->privacy($id);
