<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../vendor/autoload.php';
use App\Users;

$obj = new Users();
$result = $obj->control();
$profile = $obj->profile_view();
$user = $obj->user_view();
$setting = $obj->setting();
$user_id = $obj->user_view($_GET['id'])['id'];
$data = $obj->profile_view($user_id);
$public = $obj->all_users_public();

if (!empty($profile['first_name']) || !empty($profile['last_name'])) {
    $profile['name'] = $profile['first_name'] . " " . $profile['last_name'];
} else {
    $profile['name'] = $user['username'];
}


if ($result == "Admin") {
//    echo "Login as Admin";
} else {
    header('location:login.php');
}


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $setting['title'] ;?> | Profile show</title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/icheck/flat/green.css" rel="stylesheet">


    <script src="js/jquery.min.js"></script>

    <!--[if lt IE 9]>
    <script src="../assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>


<body class="nav-md">

<div class="container body">


    <div class="main_container">
        <?php
        include_once 'menu.php';
        ?>
        <!-- page content -->
        <div class="right_col" role="main">

            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>User Profile</h3>
                    </div>

                </div>
                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">

                            <div class="x_content">

                                <div class="col-md-3 col-sm-3 col-xs-12 profile_left">

                                    <div class="profile_img">

                                        <!-- end of image cropping -->
                                        <div id="crop-avatar">
                                            <!-- Current avatar -->
                                            <div class="avatar-view" title="Change the avatar">
                                                <img src="<?php echo "../assets/img/user/".$data['image']; ?>" alt="Avatar">
                                            </div>

                                            <!-- Cropping modal -->
                                            <div class="modal fade" id="avatar-modal" aria-hidden="true"
                                                 aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <form class="avatar-form" action="crop.php"
                                                              enctype="multipart/form-data" method="post">
                                                            <div class="modal-header">
                                                                <button class="close" data-dismiss="modal"
                                                                        type="button">&times;</button>
                                                                <h4 class="modal-title" id="avatar-modal-label">Change
                                                                    Avatar</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="avatar-body">

                                                                    <!-- Upload image and data -->
                                                                    <div class="avatar-upload">
                                                                        <input class="avatar-src" name="avatar_src"
                                                                               type="hidden">
                                                                        <input class="avatar-data" name="avatar_data"
                                                                               type="hidden">
                                                                        <label for="avatarInput">Local upload</label>
                                                                        <input class="avatar-input" id="avatarInput"
                                                                               name="avatar_file" type="file">
                                                                    </div>

                                                                    <!-- Crop and preview -->
                                                                    <div class="row">
                                                                        <div class="col-md-9">
                                                                            <div class="avatar-wrapper"></div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div
                                                                                class="avatar-preview preview-lg"></div>
                                                                            <div
                                                                                class="avatar-preview preview-md"></div>
                                                                            <div
                                                                                class="avatar-preview preview-sm"></div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row avatar-btns">
                                                                        <div class="col-md-9">
                                                                            <div class="btn-group">
                                                                                <button class="btn btn-primary"
                                                                                        data-method="rotate"
                                                                                        data-option="-90" type="button"
                                                                                        title="Rotate -90 degrees">
                                                                                    Rotate Left
                                                                                </button>
                                                                                <button class="btn btn-primary"
                                                                                        data-method="rotate"
                                                                                        data-option="-15" type="button">
                                                                                    -15deg
                                                                                </button>
                                                                                <button class="btn btn-primary"
                                                                                        data-method="rotate"
                                                                                        data-option="-30" type="button">
                                                                                    -30deg
                                                                                </button>
                                                                                <button class="btn btn-primary"
                                                                                        data-method="rotate"
                                                                                        data-option="-45" type="button">
                                                                                    -45deg
                                                                                </button>
                                                                            </div>
                                                                            <div class="btn-group">
                                                                                <button class="btn btn-primary"
                                                                                        data-method="rotate"
                                                                                        data-option="90" type="button"
                                                                                        title="Rotate 90 degrees">Rotate
                                                                                    Right
                                                                                </button>
                                                                                <button class="btn btn-primary"
                                                                                        data-method="rotate"
                                                                                        data-option="15" type="button">
                                                                                    15deg
                                                                                </button>
                                                                                <button class="btn btn-primary"
                                                                                        data-method="rotate"
                                                                                        data-option="30" type="button">
                                                                                    30deg
                                                                                </button>
                                                                                <button class="btn btn-primary"
                                                                                        data-method="rotate"
                                                                                        data-option="45" type="button">
                                                                                    45deg
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <button
                                                                                class="btn btn-primary btn-block avatar-save"
                                                                                type="submit">Done
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="modal-footer">
                                              <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                                            </div> -->
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.modal -->

                                            <!-- Loading state -->
                                            <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                                        </div>
                                        <!-- end of image cropping -->

                                    </div>
                                    <h3><?php echo $data['first_name']." ".$data['last_name'];?></h3>

                                    <ul class="list-unstyled user_data">
                                        <li><i class="fa fa-birthday-cake"></i> <?php echo $data['birthday']?>
                                        </li>

                                        <li>
                                            <i class="fa fa-briefcase user-profile-icon"></i> <?php echo $data['occupation']?>
                                        </li>

                                        <li class="m-top-xs">
                                            <i class="fa fa-user"></i>
                                            <?php echo $data['gender']?>
                                        </li>
                                    </ul>

                                    <a href="profileedit.php" class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Edit Profile</a>
                                    <br/>


                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-12">

                                    <div class="profile_title">

                                    </div>


                                    <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#tab_content1" id="home-tab"
                                                                                      role="tab" data-toggle="tab"
                                                                                      aria-expanded="true">Profile Information</a>
                                            </li>
                                            <li role="presentation" class=""><a href="#tab_content2" role="tab"
                                                                                id="profile-tab" data-toggle="tab"
                                                                                aria-expanded="false">Public users</a>
                                            </li>
                                            <li role="presentation" class=""><a href="#tab_content3" role="tab"
                                                                                id="profile-tab2" data-toggle="tab"
                                                                                aria-expanded="false">Privacy</a>
                                            </li>
                                        </ul>
                                        <div id="myTabContent" class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1"
                                                 aria-labelledby="home-tab">

                                                <!-- start Profile -->
                                                <ul class="messages">

                                                    <li>
                                                        <div class="message_wrapper">
                                                            <blockquote class="message">
                                                                <p>Full Name</p>
                                                            </blockquote><br/>
                                                            <h4 class="heading"> <?php
                                                                echo $data['first_name']." ".$data['last_name'];
                                                                ?>
                                                            </h4>

                                                            <br/>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="message_wrapper">
                                                            <blockquote class="message">
                                                                <h4 class="heading">Phone Numbers</h4>
                                                            </blockquote><br/>
                                                            <p>
                                                                <?php
                                                                echo "Personal :".$data['personal_phone']."<br>";
                                                                echo "Home     :".$data['personal_phone']."<br>";
                                                                echo "Office    :".$data['personal_phone'];
                                                                ?>
                                                            </p>

                                                            <br/>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="message_wrapper">
                                                            <blockquote class="message">
                                                                <h4 class="heading">Contact Address</h4>
                                                            </blockquote><br/>
                                                            <p>
                                                                <?php
                                                                echo "Present :".$data['present_address']."<br>";
                                                                echo "Permanent    :".$data['permanent_address'];
                                                                ?>
                                                            </p>

                                                            <br/>
                                                        </div>
                                                    </li>

                                                </ul>
                                                <!-- end Profile -->

                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="tab_content2"
                                                 aria-labelledby="profile-tab">

                                                <!-- start Public Users -->
                                                <table class="data table table-striped no-margin">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Full Name</th>
                                                        <th>Phone Number</th>
                                                        <th class="hidden-phone">Gender</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    if (isset($public) && !empty($public)) {
                                                        $serial = 0;
                                                        foreach ($public as $data) {
                                                            $serial++

                                                            ?>

                                                            <tr>
                                                                <td><?php echo $serial; ?></td>
                                                                <td><?php echo $data['first_name']." ".$data['last_name']; ?></td>
                                                                <td><?php echo $data['personal_phone']; ?></td>
                                                                <td class="hidden-phone"><?php echo $data['gender']; ?></td>
                                                                <td class="vertical-align-mid">
                                                                    <a href="<?php
                                                                    echo "public-profile.php?id=".$data['id'];
                                                                    ?>"> View </a>
                                                                </td>
                                                            </tr>
                                                        <?php }} ?>

                                                    </tbody>
                                                </table>
                                                <!-- end user projects -->

                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="tab_content3"
                                                 aria-labelledby="profile-tab">
                                                <!-- start Privacy -->
                                                <p>You can make your profile private of public. Public profile can view by other users.
                                                    If you think that you won't face any problem you can make it public. Other wise meke
                                                    if private for batter security. By default we make it private.</p>
                                                <div class="col-xs-12">
                                                    <?php
                                                    if ($data['is_public'] == '1'){
                                                        ?>
                                                        <a href="ac_privacy.php?"><button class="btn btn-success pull-center"> Account is now <b>PUBLIC</b>. Click to make private</button></a>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <a href="ac_privacy.php"><button class="btn btn-success pull-center"> Account is now <b>PRIVATE</b>. Click to make public</button></a>
                                                    <?php } ?>

                                                </div>
                                                <!-- end Privacy -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- footer content -->
                <?php include_once 'footer.php'; ?>
                <!-- /footer content -->
            </div>


        </div>
        <!-- /page content -->
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>

<script src="js/bootstrap.min.js"></script>

<!-- chart js -->
<script src="js/chartjs/chart.min.js"></script>
<!-- bootstrap progress js -->
<script src="js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
<!-- icheck -->
<script src="js/icheck/icheck.min.js"></script>

<script src="js/custom.js"></script>

<!-- image cropping -->
<script src="js/cropping/cropper.min.js"></script>
<script src="js/cropping/main.js"></script>


<!-- daterangepicker -->
<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
<!-- moris js -->
<script src="js/moris/raphael-min.js"></script>
<script src="js/moris/morris.js"></script>
<script>
    $(function () {
        var day_data = [
            {
                "period": "Jan",
                "Hours worked": 80
            },
            {
                "period": "Feb",
                "Hours worked": 125
            },
            {
                "period": "Mar",
                "Hours worked": 176
            },
            {
                "period": "Apr",
                "Hours worked": 224
            },
            {
                "period": "May",
                "Hours worked": 265
            },
            {
                "period": "Jun",
                "Hours worked": 314
            },
            {
                "period": "Jul",
                "Hours worked": 347
            },
            {
                "period": "Aug",
                "Hours worked": 287
            },
            {
                "period": "Sep",
                "Hours worked": 240
            },
            {
                "period": "Oct",
                "Hours worked": 211
            }
        ];
        Morris.Bar({
            element: 'graph_bar',
            data: day_data,
            xkey: 'period',
            hideHover: 'auto',
            barColors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
            ykeys: ['Hours worked', 'sorned'],
            labels: ['Hours worked', 'SORN'],
            xLabelAngle: 60
        });
    });
</script>
<!-- datepicker -->
<script type="text/javascript">
    $(document).ready(function () {

        var cb = function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
        }

        var optionSet1 = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2015',
            dateLimit: {
                days: 60
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        };
        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
        $('#reportrange').daterangepicker(optionSet1, cb);
        $('#reportrange').on('show.daterangepicker', function () {
            console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function () {
            console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
        });
        $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
            console.log("cancel event fired");
        });
        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });
        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });
        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });
    });
</script>
<!-- /datepicker -->
</body>

</html>



