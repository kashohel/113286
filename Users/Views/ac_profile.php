<?php

include_once '../vendor/autoload.php';
use App\Users;
$obj = new Users();

//Get all uploaded file datas.
$file_name = $_FILES['image']['name'];
$file_type = $_FILES['image']['type'];
$file_temp = $_FILES['image']['tmp_name'];
$file_error = $_FILES['image']['error'];
$file_size = $_FILES['image']['size'];

//Process file extension and modifies file unique name
$data = explode('.', $file_name);
$file_ext = strtolower(end($data));
$image_name = time().$file_name;

//File validation check using file extension.
$my_ext = array('jpeg', 'jpg', 'gif', 'png');
if (in_array($file_ext, $my_ext) === false)
{
    echo "Error message show.";

} else {

    $_POST['image']= $image_name;
    move_uploaded_file($file_temp,"../assets/img/user/".$image_name);
}

//print_r($_POST);
//die();

$obj->prepare($_POST);
$obj->profile_store();