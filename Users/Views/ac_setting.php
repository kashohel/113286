<?php

include_once '../vendor/autoload.php';
use App\Users;
$obj = new Users();

//Get all uploaded file datas.
$file_name = $_FILES['logo']['name'];
$file_type = $_FILES['logo']['type'];
$file_temp = $_FILES['logo']['tmp_name'];
$file_error = $_FILES['logo']['error'];
$file_size = $_FILES['logo']['size'];

//Process file extension and modifies file unique name
$data = explode('.', $file_name);
$file_ext = strtolower(end($data));
$image_name = "logo.".$file_ext;

//File validation check using file extension.
$my_ext = array('jpeg', 'jpg', 'gif', 'png');
if (in_array($file_ext, $my_ext) === false)
{
    echo "Error message show.";

} else {

    $_POST['logo']= $image_name;
    unlink("../assets/img/".$_POST['oldimage']);
    move_uploaded_file($file_temp,"../assets/img/logo.".$file_ext);
}

//print_r($_POST);
//die();
$obj->settings($_POST);