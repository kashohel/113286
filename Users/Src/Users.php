<?php
namespace App;
session_start();

class Users
{
    public $id = '';
    public $uniqid = '';
    public $user_id = '';
    public $name = '';
    public $first_name = '';
    public $last_name = '';
    public $username = '';
    public $email = '';
    public $password = '';
    public $is_admin = '';
    public $is_active = '';
    public $group = '';
    public $personal_phone = '';
    public $home_phone = '';
    public $office_phone = '';
    public $present_address = '';
    public $permanent_address = '';
    public $gender = '';
    public $occupation = '';
    public $birthday = '';
    public $is_public = '';
    public $details = '';
    public $image = '';
    public $oldimage = '';


    public function __construct()
    {
        $conn = mysql_connect('localhost', 'root', '') or die ("Unable to connect with DB server.");
        mysql_select_db('users') or die ("Unable to connect with Database.");
    }

    public function prepare($data = '')
    {
//        print_r($data);
//        die();

        if (isset($data['id']) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (isset($data['uniqid']) && !empty($data['uniqid'])) {
            $this->uniqid = $data['uniqid'];
        } else {
            $this->uniqid = uniqid();
        }
        if (isset($data['user_id']) && !empty($data['user_id'])) {
            $this->user_id = $data['user_id'];
        }
        if (isset($data['group']) && !empty($data['group'])) {
            $this->group = $data['group'];
        }
        if (isset($data['username']) && !empty($data['username'])) {
            $this->username = $data['username'];
        }
        if (isset($data['email']) && !empty($data['email'])) {
            $this->email = $data['email'];
        }
        if (isset($data['password']) && !empty($data['password'])) {
            $this->password = $data['password'];
        }
        if (isset($data['name']) && !empty($data['name'])) {
            $this->name = $data['name'];
        }
        if (isset($data['details']) && !empty($data['details'])) {
            $this->details = $data['details'];
        }
        if (isset($data['first_name']) && !empty($data['first_name'])) {
            $this->first_name = $data['first_name'];
        }
        if (isset($data['last_name']) && !empty($data['last_name'])) {
            $this->last_name = $data['last_name'];
        }
        if (isset($data['personal_phone']) && !empty($data['personal_phone'])) {
            $this->personal_phone = $data['personal_phone'];
        }
        if (isset($data['home_phone']) && !empty($data['home_phone'])) {
            $this->home_phone = $data['home_phone'];
        }
        if (isset($data['office_phone']) && !empty($data['office_phone'])) {
            $this->office_phone = $data['office_phone'];
        }
        if (isset($data['present_address']) && !empty($data['present_address'])) {
            $this->present_address = $data['present_address'];
        }
        if (isset($data['permanent_address']) && !empty($data['permanent_address'])) {
            $this->permanent_address = $data['permanent_address'];
        }
        if (isset($data['gender']) && !empty($data['gender'])) {
            $this->gender = $data['gender'];
        }
        if (isset($data['occupation']) && !empty($data['occupation'])) {
            $this->occupation = $data['occupation'];
        }
        if (isset($data['birthday']) && !empty($data['birthday'])) {
            $this->birthday = $data['birthday'];
        }
        if (isset($data['image']) && !empty($data['image'])) {
            $this->image = $data['image'];
        }
        if (isset($data['oldimage']) && !empty($data['oldimage'])) {
            $this->oldimage = $data['oldimage'];
        }

    }

    public function reg_check()
    {

        $query = "SELECT * FROM `users` WHERE `email` = '" . $this->email . "' OR `username` = '" . $this->username . "'";
        $run_query = mysql_query($query);
        $result = mysql_fetch_assoc($run_query);
        $username = $result['username'];
        $email = $result['email'];
//        die();
        if (!isset($this->username) || empty($this->email) || empty($this->password)) {
            $_SESSION['Login_Fail'] = "Please fill up all fields.";
            header('location:login.php#toregister');
        } elseif (($this->username == $username) || ($this->email == $email)) {
            $_SESSION['Login_Notice'] = "You are already a member. So try to login";
            header('location:login.php');
        } else {
            return "Success";
        }
    }

    public function register()
    {
        $set = "SELECT * FROM `settings`";
        $sett = mysql_query($set);
        $setting = mysql_fetch_array($sett);

        $query = "INSERT INTO `users`.`users` (`uniqid`, `username`, `email`, `password`, `is_active`) VALUES ('" . $this->uniqid . "', '" . $this->username . "', '" . $this->email . "', '" . md5($this->password) . "', '" . $setting['user_active'] . "')";
//        echo $query;
//        die();
        if (mysql_query($query)) {

            if (isset($_SESSION['Admin'])) {
                $_SESSION['Message'] = "New uer created successfully.";
                header('location:users.php');
            } elseif ($setting['user_active'] == 1) {
                $_SESSION['Login_Notice'] = "Registration complete. Just login.";
                header('location:login.php');
            } else {
                $_SESSION['Message'] = "Registration complete. Wait for approval.";
                header('location:../index.php');
            }
        } else {
            $_SESSION['Message'] = "Registration fail. Please try again.";
        }

    }

    public function update_user()
    {
//        print_r($this);
//        die();
        if (isset($this->id) && !empty($this->email && !empty($this->password))) {
            $query = "UPDATE `users`.`users` SET `email` = '" . $this->email . "', `password` = '" . md5($this->password) . "' WHERE `users`.`uniqid` = '" . $this->uniqid . "'";
//            echo $query;
//            die();
            mysql_query($query);
            if (isset($_SESSION['Admin'])) {
                $_SESSION['Message'] = "Your details successfully updated.";
                header('location:users.php');
            } else {
                $_SESSION['Message'] = "Your details successfully updated.";
                header('location:index.php');
            }
        } else {
            $_SESSION['Message'] = "Please fill up all field to register.";
        }


    }

    public function update_user_id($id = '')
    {
//        print_r($this);
//        die();
        if (isset($this->id) && !empty($this->email && !empty($this->password))) {
            $query = "UPDATE `users`.`users` SET `email` = '" . $this->email . "', `password` = '" . md5($this->password) . "' WHERE `users`.`id` = '" . $this->id . "'";
            mysql_query($query);
            $_SESSION['Message'] = "User details successfully updated.";
            header('location:users.php');
        } else {
            $_SESSION['Message'] = "Please fill up all field to register.";
            header('location:edit_user.php');
        }

    }

    public function all_users()
    {
        $allUser = array();
        $query = "SELECT * FROM `users` WHERE `deleted_at` IS NULL";
        $run_query = mysql_query($query);
        while ($user = mysql_fetch_assoc($run_query)) {
            @$allUser[] = $user;
        }
        return $allUser;
    }

    public function all_users_public()
    {
        $allUser = array();
        $query = "SELECT * FROM `profiles` WHERE (`is_public` = '1' AND `deleted_at` IS NULL)";
        $run_query = mysql_query($query);
        while ($user = mysql_fetch_assoc($run_query)) {
            @$allUser[] = $user;
        }
        return $allUser;
    }

    public function all_active_users()
    {
        $allUser = array();
        $query = "SELECT * FROM `users` WHERE (`deleted_at` IS NULL AND `is_active` = '1')";
        $run_query = mysql_query($query);
        while ($user = mysql_fetch_assoc($run_query)) {
            @$allUser[] = $user;
        }
        return $allUser;
    }

    public function all_deactive_users()
    {
        $allUser = array();
        $query = "SELECT * FROM `users` WHERE (`deleted_at` IS NULL AND `is_active` = '0')";
        $run_query = mysql_query($query);
        while ($user = mysql_fetch_assoc($run_query)) {
            @$allUser[] = $user;
        }
        return $allUser;
    }

    public function user_view($id = '')
    {
        if (isset($id) && !empty($id)) {
            $query = "SELECT * FROM `users` WHERE `uniqid` = '" . $id . "'";
        } else {
            $query = "SELECT * FROM `users` WHERE `id` = '" . $_SESSION['User'] . "'";
        }
        $run_query = mysql_query($query);
        $result = mysql_fetch_assoc($run_query);
        return $result;
    }

    public function profile_view_id($uniqid = '')
    {
        $query = "SELECT * FROM `profiles` WHERE `user_id` = '" . $uniqid . "'";
        $run_query = mysql_query($query);
        $result = mysql_fetch_assoc($run_query);
        return $result;
    }

    public function login()
    {
//        print_r($data['username']);
//        print_r($data['password']);
//        die();
        $query = "SELECT * FROM `users` WHERE (`username`= '" . $this->username . "')";
        $run_query = mysql_query($query);

//        echo $query;
//        die();

        $result = mysql_fetch_assoc($run_query);

        $userid = $result['id'];
        $username = $result['username'];
        $email = $result['email'];
        $password = $result['password'];
        $active = $result['is_active'];
        $admin = $result['is_admin'];

        $input_pass = md5($this->password);

//        echo $userid."  ".$password;
//        die();

        if ((($input_pass == $password) && ($this->username == $username) && ($active == 1)) || (($input_pass == $password) && ($this->username == $email) && ($active == 1))) {
            $_SESSION['User'] = $userid;
            if ($admin == 1) {
                $_SESSION['Admin'] = "admin";
                $_SESSION['Message'] = "Successfully login as Admin";
            } else {
                $_SESSION['Message'] = "Successfully login as User";
            }
            header('location:index.php');
        } elseif ((($input_pass == $password) && ($this->username == $username) && ($active == 0)) || (($input_pass == $password) && ($this->username == $email) && ($active == 0))) {
            $_SESSION['Login_Notice'] = "Wait for permission.";
            header('location:login.php');
        } elseif ($this->username == $username) {
            $_SESSION['Login_Fail'] = "Incorrect Password.";
            header('location:login.php');
        } elseif ($input_pass == $password) {
            $_SESSION['Login_Fail'] = "Incorrect Username.";
            header('location:login.php');
        } else {
            $_SESSION['Login_Fail'] = "Incorrect information.";
            header('location:login.php');
        }

    }

    public function control()
    {
        if (isset($_SESSION['User']) && isset($_SESSION['Admin'])) {
            return "Admin";
        } elseif (isset($_SESSION['User'])) {
            return "User";
        }
    }

    public function activation($data = '')
    {
        $query = "UPDATE `users`.`users` SET `is_active` = '1' WHERE `users`.`uniqid`=  '" . $data . "'";
//        echo $query;
//        die();
        if (mysql_query($query)) {
            if (isset($_SESSION['Admin'])) {
                $_SESSION['Message'] = "User successfully activated.";
                header('location:users.php');
            } else {
                $_SESSION['Login_Notice'] = "Your account successfully activated.";
                header('location:login.php');
            }
        } else {
            $_SESSION['Message_Err'] = "Something Wrong!! Please contact us.";
            header('location:../index.php');
        }
    }

    public function deactivation($data = '')
    {
        $query = "UPDATE `users`.`users` SET `is_active` = '0' WHERE `users`.`uniqid`=  '" . $data . "'";
        if (mysql_query($query)) {
            if (isset($_SESSION['Admin'])) {
                $_SESSION['Message'] = "User deactivate successfully.";
                header('location:users.php');
            } else {
                $_SESSION['Login_Notice'] = "Your account successfully deactivated.";
                header('location:login.php');
            }
        } else {
            $_SESSION['Message_Err'] = "Something Wrong!! Please contact us.";
            header('location:../index.php');
        }
    }

    public function profile_view($id = '')
    {
        if (isset($id) && !empty($id)) {
            $query = "SELECT * FROM `profiles` WHERE `user_id` = '" . $id . "'";
        } else {
            $query = "SELECT * FROM `profiles` WHERE `user_id` = '" . $_SESSION['User'] . "'";
        }
        $run_query = mysql_query($query);
        $result = mysql_fetch_assoc($run_query);
        return $result;
    }

    public function profile_view_public($id = '')
    {
        if (isset($id) && !empty($id)) {
            $query = "SELECT * FROM `profiles` WHERE (`is_public` = '1' AND `id` = '" . $id . "')";
        } else {
            $query = "SELECT * FROM `profiles` WHERE `user_id` = '" . $_SESSION['User'] . "'";
        }
        $run_query = mysql_query($query);
        $result = mysql_fetch_assoc($run_query);
        return $result;
    }

    public function user_profile_view($id = '')
    {
        $query = "SELECT * FROM `profiles` WHERE `user_id` = '" . $id . "'";
        $run_query = mysql_query($query);
        $result = mysql_fetch_assoc($run_query);
        return $result;
    }

    public function profile_store()
    {
        if (isset($this->id) && !empty($this->id) && !empty($this->image)) {
            $query = "UPDATE `users`.`profiles` SET `id` = '" . $this->id . "', `user_id` = '" . $_SESSION['User'] . "', `first_name` = '" . $this->first_name . "', `last_name` = '" . $this->last_name . "', `personal_phone` = '" . $this->personal_phone . "', `home_phone` = '" . $this->home_phone . "', `office_phone` = '" . $this->office_phone . "', `present_address` = '" . $this->present_address . "', `permanent_address` = '" . $this->permanent_address . "', `gender` = '" . $this->gender . "', `occupation` = '" . $this->occupation . "', `image` = '" . $this->image . "' WHERE `profiles`.`id` = " . $this->id;
//            echo $query;
            $_SESSION['Message'] = "Profile successfully updated.";
            if (isset($this->oldimage)) {
                unlink("../assets/img/user/$this->oldimage");
            }

        } elseif (isset($this->id) && !empty($this->id) && empty($this->image)) {
            $query = "UPDATE `users`.`profiles` SET `id` = '" . $this->id . "', `user_id` = '" . $_SESSION['User'] . "', `first_name` = '" . $this->first_name . "', `last_name` = '" . $this->last_name . "', `personal_phone` = '" . $this->personal_phone . "', `home_phone` = '" . $this->home_phone . "', `office_phone` = '" . $this->office_phone . "', `present_address` = '" . $this->present_address . "', `permanent_address` = '" . $this->permanent_address . "', `gender` = '" . $this->gender . "', `occupation` = '" . $this->occupation . "' WHERE `profiles`.`id` = " . $this->id;
//            echo $query;
            $_SESSION['Message'] = "Profile successfully updated.";

        } else {
            $query = "INSERT INTO `users`.`profiles` (`user_id`, `first_name`, `last_name`, `personal_phone`, `home_phone`, `office_phone`, `present_address`, `permanent_address`, `gender`, `occupation`, `birthday`, `image`)"
                . "VALUES ('" . $this->user_id . "', '" . $this->first_name . "', '" . $this->last_name . "', '" . $this->personal_phone . "', '" . $this->home_phone . "', '" . $this->office_phone . "', '" . $this->present_address . "', '" . $this->permanent_address . "', '" . $this->gender . "', '" . $this->occupation . "', '" . $this->birthday . "', '" . $this->image . "')";
            $_SESSION['Message'] = "Profile Data save successfull.";
        }
//        echo $query;
//        die();
        mysql_query($query);

        header('location:index.php');
    }

    public function settings($data = '')
    {
//        print_r($data);
//        die();
        if (isset($data['title']) && !empty($data['subtitle'])) {
            if (isset($data['logo']) && !empty($data['logo'])) {
                $query = "UPDATE `users`.`settings` SET `title` = '" . $data['title'] . "', `subtitle` = '" . $data['subtitle'] . "', `email` = '" . $data['email'] . "', `reg_permission` = '" . $data['reg_permission'] . "', `user_active` = '" . $data['user_active'] . "', `footer` = '" . $data['footer'] . "', `logo` = '" . $data['logo'] . "'";
            } else {
                $query = "UPDATE `users`.`settings` SET `title` = '" . $data['title'] . "', `subtitle` = '" . $data['subtitle'] . "', `email` = '" . $data['email'] . "', `reg_permission` = '" . $data['reg_permission'] . "', `user_active` = '" . $data['user_active'] . "', `footer` = '" . $data['footer'] . "'";
            }
        } else {
            $query = "INSERT INTO `users`.`settings` (`title`, `subtitle`, `email`, `reg_permission`, `user_active`, `footer`, `logo`) VALUES ('Site Title', 'subtitle of this site', 'admin@thissite.com', '1', '1', 'something for footer', 'logo.png');";
        }
        mysql_query($query);
        header('location:settings.php');
    }

    public function setting()
    {
        $set = "SELECT * FROM `settings`";
        $sett = mysql_query($set);
        $setting = mysql_fetch_array($sett);
        return $setting;
    }

    public function privacy($id = '')
    {
        $query = "UPDATE `users`.`profiles` SET `is_public` = '" . $id . "' WHERE `profiles`.`user_id` = '" . $_SESSION['User'] . "'";
        mysql_query($query);
        header('location:profile.php');
    }

    public function counter_user()
    {

        $query = "SELECT * FROM `users`";
        $num = mysql_query($query);
        $rows = mysql_num_rows($num);
        echo $rows;
    }

    public function counter_active_user()
    {

        $query = "SELECT * FROM `users` WHERE `is_active` = '1'";
        $num = mysql_query($query);
        $rows = mysql_num_rows($num);
        echo $rows;
    }

    public function counter_deactive_user()
    {

        $query = "SELECT * FROM `users` WHERE `is_active` = '0'" ;
        $num = mysql_query($query);
        $rows = mysql_num_rows($num);
        echo $rows;
    }

    public function counter_user_profile()
    {

        $query = "SELECT * FROM `profiles`";
        $num = mysql_query($query);
        $rows = mysql_num_rows($num);
        echo $rows;
    }


}