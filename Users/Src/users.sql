-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2016 at 06:08 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `users`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `personal_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `home_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `office_phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `present_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `permanent_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `occupation` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `image` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `use_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `first_name`, `last_name`, `personal_phone`, `home_phone`, `office_phone`, `present_address`, `permanent_address`, `gender`, `occupation`, `birthday`, `image`, `is_public`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 2, 'Power of', 'User', '', '', '', 'present_address', 'permanent_address', 'Male', 'occupation', '2016-04-27', '', 1, '0000-00-00 00:00:00', '2016-04-19 09:13:35', NULL),
(6, 5, 'Labib', 'Ahmed', '58563', '3565', '3456356', 'pressent', 'permanent', 'Mail', 'Teacher', '0000-00-00', '1460986480266a371326c3703199bee366a994e757_Geometric_Color_Shapes_Blurred_Wallpaper_019.jpg', 1, '0000-00-00 00:00:00', '2016-04-18 22:40:56', NULL),
(10, 1, 'Khorshed', 'Alam', '123455678', '3453425', '4325435', 'Mawna, Sreepur', 'gazipur', 'Male', 'ebaro', '2016-04-13', '14609081265g.png', 0, '0000-00-00 00:00:00', '2016-04-19 08:22:23', NULL),
(13, 16, 'First', 'naem', '01749999700', '01749999700', '01749999700', 'Mawna, Sreepur', '', 'Mail', 'one', '2016-04-15', '1460992184Alam.foxof QR.PNG', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL),
(14, 21, 'Rakibul', 'Islam', '01749999700', '01749999700', '01749999700', 'Mawna, Sreepur', 'sdafsdahflksda', 'Mail', 'hello', '2016-04-14', '1460992290file (2).png', 1, '0000-00-00 00:00:00', '2016-04-18 21:53:31', NULL),
(15, 19, 'Shariful', 'Islam', '343423', '5434534532', '435345342532', 'Mawna, Sreepursd', 'fsdafsdafsda', 'Mail', 'sfsdfasd', '2016-04-15', '1460992611file (1).jpeg', 1, '0000-00-00 00:00:00', '2016-04-18 21:53:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `title` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `subtitle` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `reg_permission` tinyint(4) NOT NULL,
  `user_active` tinyint(4) NOT NULL,
  `footer` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`title`, `subtitle`, `email`, `reg_permission`, `user_active`, `footer`, `logo`) VALUES
('5G Boombing', 'subtitle of this sitess@', 'alam@foxof.org', 1, 0, 'Copyrite Â© 2016 Created by <a href="http://alam.foxof.org">Khorshed Alam</a>', 'logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniqid` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `group` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `is_admin` int(2) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL,
  `is_active` int(2) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uniqid`, `group`, `username`, `email`, `password`, `is_admin`, `created_at`, `updated_at`, `deleted_at`, `is_active`) VALUES
(1, '1', '', 'admin', 'admin@ka.s', 'd7b3bbbc0a472166030089695af7d5cb', 1, '0000-00-00 00:00:00', '2016-04-18 12:34:42', NULL, 1),
(2, '2', '', 'user', 'kas@gmail.com', 'd7b3bbbc0a472166030089695af7d5cb', 0, '0000-00-00 00:00:00', '2016-04-18 13:03:49', NULL, 1),
(5, '57000', '', 'user1', 'user1@ka.s', 'kas', 0, '0000-00-00 00:00:00', '2016-04-18 12:41:51', NULL, 0),
(15, '57147cfd272dc', '', '2nd', 'seco@nd.com', '1234', 0, '0000-00-00 00:00:00', '2016-04-18 12:47:27', NULL, 1),
(16, '57147d349433c', '', 'first', 'fir@st.com', '1234', 0, '0000-00-00 00:00:00', '2016-04-18 11:48:41', NULL, 1),
(17, '5714bfa068e04', '', 'newww', 'newww@gmail.com', '4584b4857cde83c4aeac', 0, '0000-00-00 00:00:00', '2016-04-18 11:08:47', NULL, 1),
(18, '5714cc675ce83', '', 'ds', 'dsa@dsaf', '010bb919679d4ef9adae3265232588dd', 0, '0000-00-00 00:00:00', '2016-04-18 12:39:07', NULL, 1),
(19, '5714cc97af63b', '', 'sadfash', 'dsjhfskad@sdfhskad', '43595882b8217ad21001bc3e6afad5c1', 0, '0000-00-00 00:00:00', '2016-04-18 12:39:10', NULL, 1),
(20, '5714cd00633ba', '', 'amader', 'ma@der', '217a7dadea85487427d29a9c102c886c', 0, '0000-00-00 00:00:00', '2016-04-18 12:50:16', NULL, 1),
(21, '5714d2ab47580', '', 'robel', 'robel@live.com', 'f328e3be16a1caeedfa6ba2b0b93c717', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0),
(22, '5714d2e772a9e', '', 'helal', 'uss@man', 'f12c886d0956d963d0f31f976ee3be0d', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0),
(23, '5714d33147965', '', 'amanullahq', 'dfsda@dsfs', '2346ff1e2084245f7ea25a39cc4c645d', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0),
(24, '5714d375f233a', '', 'Kabir', 'dsfasf@dfs', '03f7c1099cc8b64a0fc880fb642cba7b', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
