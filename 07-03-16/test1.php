<?php


class MyClass
{
    public $propert1 = "I'm a class property!";
    public function setProperty($newval)
    {
        $this->propert1 = $newval;
    }

    public function getProperty()
    {
        return $this->propert1."<br/>";
    }

}

//Create two object
$obj1 = new MyClass;
$obj2 = new MyClass;

//Get the value of $property1 from both objects
echo $obj1->getProperty();
echo $obj2->getProperty();

//Set new values for both objects
$obj1->setProperty("I'am a new property value!");
$obj2->setProperty("I belong to second instance!");

//Output both objects $property value
echo $obj1->getProperty();
echo $obj2->getProperty();


?>