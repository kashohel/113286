<?php


class MyClass
{
    public $propert1 = "I'm a class property!";
    public function setProperty($newval)
    {
        $this->propert1 = $newval;
    }

    /**
     * @return string
     */
    public function getProperty()
    {
        return $this->propert1."<br/>";
    }

}

$obj = new MyClass;
echo $obj->getProperty();
$obj->setProperty("I'am a new property value!");
echo $obj->getProperty();

?>