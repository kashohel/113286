<?php

namespace App;

use PDO;

if (!isset($_SESSION)) {
    session_start();
}

class Users {

    public $pdo;
    public $table = 'users';
    public $username = '';
    public $email = '';
    public $password = '';
    public $remember = '';
    public $unique_id = '';
    public $active = 1;

    public function __construct() {
        try {
            $this->pdo = new PDO('mysql:host=localhost;dbname=db_owncms', 'root', '');
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO:: ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "connection failed!" . $e->getMessage();
        }
    }

    public function prepare_data($data = '') {
//        echo '<pre>';
//        print_r($data);
//        exit();
        
//        $p = md5($data['password']);
//        $r = md5(2);
//        $s = md5(123456);
//        $m = md5(123456);
//         echo '<pre>';
//        print_r($p.'<br>');
//        print_r($r.'<br>');
//        print_r($s.'<br>');
//        print_r($m);
//        
//        
//        exit();
//        $r = crypt(123456);
//        $s = crypt(123456);
//        print_r($r.'<br>');
//        print_r($s.'<br>');
//        exit();
        if (array_key_exists('username', $data) && !empty($data['username'])) {
            $this->username = $data['username'];
        }
        if (array_key_exists('email', $data) && !empty($data['email'])) {
            $this->email = $data['email'];
        }
        if (array_key_exists('password', $data) && !empty($data['password'])) {
            $this->password = $data['password'];
        }
        if (array_key_exists('remember', $data) && !empty($data['remember'])) {
            $this->remember = $data['remember'];
        }


        $this->unique_id = uniqid();
    }

    public function insert() {
        $sql = "INSERT INTO $this->table (unique_id, username, email, password) VALUES (:unique_id, :username, :email, :password)";
        $q = $this->pdo->prepare($sql);
        $q->bindParam(':unique_id', $this->unique_id);
        $q->bindParam(':username', $this->username);
        $q->bindParam(':email', $this->email);
        $q->bindParam(':password', $this->password);
        $q->execute();

        $_SESSION['s_mgs'] = "Your are Successfully Register. Please Login Your account";
        header('location:login.php#tologin');
    }

    public function login_check() {
      //  session_start();
        $sql = "SELECT * FROM $this->table WHERE (username = :username || password = :password)";
        $q = $this->pdo->prepare($sql);
        $q->bindParam(':username', $this->username);
        $q->bindParam(':password', $this->password);
      //  $q->bindParam(':active', $this->active);
        $q->execute();
        $result = $q->fetch(PDO::FETCH_ASSOC);
//        echo '<pre>';
//        print_r($result);
//        exit();
        $id = $result['id'];
        $username = $result['username'];
        $password = $result['password'];
        $active = $result['is_active'];

//        echo '<pre>';
//        print_r($username .' '. $this->username .'<br>');
//        print_r($password .' '. $this->password);
//        exit();
        if (($username == $this->username) && ($password == $this->password) && ($active == $this->active)) {
            $_SESSION['username'] = $this->username;
            $_SESSION['user_id'] = $id;
            $_SESSION['login_succ_mgs'] = "Welcome to Admin Panel";

            //  $cookie_name = array('username');
            //  $cookie_value = array($this->username);
            if ($this->remember === 'on') {
                setcookie('username', $this->username, time() + 60);
                setcookie('password', $this->password, time() + 60);
            }

            header('location:backend/dashboard.php');
        } elseif (($username == $this->username) && ($password == $this->password) && ($active != $this->active)) {

            $_SESSION['login_failed_mgs'] = "Wait to Admin Approve";
            header('location:login.php#tologin');
        } elseif (($username == $this->username) && $password != $this->password) {
            $_SESSION['login_failed_mgs'] = "Invalid Password!";
            header('location:login.php#tologin');
        } elseif (($password == $this->password) && ($username != $this->username)) {
            $_SESSION['login_failed_mgs'] = "Invalid Username!";
            header('location:login.php#tologin');
        } else {
            $_SESSION['login_failed_mgs'] = "Invalid Username or Password";
            header('location:login.php#tologin');
        }
    }

    public function store() {
        //  print_r($this->password);
        //  die();
        $sql = "INSERT INTO $this->table (username, password) VALUES (:username, :password)";
        $q = $this->pdo->prepare($sql);
        $q->bindParam(':username', $this->username);
        $q->bindParam(':password', $this->password);

        $q->execute();

        $_SESSION['profile_save_mgs'] = "User saved successfully";
        header('location:users.php');
    }

    public function view_all_users() {
        $sql = "SELECT * FROM $this->table";
        $q = $this->pdo->prepare($sql);
        $q->execute();
        $result = $q->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function view_user($user_id) {
        $sql = "SELECT * FROM $this->table WHERE id = :id";
        $q = $this->pdo->prepare($sql);
        $q->bindParam(':id', $user_id);
        $q->execute();
        $result = $q->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function status_update($id = '') {


        $sql = "UPDATE $this->table SET is_active = :active WHERE id = :id";
        //  print_r($id);
        $q = $this->pdo->prepare($sql);
        $q->bindParam(':id', $id);
        $q->bindParam(':active', $this->inactive);
        $q->execute();

        header('location:users.php');
    }

    public function user_status_inactive($id = '') {


        $sql = "UPDATE $this->table SET is_active = :active WHERE id = :id";
        //  print_r($id);
        $q = $this->pdo->prepare($sql);
        $q->bindParam(':id', $id);
        $q->bindParam(':active', $this->active);
        $q->execute();

        header('location:users.php');
    }

    public function logout() {
        unset($_SESSION['user_id']);
        unset($_SESSION['username']);

        header('location:index.php');
    }

}

?>