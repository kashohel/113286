<?php
include_once '../vendor/autoload.php';

use App\Profiles;
use App\Users;

$userobj = new Users();
$profileobj = new Profiles();

$id = $_GET['id'];
$user_info = $userobj->view_user($id);
$user_profile_info = $profileobj->view_user_details($id);
//echo '<pre>';
//print_r($user_info);
//exit();

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SuperheroAdmin - Bootstrap Admin Template</title>

        <!-- bootstrap -->
        <link href="../css/bootstrap/bootstrap.css" rel="stylesheet" />

        <!-- libraries -->
        <link href="../css/libs/font-awesome.css" type="text/css" rel="stylesheet" />

        <!-- global styles -->
        <link rel="stylesheet" type="text/css" href="../css/compiled/layout.css">
        <link rel="stylesheet" type="text/css" href="../css/compiled/elements.css">

        <!-- this page specific styles -->
        <link rel="stylesheet" href="../css/libs/datepicker.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/daterangepicker.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/bootstrap-timepicker.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/select2.css" type="text/css" />

        <!-- google font libraries -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>


    </head>
    <body>
    <?php include_once 'admin_header_bar.php';?>
        <div class="container">
            <div class="row">
                <div class="col-md-2" id="nav-col">
                   <?php include_once 'admin_left_sidebar.php';?>
                </div>
                <div class="col-md-10" id="content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                         
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="main-box">
                                        <h2>Edit Profile</h2>
                                        <form action="user_store.php" method="POST" >
                                            
                                            <div class="form-group">
                                                <label for="username">Username</label>
                                                <input type="text" name="username" class="form-control" id="username" placeholder="User Name">
                                            </div>
                                            <div class="form-group">
                                                <label for="username">Password</label>
                                                <input type="password" name="password" class="form-control" id="password" placeholder="Enter Password">
                                            </div>
                                           
                                           
                                           
                                            <input type="submit" value="Save">
                                            <input type="reset"  value="Reset">
                                        </form>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                   
                                </div>	
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer id="footer-bar">
            <p id="footer-copyright">
                &copy; 2014 <a href="http://www.adbee.sk/" target="_blank">Adbee digital</a>. Powered by SuperheroAdmin.
            </p>
        </footer>

        <!-- global scripts -->
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.js"></script>

        <!-- this page specific scripts -->
        <script src="../js/jquery.maskedinput.min.js"></script>
        <script src="../js/bootstrap-datepicker.js"></script>
        <script src="../js/moment.min.js"></script>
        <script src="../js/daterangepicker.js"></script>
        <script src="../js/bootstrap-timepicker.min.js"></script>
        <script src="../js/select2.min.js"></script>
        <script src="../js/hogan.js"></script>
        <script src="../js/typeahead.min.js"></script>
        <script src="../js/jquery.pwstrength.js"></script>

        <!-- theme scripts -->
        <script src="../js/scripts.js"></script>

        <!-- this page specific inline scripts -->
        <script>
            $(function ($) {
                //tooltip init
                $('#exampleTooltip').tooltip();

                //nice select boxes
                $('#sel2').select2();

                $('#sel2Multi').select2({
                    placeholder: 'Select a Country',
                    allowClear: true
                });

                //masked inputs
                $("#maskedDate").mask("99/99/9999");
                $("#maskedPhone").mask("(999) 999-9999");
                $("#maskedPhoneExt").mask("(999) 999-9999? x99999");
                $("#maskedTax").mask("99-9999999");
                $("#maskedSsn").mask("999-99-9999");

                $("#maskedProductKey").mask("a*-999-a999", {placeholder: " ", completed: function () {
                        alert("You typed the following: " + this.val());
                    }});

                $.mask.definitions['~'] = '[+-]';
                $("#maskedEye").mask("~9.99 ~9.99 999");

                //datepicker
                $('#datepickerDate').datepicker({
                    format: 'mm-dd-yyyy'
                });

                $('#datepickerDateComponent').datepicker();

                //daterange picker
                $('#datepickerDateRange').daterangepicker();

                //timepicker
                $('#timepicker').timepicker({
                    minuteStep: 5,
                    showSeconds: true,
                    showMeridian: false,
                    disableFocus: false,
                    showWidget: true
                }).focus(function () {
                    $(this).next().trigger('click');
                });

                //autocomplete simple
                $('#exampleAutocompleteSimple').typeahead({
                    prefetch: '/data/countries.json',
                    limit: 10
                });

                //autocomplete with templating
                $('#exampleAutocomplete').typeahead({
                    name: 'twitter-oss',
                    prefetch: '/data/repos.json',
                    template: [
                        '<p class="repo-language">{{language}}</p>',
                        '<p class="repo-name">{{name}}</p>',
                        '<p class="repo-description">{{description}}</p>'
                    ].join(''),
                    engine: Hogan
                });

                //password strength meter
                $('#examplePwdMeter').pwstrength({
                    label: '.pwdstrength-label'
                });

            });
        </script>
    </body>
</html>