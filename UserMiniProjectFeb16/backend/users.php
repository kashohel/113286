<?php
if(!isset($_SESSION)){
   session_start(); 
}
if ($_SESSION['user_id']) {
    
} else {
    header('location:../index.php');
}
?>
<?php
include_once '../vendor/autoload.php';

use App\Users;
use App\Profiles;

$profileobj = new Profiles();
$userobj = new Users();
$all_users = $userobj->view_all_users();

$user_all_info = $profileobj->view_all_users();
//echo '<pre>';
//print_r($user_profile_info);
//exit();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>SuperheroAdmin - Bootstrap Admin Template</title>

        <!-- bootstrap -->
        <link href="../css/bootstrap/bootstrap.css" rel="stylesheet" />

        <!-- libraries -->
        <!-- <link href="css/libs/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css" /> -->
        <link href="../css/libs/font-awesome.css" type="text/css" rel="stylesheet" />

        <!-- global styles -->
        <link rel="stylesheet" type="text/css" href="../css/compiled/layout.css">
        <link rel="stylesheet" type="text/css" href="../css/compiled/elements.css">

        <!-- this page specific styles -->
        <link rel="stylesheet" href="../css/libs/fullcalendar.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/fullcalendar.print.css" type="text/css" media="print" />
        <link rel="stylesheet" href="../css/compiled/calendar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="../css/libs/morris.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/daterangepicker.css" type="text/css" />
        <link rel="stylesheet" href="../css/libs/jquery-jvectormap-1.2.2.css" type="text/css" />

        <!-- Favicon -->
        <link type="image/x-icon" href="../favicon.png" rel="shortcut icon"/>

        <!-- google font libraries -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300|Titillium+Web:200,300,400' rel='stylesheet' type='text/css'>


    </head>
    <body>
        <?php include_once 'admin_header_bar.php'; ?>
        <div class="container">
            <div class="row">
                <div class="col-md-2" id="nav-col">
                    <?php include_once 'admin_left_sidebar.php'; ?>
                </div>
                <div class="col-md-10" id="content-wrapper">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="clearfix">
                                <h1 class="pull-left">Users</h1>

                                <div class="pull-right top-page-ui">
                                    <a href="add_user.php" class="btn btn-primary pull-right">
                                        <i class="fa fa-plus-circle fa-lg"></i> Add user
                                    </a>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-box clearfix">
                                        <div class="table-responsive">
                                            <table class="table user-list">
                                                <thead>
                                                    <tr>
                                                        <th><span>User</span></th>
                                                        <th><span>Created</span></th>
                                                        <th class="text-center"><span>Status</span></th>
                                                        <th><span>Email</span></th>
                                                        <th>&nbsp;</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    
                                                    foreach ($user_all_info as $v_user) {
                                                       
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php if (isset($v_user['image']) && !empty($v_user['image'])) { ?>
                                                                <img src="../img/profile_pics/<?php echo $v_user['image']; ?>" width="50" height="50" alt="" class="profile-img img-responsive center-block"/>
                                                                <?php } else { ?>
                                                                    <img src="../img/samples/Unknown_Person.png" width="50" height="50" alt="" class="profile-img img-responsive center-block"/>

                                                                <?php } ?>
                                                                <a href="user_view.php?id=<?php echo $v_user['id'];?>" class="user-link"><?php echo $v_user['firstname'] . ' ' . $v_user['lastname']; ?></a>
                                                                <span class="user-subhead">
                                                                    <?php
                                                                    if ($v_user['is_admin'] == 1) {
                                                                        echo "Admin";
                                                                    } else {
                                                                        echo "User";
                                                                    }
                                                                    ?>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <?php echo $v_user['created_at']; ?>
                                                            </td>
                                                            <td class="text-center">
                                                                    <?php
                                                                    if ($v_user['is_active'] == 1) { ?>
                                                                        <a href = "status.php?id=<?php echo $v_user['id'];?>">
                                                                        <span class = "label label-danger">Deactive</span>
                                                                        </a>
                                                                            <?php } else { ?>
                                                                <a href = "status_inactive.php?id=<?php echo $v_user['id']; ?>">
                                                                                    <span class = "label label-success">active</span>
                                                                                </a>
                                                                            <?php }  ?>
                                                                            
                                                            </td>
                                                            <td>
                                                                <a href="#"> <?php echo $v_user['email']; ?></a>
                                                            </td>
                                                            <td style="width: 20%;">
                                                                <a href="user_view.php?id=<?php echo $v_user['id'];?>" class="table-link">
                                                                    <span class="fa-stack">
                                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                                        <i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
                                                                    </span>
                                                                </a>
                                                                <a href="user_edit.php?id=<?php echo $v_user['id'];?>" class="table-link">
                                                                    <span class="fa-stack">
                                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                                        <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                                                                    </span>
                                                                </a>
                                                                <a href="user_delete.php?id=<?php echo $v_user['id'];?>" class="table-link danger">
                                                                    <span class="fa-stack">
                                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                                        <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                                    </span>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?> 
                                                        <?php
                                                    
                                                    foreach ($all_users as $v_user) {
                                                       
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php if (isset($v_user['image']) && !empty($v_user['image'])) { ?>
                                                                <img src="../img/profile_pics/<?php echo $v_user['image']; ?>" width="50" height="50" alt="" class="profile-img img-responsive center-block"/>
                                                                <?php } else { ?>
                                                                    <img src="../img/samples/Unknown_Person.png" width="50" height="50" alt="" class="profile-img img-responsive center-block"/>

                                                                <?php } ?>
                                                                <a href="user_view.php?id=<?php echo $v_user['id'];?>" class="user-link"><?php echo $v_user['username'] ; ?></a>
                                                                <span class="user-subhead">
                                                                    <?php
                                                                    if ($v_user['is_admin'] == 1) {
                                                                        echo "Admin";
                                                                    } else {
                                                                        echo "User";
                                                                    }
                                                                    ?>
                                                                </span>
                                                            </td>
                                                            <td>
                                                                <?php echo $v_user['created_at']; ?>
                                                            </td>
                                                            <td class="text-center">
                                                                    <?php
                                                                    if ($v_user['is_active'] == 1) { ?>
                                                                        <a href = "user_status.php?id=<?php echo $v_user['id'];?>">
                                                                        <span class = "label label-danger">Deactive</span>
                                                                        </a>
                                                                            <?php } else { ?>
                                                                <a href = "user_status_inactive.php?id=<?php echo $v_user['id']; ?>">
                                                                                    <span class = "label label-success">active</span>
                                                                                </a>
                                                                            <?php }  ?>
                                                                            
                                                            </td>
                                                            <td>
                                                                <a href="#"> <?php echo $v_user['email']; ?></a>
                                                            </td>
                                                            <td style="width: 20%;">
                                                                <a href="user_view.php?id=<?php echo $v_user['id'];?>" class="table-link">
                                                                    <span class="fa-stack">
                                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                                        <i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
                                                                    </span>
                                                                </a>
                                                                <a href="user_edit.php?id=<?php echo $v_user['id'];?>" class="table-link">
                                                                    <span class="fa-stack">
                                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                                        <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                                                                    </span>
                                                                </a>
                                                                <a href="user_delete.php?id=<?php echo $v_user['id'];?>" class="table-link danger">
                                                                    <span class="fa-stack">
                                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                                        <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                                    </span>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?> 
                                                </tbody>
                                            </table>
                                        </div>
                                        <ul class="pagination pull-right">
                                            <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer id="footer-bar">
            <p id="footer-copyright">
                &copy; 2014 <a href="http://www.adbee.sk/" target="_blank">Adbee digital</a>. Powered by SuperheroAdmin.
            </p>
        </footer>

        <!-- global scripts -->
        <script src="../js/jquery.js"></script>
        <script src="../js/bootstrap.js"></script>

        <!-- this page specific scripts -->


        <!-- theme scripts -->
        <script src="../js/scripts.js"></script>

        <!-- this page specific inline scripts -->

    </body>
</html>