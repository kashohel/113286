<?php

//double decrement from 11 to 1
echo "Double decrement from 11 to 1"." </br>";
for ( $d=11; $d>=1; $d--){
        echo ($d--)."</br>";
}

echo "</br>";

//double decrement from 10 to 0
echo "Double decrement from 10 to 0"." </br>";
for ($e=10; $e>=0; $e--){
   echo ($e--)."</br>";
}

echo "</br>";

//double increment from 1 to 11
echo "Double increment from 1 to 11"." </br>";
for ( $f=1; $f<=11; $f++){
        echo ($f++)."</br>";
}

echo "</br>";

//double increment from 0 to 10
echo "Double increment from 0 to 10"." </br>";
for ($g=0; $g<=10; $g++){
   echo ($g++)."</br>";
}

echo "</br>";

//Increment from 0 to 10
echo "Increment from 0 to 10"." </br>";
for ($g=0; $g<=10; $g++){
   echo $g."</br>";
}

echo "</br>";

//Increment from 0 to 100 using sequence 8
echo "Increment from 0 to 100 using sequence 8"." </br>";

for ($e=0; $e<=100; $e+=8){
   echo $e."</br>";
}


?>