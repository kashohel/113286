<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once 'vendor/autoload.php';
use App\Manage;

$id = $_GET['id'];
$users = new Manage();
$user = $users->show($id);
?>

<a href="Create.php">Add New</a> |
<a href="Index.php">Back to List</a>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Age</th>
        <th>Education</th>
        <th>Action</th>
    </tr>
    <tr>
        <td><?php echo $user['id']?></td>
        <td><?php echo $user['name']?></td>
        <td><?php echo $user['email']?></td>
        <td><?php echo $user['age']?></td>
        <td><?php echo $user['education']?></td>
        <td>
            <a href="Edit.php?id=<?php echo $user['id']?>">Edit</a> |
            <a href="Trash.php?id=<?php echo $user['id']?>">Delete</a>
        </td>
    </tr>
</table>
