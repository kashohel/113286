<?php
include_once 'vendor/autoload.php';
use App\Manage;
$users =  new Manage();
$id = $_GET['id'];
$user = $users->show($id);

?>

<html>
<head>
    <title>Mobile Models | Update</title>
</head>
<body>
<form action="Update.php" method="POST">
    <fieldset>
        <input type="hidden" name="id" value="<?php echo $id; ?>" id="id">
        <legend>Add favorite Mobile Models</legend>
        <label>Enter your Name</label><br/>
        <input type="text" name="title" value="<?php echo $user['title']; ?>" id="title" required title="title"><br/>
        <label>Enter your email</label><br/>
        <input type="email" name="email" value="<?php echo $user['email']; ?>" id="email" required><br/>
        <label>Enter your password</label><br/>
        <input type="password" name="password" value="" id="password"><br/>
        <label>Enter your age</label><br/>
        <input type="number" name="age" value="<?php echo $user['age']; ?>" id="age"><br/>
        <label>Enter your Phone number</label><br/>
        <input type="number" name="phone" value="<?php echo $user['phone']; ?>" id="phone"><br/>
        <input type="hidden" name="usertype" value="<?php echo $user['usertype']; ?>" id="usertype">
        <input type="hidden" name="status" value="<?php echo $user['status']; ?>" id="status">
        <label>Select your gender</label><br/>
        <select type="input" name="gender" required>
            <option value="">Select one</option>
            <option value="male">Male</option>
            <option value="female">Femail</option>
            <option value="common">Common</option>
        </select><br/>
        <label>Select your leaving country</label><br/>
        <select type="input" name="country" required>
            <option value="">Select one of this</option>
            <option value="bangladesh">Bangladesh</option>
            <option value="india">India</option>
            <option value="usa">US</option>
            <option value="england">UK</option>
        </select><br/>
        <label>Your Facebook link</label><br/>
        <input type="url" name="facebook" value="<?php echo $user['facebook']; ?>" id="facebook" required><br/>
        <label>Your twitter link</label><br/>
        <input type="url" name="twitter" value="<?php echo $user['twitter']; ?>" id="twitter" required><br/>
        <label>Your Google+ link</label><br/>
        <input type="url" name="googleplus" value="<?php echo $user['googleplus']; ?>" id="googleplus" required><br/>
        <input type="submit" value="SAVE">
        <input type="reset" value="Reset">
    </fieldset>
</form>
</body>
</html>


