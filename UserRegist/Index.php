
<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once 'vendor/autoload.php';
use App\Manage;
session_start();
if(isset($_SESSION['Message']) && !empty($_SESSION['Message'])){
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}

$users = new Manage();
$allUsers = $users->index();

//$Users.php->debug($allUsers);

?>

<a href="Create.php">On create</a></br>

<table border="1">
    <tr>
        <th>SL</th>
        <th>Name</th>
        <th>Email</th>
        <th>Age</th>
        <th>Actions</th>
    </tr>
    <?php
    if(isset($allUsers) && !empty($allUsers)){
        $serial = 0;
        foreach ($allUsers as $User){
            $serial++
            ?>
            <tr>
                <td><?php echo $serial ?></td>
                <td>
                    <?php echo $User['title'] ?>
                </td>
                <td>
                    <?php echo $User['email'] ?>
                </td>
                <td>
                    <?php echo $User['age'] ?>
                </td>
                <td>
                    <a href="Edit.php?id=<?php echo $User['id'] ?>">Edit</a> |
                    <a href="Show.php?id=<?php echo $User['id'] ?>">Show</a> |
                    <a href="Trash.php?id=<?php echo $User['id'] ?>">Delete</a>
                </td>

            </tr>
        <?php } }else{ ?>
        <tr>
            <td colspan="4">
                <?php echo "Opps! No avilable Data here" ?>
            </td>
        </tr>
    <?php  }
    ?>
</table>