<?php 

$a = true && false; 
var_dump($a); 

$b = false && true; 
var_dump($b); 

$c = true && true; 
var_dump($c); 

$d = false && false; 
var_dump($d); 

$a = true || false; 
var_dump($a); 

$b = false || true; 
var_dump($b); 

$c = true || true; 
var_dump($c); 

$d = false || false; 
var_dump($d); 

?> 