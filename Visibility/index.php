<?php
include_once 'Manage.php';

echo "----Output of Main Calc Class-----". "<br/>";
$Calc = new Calc();

$result1 = $Calc->add1(50, 10, '+');
echo "PUBLIC:".$result1. "<br/>";
echo $Calc->name1(). "<br/>";

echo "Private can not access from here". "<br/>";
//$result2 = $Calc->add2(50, 10, '+');
//echo "PRIVATE:".$result2. "<br/>";
//echo $Calc->name2(). "<br/>";

echo "Protected can not access from here". "<br/>";
//$result3 = $Calc->add3(50, 10, '+');
//echo "PRIVATE:".$result3. "<br/>";
//echo $Calc->name3(). "<br/>";

echo "----Output of Sub Calc Class-----". "<br/>";
$Clild = new Clild();

$result1 = $Clild->add1(60, 30, '+');
echo "PUBLIC:".$result1. "<br/>";
echo $Clild->name1(). "<br/>";

echo "Private can not access from here". "<br/>";
//$result2 = $Clild->add2(60, 30, '+');
//echo "PRIVATE:".$result2. "<br/>";
//echo $Clild->name2(). "<br/>";

echo "Protected can not access from here". "<br/>";
//$result3 = $Clild->add3(60, 30, '+');
//echo "PRIVATE:".$result3. "<br/>";
//echo $Clild->name3(). "<br/>";
echo $Clild->addd(). "<br/>";