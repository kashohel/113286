-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2016 at 10:29 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_owncms`
--

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `title` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `subtitle` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `site_footer` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `reg_permission` tinyint(4) NOT NULL,
  `user_active` tinyint(4) NOT NULL,
  `admin_footer` varchar(222) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(111) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`title`, `subtitle`, `site_footer`, `reg_permission`, `user_active`, `admin_footer`, `logo`) VALUES
('Site titlessdfsgfdg@@', 'subtitlesdsfdsdsafdsaf@@', 'alam@fosof.rogsdfssdfdsgfds@@', 0, 0, '@@@@@@@@@@@@', 'logo.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
 ADD PRIMARY KEY (`title`), ADD KEY `title` (`title`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
