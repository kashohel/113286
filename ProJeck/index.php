<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <title>Project of Khorshed Alam</title>

    <link rel="stylesheet" href="assets/css/bootstraps.min.css">

    <!-- Related styles of various icon packs and plugins -->
    <link rel="stylesheet" href="assets/css/pluginss.css">

    <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
    <link rel="stylesheet" href="assets/css/mains.css">

    <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
    <link rel="stylesheet" href="assets/css/themess.css">
    <!-- END Stylesheets -->

    <!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
    <script src="assets/js/vendor/modernizr-2.7.1.js"></script>
</head>
<body>
<!-- Page Container -->
<!-- In the PHP version you can set the following options from inc/config file -->
<!-- 'boxed' class for a boxed layout -->
<div id="page-container">
    <!-- Site Header -->
    <header>
        <div class="container">
            <!-- Site Logo -->
            <a href="index.html" class="site-logo">
                <i class="gi gi-dashboard"></i> Marge <strong>Project</strong>
            </a>
            <!-- Site Logo -->

            <!-- Site Navigation -->
            <nav>
                <!-- Menu Toggle -->
                <!-- Toggles menu on small screens -->
                <a href="javascript:void(0)" class="btn btn-default site-menu-toggle visible-xs visible-sm">
                    <i class="fa fa-bars"></i>
                </a>
                <!-- END Menu Toggle -->

                <!-- Main Menu -->
                <ul class="site-nav">
                    <!-- Toggles menu on small screens -->
                    <li class="visible-xs visible-sm">
                        <a href="javascript:void(0)" class="site-menu-toggle text-center">
                            <i class="fa fa-times"></i>
                        </a>
                    </li>
                    <!-- END Menu Toggle -->
                    <li>
                        <a href="index.php"> Home</a>
                    </li>
                    <li>
                        <a href="about.php">About</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="site-nav-sub"><i
                                class="fa fa-angle-down site-nav-arrow"></i>Projects</a>
                        <ul>
                            <li>
                                <a href="Views/Book.php">Book</a>
                            </li>
                            <li>
                                <a href="Views/Birthday.php">Birthday</a>
                            </li>
                            <li>
                                <a href="Views/ProfilePic.php">Profile Pic</a>
                            </li>
                            <li>
                                <a href="Views/Gender.php">Gender</a>
                            </li>
                            <li>
                                <a href="Views/Education.php">Education</a>
                            </li>
                            <li>
                                <a href="Views/Mobile.php">Mobile Model</a>
                            </li>
                            <li>
                                <a href="Views/Hobby.php">Hobby</a>
                            </li>
                            <li>
                                <a href="Views/City.php">City</a>
                            </li>
                            <li>
                                <a href="Views/Subscribe.php">Email Subs</a>
                            </li>
                            <li>
                                <a href="Views/Summery.php">Summery</a>
                            </li>
                            <li>
                                <a href="Views/Term.php">Term and Condition</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="contact.html">Contact</a>
                    </li>

                </ul>
                <!-- END Main Menu -->
            </nav>
            <!-- END Site Navigation -->
        </div>
    </header>
    <!-- END Site Header -->

    <!-- Intro -->
    <section class="site-section site-section-light site-section-top themed-background-dark">
        <div class="container" align="center">
            <h1 class="animation-slideDown"><strong>PList of Projrct</strong></h1>
            <h2 class="h3 animation-slideUp">Which already done successfully.</h2>
        </div>
    </section>
    <!-- END Intro -->

    <!-- Key Features -->
    <section class="site-content site-section">
        <div class="container">
            <div class="row row-items text-center">
                <div class="col-sm-3">
                    <a href="Views/Book.php" class="circle visibility-none themed-background-fire"
                       data-toggle="animation-appear" data-animation-class="animation-fadeIn360"
                       data-element-offset="-100">
                        <i class="gi gi-blog"></i>
                    </a>
                    <h4><strong>Book</strong> Title</h4>
                </div>
                <div class="col-sm-3">
                    <a href="Views/Birthday.php" class="circle visibility-none themed-background"
                       data-toggle="animation-appear" data-animation-class="animation-fadeIn360"
                       data-element-offset="-100">
                        <i class="gi gi-birthday_cake"></i>
                    </a>
                    <h4><strong>Birthday</strong></h4>
                </div>
                <div class="col-sm-3">
                    <a href="Views/ProfilePic.php" class="circle visibility-none themed-background-flatie"
                       data-toggle="animation-appear" data-animation-class="animation-fadeIn360"
                       data-element-offset="-100">
                        <i class="gi gi-nameplate_alt"></i>
                    </a>
                    <h4><strong>Profile</strong> Picture</h4>
                </div>
                <div class="col-sm-3">
                    <a href="Views/Gender.php" class="circle visibility-none themed-background-amethyst"
                       data-toggle="animation-appear" data-animation-class="animation-fadeIn360"
                       data-element-offset="-100">
                        <i class="gi gi-parents"></i>
                    </a>
                    <h4><strong>Gender</strong></h4>
                </div>
                <div class="col-sm-3">
                    <a href="Views/Education.php" class="circle visibility-none themed-background-spring"
                       data-toggle="animation-appear" data-animation-class="animation-fadeIn360"
                       data-element-offset="-100">
                        <i class="gi gi-book_open"></i>
                    </a>
                    <h4><strong>Education</strong> Lavel</h4>
                </div>
                <div class="col-sm-3">
                    <a href="Views/Mobile.php" class="circle visibility-none themed-background-autumn"
                       data-toggle="animation-appear" data-animation-class="animation-fadeIn360"
                       data-element-offset="-100">
                        <i class="gi gi-iphone"></i>
                    </a>
                    <h4><strong>Mobile</strong> Model</h4>
                </div>
                <div class="col-sm-3">
                    <a href="Views/Hobby.php" class="circle visibility-none themed-background-night"
                       data-toggle="animation-appear" data-animation-class="animation-fadeIn360"
                       data-element-offset="-100">
                        <i class="gi gi-fast_food"></i>
                    </a>
                    <h4><strong>Hobby</strong></h4>
                </div>
                <div class="col-sm-3">
                    <a href="Views/City.php" class="circle visibility-none themed-background-fancy"
                       data-toggle="animation-appear" data-animation-class="animation-fadeIn360"
                       data-element-offset="-100">
                        <i class="gi gi-google_maps"></i>
                    </a>
                    <h4><strong>City</strong></h4>
                </div>
                <div class="col-sm-4">
                    <a href="Views/Subscribe.php" class="circle visibility-none themed-background-night"
                       data-toggle="animation-appear" data-animation-class="animation-fadeIn360"
                       data-element-offset="-100">
                        <i class="si si-e-mail"></i>
                    </a>
                    <h4><strong>Email</strong> Subscribe</h4>
                </div>
                <div class="col-sm-4">
                    <a href="Views/Summery.php" class="circle visibility-none themed-background-fancy"
                       data-toggle="animation-appear" data-animation-class="animation-fadeIn360"
                       data-element-offset="-100">
                        <i class="gi gi-notes_2"></i>
                    </a>
                    <h4><strong>Summery</strong></h4>
                </div>
                <div class="col-sm-4">
                    <a href="Views/Term.php" class="circle visibility-none themed-background-fancy"
                       data-toggle="animation-appear" data-animation-class="animation-fadeIn360"
                       data-element-offset="-100">
                        <i class="gi gi-ok"></i>
                    </a>
                    <h4><strong>Term</strong> & Condition</h4>
                </div>
            </div>
            <hr>
        </div>
    </section>
    <!-- END Key Features -->

    <!-- Testimonials -->
    <section class="site-content site-section">
        <div class="container">
            <div class="site-block">
                <div id="testimonials-carousel" class="carousel slide carousel-html" data-ride="carousel"
                     data-interval="4000">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#testimonials-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#testimonials-carousel" data-slide-to="1"></li>
                        <li data-target="#testimonials-carousel" data-slide-to="2"></li>
                        <li data-target="#testimonials-carousel" data-slide-to="3"></li>
                        <li data-target="#testimonials-carousel" data-slide-to="4"></li>
                        <li data-target="#testimonials-carousel" data-slide-to="5"></li>
                        <li data-target="#testimonials-carousel" data-slide-to="6"></li>
                        <li data-target="#testimonials-carousel" data-slide-to="7"></li>
                        <li data-target="#testimonials-carousel" data-slide-to="8"></li>
                        <li data-target="#testimonials-carousel" data-slide-to="9"></li>
                        <li data-target="#testimonials-carousel" data-slide-to="10"></li>
                    </ol>
                    <!-- END Indicators -->

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner text-center">
                        <div class="active item">
                            <p>
                                <img src="assets/img/placeholders/avatars/book.png" width="50" height="50" alt="Avatar"
                                     class="img-circle">
                            </p>
                            <blockquote class="no-symbol">
                                <p>An awesome team that brought our ideas to life! Highly recommended!</p>
                                <footer><strong>Book </strong>, title</footer>
                            </blockquote>
                        </div>
                        <div class="item">
                            <p>
                                <img src="assets/img/placeholders/avatars/birthday.png" width="50" height="50"
                                     alt="Avatar" class="img-circle">
                            </p>
                            <blockquote class="no-symbol">
                                <p>An awesome team that brought our ideas to life! Highly recommended!</p>
                                <footer><strong>Birthday</strong>, entry</footer>
                            </blockquote>
                        </div>
                        <div class="item">
                            <p>
                                <img src="assets/img/placeholders/avatars/profilepic.png" width="50" height="50"
                                     alt="Avatar" class="img-circle">
                            </p>
                            <blockquote class="no-symbol">
                                <p>An awesome team that brought our ideas to life! Highly recommended!</p>
                                <footer><strong>Profile</strong>, picture</footer>
                            </blockquote>
                        </div>
                        <div class="item">
                            <p>
                                <img src="assets/img/placeholders/avatars/gender.png" width="50" height="50"
                                     alt="Avatar" class="img-circle">
                            </p>
                            <blockquote class="no-symbol">
                                <p>An awesome team that brought our ideas to life! Highly recommended!</p>
                                <footer><strong>Gender</strong>, selection</footer>
                            </blockquote>
                        </div>
                        <div class="item">
                            <p>
                                <img src="assets/img/placeholders/avatars/education.png" width="50" height="50"
                                     alt="Avatar" alt="Avatar" class="img-circle">
                            </p>
                            <blockquote class="no-symbol">
                                <p>I have never imagined that our final product would look that good!</p>
                                <footer><strong>Edicational </strong>, lavel</footer>
                            </blockquote>
                        </div>
                        <div class="item">
                            <p>
                                <img src="assets/img/placeholders/avatars/mobile.png" width="50" height="50"
                                     alt="Avatar" alt="Avatar" class="img-circle">
                            </p>
                            <blockquote class="no-symbol">
                                <p>I have never imagined that our final product would look that good!</p>
                                <footer><strong>Mobile </strong>, model</footer>
                            </blockquote>
                        </div>
                        <div class="item">
                            <p>
                                <img src="assets/img/placeholders/avatars/hobby.png" width="50" height="50" alt="Avatar"
                                     alt="Avatar" class="img-circle">
                            </p>
                            <blockquote class="no-symbol">
                                <p>I have never imagined that our final product would look that good!</p>
                                <footer><strong>Hobby </strong>, selection</footer>
                            </blockquote>
                        </div>
                        <div class="item">
                            <p>
                                <img src="assets/img/placeholders/avatars/city.png" width="50" height="50" alt="Avatar"
                                     alt="Avatar" class="img-circle">
                            </p>
                            <blockquote class="no-symbol">
                                <p>I have never imagined that our final product would look that good!</p>
                                <footer><strong>Leaving </strong>, City</footer>
                            </blockquote>
                        </div>
                        <div class="item">
                            <p>
                                <img src="assets/img/placeholders/avatars/email.png" width="50" height="50" alt="Avatar"
                                     alt="Avatar" class="img-circle">
                            </p>
                            <blockquote class="no-symbol">
                                <p>I have never imagined that our final product would look that good!</p>
                                <footer><strong>Email </strong>, Subscribtion</footer>
                            </blockquote>
                        </div>
                        <div class="item">
                            <p>
                                <img src="assets/img/placeholders/avatars/details.png" width="50" height="50"
                                     alt="Avatar" alt="Avatar" class="img-circle">
                            </p>
                            <blockquote class="no-symbol">
                                <p>I have never imagined that our final product would look that good!</p>
                                <footer><strong>Summery </strong>, of Organization</footer>
                            </blockquote>
                        </div>
                        <div class="item">
                            <p>
                                <img src="assets/img/placeholders/avatars/term.png" width="50" height="50" alt="Avatar"
                                     alt="Avatar" class="img-circle">
                            </p>
                            <blockquote class="no-symbol">
                                <p>I have never imagined that our final product would look that good!</p>
                                <footer><strong>Term </strong>, & condition</footer>
                            </blockquote>
                        </div>
                    </div>
                    <!-- END Wrapper for slides -->
                </div>
            </div>
        </div>
    </section>
    <!-- END Testimonials -->

    <!-- Footer -->
    <footer class="site-footer site-section">
        <div class="container">
            <!-- Footer Links -->
            <div class="row">
                <div class="col-sm-7 col-md-4" align="left">
                    <ul class="footer-nav list-inline><span id=" year-copy
                    ">Copyrite</span> &copy; 2016 <a href="">BITM PHP 14</a></ul>
                </div>
                <div class="col-sm-7 col-md-4" align="center">
                    <ul class="footer-nav footer-nav-social list-inline">
                        <li><a href="http://facebook.com/kashohel"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="http://twitter.com/kashohel"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-dribbble"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-rss"></i></a></li>
                    </ul>
                </div>
                <div class="col-sm-7 col-md-4" align="right">
                    <ul class="footer-nav list-inline">
                        <li>Crafted with <i class="fa fa-heart text-danger"></i> by <a href="http://alam.foxof.org">Khorshed
                                Alam</a></li>
                    </ul>
                </div>
            </div>
            <!-- END Footer Links -->
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-up"></i></a>

<!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>!window.jQuery && document.write(decodeURI('%3Cscript src="assets/js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

<!-- Bootstrap.js, Jquery plugins and Custom JS code -->
<script src="assets/js/vendor/bootstrap.min.js"></script>
<script src="assets/js/plugin.js"></script>
<script src="assets/js/apps.js"></script>
</body>
</html>
