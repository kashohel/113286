<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>ProUI - Responsive Bootstrap Admin Template</title>
        

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="../assets/css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="../assets/css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="../assets/css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
        <script src="../assets/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
    </head>
    <!-- In the PHP version you can set the following options from inc/config file -->
    <!--
        Available body classes:

        'page-loading'      enables page preloader
    -->
    <body>
    <div id="page-content" style="min-height: 1018px;">
        <!-- Validation Header -->
        <div class="content-header">
            <div class="header-section">
                <h1>
                    <i class="gi gi-warning_sign"></i>Form Validation<br><small>Tools for easy form validation and user assist!</small>
                </h1>
            </div>
        </div>
        <ul class="breadcrumb breadcrumb-top">
            <li>Forms</li>
            <li><a href="">Validation</a></li>
        </ul>
        <!-- END Validation Header -->

        <div class="row">
            <div class="col-md-15">
                <!-- Form Validation Example Block -->
                <div class="block">
                    <!-- Form Validation Example Title -->
                    <div class="block-title">
                        <h2><strong>Form Validation</strong> Example</h2>
                    </div>
                    <!-- END Form Validation Example Title -->

                    <!-- Form Validation Example Content -->
                    <form id="form-validation" action="store_user.php" method="post" class="form-horizontal form-bordered" novalidate="novalidate">
                        <fieldset>
                            <legend><i class="fa fa-angle-right"></i> Vital Info</legend>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="firstname">First Name <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="firstname" name="firstname" class="form-control" placeholder="Your firstname..">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="lastname">Last Name <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" id="lastname" name="lastname" class="form-control" placeholder="Your lastname..">
                                        <span class="input-group-addon"><i class="gi gi-user"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="gender">Gender <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <select id="gender" name="gender" class="form-control">
                                        <option value="">Please select</option>
                                        <option value="Mail">Mail</option>
                                        <option value="Femail">Femail</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="Birthday">Birthday <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="date" id="Birthday" name="birthday" class="form-control" placeholder="Your date of birth..">
                                        <span class="input-group-addon"><i class="gi gi-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="education">Education <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <select id="education" name="education" class="form-control">
                                        <option value="">Please select</option>
                                        <option value="SSC">SSC or equal</option>
                                        <option value="HSC">HSC or equal</option>
                                        <option value="Graduate">Graduate</option>
                                        <option value="Postgraduate">Postgraduate</option>
                                        <option value="Other">Other Higher</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="hobbies">Select Hobby</label>
                                <div class="col-md-6">
                                    <select id="hobbies" name="hobbies[]" class="form-control" size="5" multiple="true">
                                        <option value="Reading">Reading</option>
                                        <option value="Coding">Coding</option>
                                        <option value="Eating">Eating</option>
                                        <option value="Fishing">Fishing</option>
                                        <option value="Drawing">Drawing</option>
                                        <option value="Sleeping">Sleeping</option>
                                        <option value="Singing">Singing</option>
                                        <option value="Hunting">Hunting</option>
                                        <option value="Dancing">Dancing</option>
                                        <option value="Playing">Playing</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="city">Select City <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <select id="city" name="city" class="form-control">
                                        <option value="">Please select</option>
                                        <option value="Dhaka">Dhaka</option>
                                        <option value="Rongpur">Rongpur</option>
                                        <option value="Rajshahi">Rajshahi</option>
                                        <option value="Sylhet">Sylhet</option>
                                        <option value="Chittagong">Chittagong</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="email">Email <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="email" id="email" name="email" class="form-control" placeholder="test@example.com">
                                        <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="phone">Phone Number <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="number" id="phone" name="phone" class="form-control" placeholder="+880">
                                        <span class="input-group-addon"><i class="gi gi-iphone"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="summery">Mini Bio <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <textarea id="summery" name="summery" rows="6" class="form-control" placeholder="Tell us your story.."></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label"><a href="../assets/#modal-terms" data-toggle="modal">Service Terms</a> <span class="text-danger">*</span></label>
                                <div class="col-md-6">
                                    <label class="switch switch-primary" for="terms">
                                        <input type="checkbox" id="terms" name="terms" value="1">
                                        <span data-toggle="tooltip" title="" data-original-title="I agree to the terms!"></span>
                                    </label>
                                </div>
                            </div>
                        </fieldset>
                        <div class="form-group form-actions">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> Submit</button>
                                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                            </div>
                        </div>
                    </form>
                    <!-- END Form Validation Example Content -->

                    <!-- Terms Modal -->
                    <div id="modal-terms" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 class="modal-title"><i class="gi gi-pen"></i> Service Terms</h3>
                                </div>
                                <div class="modal-body">
                                    <h4 class="sub-header">1.1 | General</h4>
                                    <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <h4 class="sub-header">1.2 | Account</h4>
                                    <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <h4 class="sub-header">1.3 | Service</h4>
                                    <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <h4 class="sub-header">1.4 | Payments</h4>
                                    <p>Donec lacinia venenatis metus at bibendum? In hac habitasse platea dictumst. Proin ac nibh rutrum lectus rhoncus eleifend. Sed porttitor pretium venenatis. Suspendisse potenti. Aliquam quis ligula elit. Aliquam at orci ac neque semper dictum. Sed tincidunt scelerisque ligula, et facilisis nulla hendrerit non. Suspendisse potenti. Pellentesque non accumsan orci. Praesent at lacinia dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Ok, I've read them!</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Terms Modal -->
                </div>
                <!-- END Validation Block -->
            </div>

        </div>
    </div>



    <footer class="clearfix">
        <div class="pull-right">
            Crafted with <i class="fa fa-heart text-danger"></i> by <a href="http://goo.gl/vNS3I" target="_blank">pixelcave</a>
        </div>
        <div class="pull-left">
            <span id="year-copy">2014-16</span> © <a href="http://goo.gl/TDOSuC" target="_blank">ProUI 2.1</a>
        </div>
    </footer>


        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="../assets/js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="../assets/js/vendor/bootstrap.min.js"></script>
        <script src="../assets/js/plugins.js"></script>
        <script src="../assets/js/app.js"></script>

        <!-- Load and execute javascript code used only in this page -->
        <script src="../assets/js/pages/formsValidation.js"></script>
        <script>$(function(){ FormsValidation.init(); });</script>
    </body>
</html>