<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>ProUI - Responsive Bootstrap Admin Template</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/plugins.css">
    <link rel="stylesheet" href="../assets/css/main.css">
    <link rel="stylesheet" href="../assets/css/themes.css">
    <script src="../assets/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
    <link rel="stylesheet" href="../assets/css/bootstraps.min.css">
    <link rel="stylesheet" href="../assets/css/pluginss.css">
    <link rel="stylesheet" href="../assets/css/mains.css">
    <link rel="stylesheet" href="../assets/css/themess.css">
    <script src="../assets/js/vendor/modernizr-2.7.1.js"></script>
</head>
<body>
<div id="page-container">
    <!-- Site Header -->
    <header>
        <div class="container">
            <!-- Site Logo -->
            <a href="index.html" class="site-logo">
                <i class="gi gi-dashboard"></i> Marge <strong>Project</strong>
            </a>
            <!-- Site Logo -->

            <!-- Site Navigation -->
            <nav>
                <!-- Menu Toggle -->
                <!-- Toggles menu on small screens -->
                <a href="javascript:void(0)" class="btn btn-default site-menu-toggle visible-xs visible-sm">
                    <i class="fa fa-bars"></i>
                </a>
                <!-- END Menu Toggle -->

                <!-- Main Menu -->
                <ul class="site-nav">
                    <!-- Toggles menu on small screens -->
                    <li class="visible-xs visible-sm">
                        <a href="javascript:void(0)" class="site-menu-toggle text-center">
                            <i class="fa fa-times"></i>
                        </a>
                    </li>
                    <!-- END Menu Toggle -->
                    <li>
                        <a href="contact.html"> Home</a>
                    </li>
                    <li>
                        <a href="about.html">About</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="site-nav-sub"><i
                                class="fa fa-angle-down site-nav-arrow"></i>Projects</a>
                        <ul>
                            <li>
                                <a href="index.html">Book</a>
                            </li>
                            <li>
                                <a href="index_alt.html">Birthday</a>
                            </li>
                            <li>
                                <a href="index_parallax.html">Profile Pic</a>
                            </li>
                            <li>
                                <a href="index_boxed.html">Gender</a>
                            </li>
                            <li>
                                <a href="index_boxed_alt.html">Education</a>
                            </li>
                            <li>
                                <a href="index_boxed_parallax.html">Mobile Model</a>
                            </li>
                            <li>
                                <a href="index_boxed_parallax.html">Hobby</a>
                            </li>
                            <li>
                                <a href="index_boxed_parallax.html">City</a>
                            </li>
                            <li>
                                <a href="Subscribe.php">Email Subs</a>
                            </li>
                            <li>
                                <a href="index_boxed_parallax.html">Summery</a>
                            </li>
                            <li>
                                <a href="index_boxed_parallax.html">Term and Condition</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="contact.html">Contact</a>
                    </li>

                </ul>
                <!-- END Main Menu -->
            </nav>
            <!-- END Site Navigation -->
        </div>
    </header>

</div>
<br/><br/><br/><br/>
<div id="page-content" style="min-height: 1018px;">
    <!-- Validation Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <div class="block-section">
                    <a href="Subscribe.php">
                        <button type="button" class="btn btn-alt btn-primary"> Create New</button>
                    </a>
                    <a href="Subscribers.php">
                        <button type="button" class="btn btn-alt btn-info">List of subscriber</button>
                    </a>
                    <a href="trashed_subs.php">
                        <button type="button" class="btn btn-alt btn-success">Deleted list</button>
                    </a>
                </div>
            </h1>
        </div>
    </div
        <!-- END Validation Header -->

    <div class="row">
        <div class="col-md-15">
            <!-- Form Validation Example Block -->
            <div class="block">
                <!-- Form Validation Example Title -->
                <div class="block-title">
                    <h2><strong>Subscribe</strong> to have all latest update in time and stay connect with us.</h2>
                </div>
                <!-- END Form Validation Example Title -->

                <!-- Form Validation Example Content -->
                <form id="form-validation" action="store_subs.php" method="post" class="form-horizontal form-bordered"
                      novalidate="novalidate">
                    <fieldset>
                        <br/><br/><br/><br/>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="email">Email <span
                                    class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="email" id="email" name="email" class="form-control"
                                           placeholder="test@example.com">
                                    <span class="input-group-addon"><i class="gi gi-envelope"></i></span>


                                </div>
                            </div>
                        </div>
                        <br/><br/><br/><br/>


                    </fieldset>
                    <div class="form-group form-actions" align="center">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i>
                                Subscribe Now
                            </button>
                        </div>
                    </div>
                </form>
                <!-- END Form Validation Example Content -->

                <!-- Terms Modal -->
                <div id="modal-terms" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 class="modal-title"><i class="gi gi-pen"></i> Service Terms</h3>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Ok, I've read
                                    them!
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Terms Modal -->
            </div>
            <!-- END Validation Block -->
        </div>

    </div>
</div>


<footer class="clearfix">
    <div class="pull-right">
        Crafted with <i class="fa fa-heart text-danger"></i> by <a href="http://goo.gl/vNS3I"
                                                                   target="_blank">pixelcave</a>
    </div>
    <div class="pull-left">
        <span id="year-copy">2014-16</span> © <a href="http://goo.gl/TDOSuC" target="_blank">ProUI 2.1</a>
    </div>
</footer>


<!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>!window.jQuery && document.write(decodeURI('%3Cscript src="../assets/js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

<!-- Bootstrap.js, Jquery plugins and Custom JS code -->
<script src="../assets/js/vendor/bootstrap.min.js"></script>
<script src="../assets/js/plugins.js"></script>
<script src="../assets/js/app.js"></script>

<!-- Load and execute javascript code used only in this page -->
<script src="../assets/js/pages/formsValidation.js"></script>
<script>$(function () {
        FormsValidation.init();
    });</script>
</body>
</html>