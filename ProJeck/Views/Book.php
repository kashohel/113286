<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../vendor/autoload.php';
use App\Manage;

$obj = new Manage();
$datas = $obj->books();
//print_r($datas);
//die();

?>
<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Book Title | List View</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/css/plugins.css">
    <link rel="stylesheet" href="../assets/css/main.css">
    <link rel="stylesheet" href="../assets/css/themes.css">
    <script src="../assets/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js"></script>
    <link rel="stylesheet" href="../assets/css/bootstraps.min.css">
    <link rel="stylesheet" href="../assets/css/pluginss.css">
    <link rel="stylesheet" href="../assets/css/mains.css">
    <link rel="stylesheet" href="../assets/css/themess.css">
    <script src="../assets/js/vendor/modernizr-2.7.1.js"></script>
</head>
<div id="page-container">
    <!-- Site Header -->
    <header>
        <div class="container">
            <!-- Site Logo -->
            <a href="../index.php" class="site-logo">
                <i class="gi gi-dashboard"></i> Marge <strong>Project</strong>
            </a>

            <nav>
                <a href="javascript:void(0)" class="btn btn-default site-menu-toggle visible-xs visible-sm">
                    <i class="fa fa-bars"></i>
                </a>

                <ul class="site-nav">
                    <!-- Toggles menu on small screens -->
                    <li class="visible-xs visible-sm">
                        <a href="javascript:void(0)" class="site-menu-toggle text-center">
                            <i class="fa fa-times"></i>
                        </a>
                    </li>
                    <!-- END Menu Toggle -->
                    <li>
                        <a href="../index.php"> Home</a>
                    </li>
                    <li>
                        <a href="../about.php">About</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" class="site-nav-sub"><i
                                class="fa fa-angle-down site-nav-arrow"></i>Projects</a>
                        <ul>
                            <li>
                                <a href="Book.php">Book</a>
                            </li>
                            <li>
                                <a href="Birthday.php">Birthday</a>
                            </li>
                            <li>
                                <a href="ProfilePic.php">Profile Pic</a>
                            </li>
                            <li>
                                <a href="Gender.php">Gender</a>
                            </li>
                            <li>
                                <a href="Education.php">Education</a>
                            </li>
                            <li>
                                <a href="Mobile.php">Mobile Model</a>
                            </li>
                            <li>
                                <a href="Hobby.php">Hobby</a>
                            </li>
                            <li>
                                <a href="City.php">City</a>
                            </li>
                            <li>
                                <a href="Subscribe.php">Email Subs</a>
                            </li>
                            <li>
                                <a href="Summery.php">Summery</a>
                            </li>
                            <li>
                                <a href="Term.php">Term and Condition</a>
                            </li>
                        </ul>
                    </li>

                    <li>
                        <a href="../contact.php">Contact</a>
                    </li>

                </ul>
                <!-- END Main Menu -->
            </nav>
            <!-- END Site Navigation -->
        </div>
    </header>

</div>
<br/><br/><br/><br/>
<body>
<div id="page-content" style="min-height: 986px;">
    <!-- Datatables Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                Main Manue
            </h1>
        </div>
    </div>

    <!-- END Datatables Header -->

    <!-- Datatables Content -->
    <div class="block full">
        <div class="block-title">
            <h2><strong>Datatables</strong> integration</h2>
        </div>
        <p><a href="https://datatables.net/" target="_blank">DataTables</a> is a plug-in for the Jquery Javascript
            library. It is a highly flexible tool, based upon the foundations of progressive enhancement, which will add
            advanced interaction controls to any HTML table. It is integrated with template's design and it offers many
            features such as on-the-fly filtering and variable length pagination.</p>

        <div class="table-responsive">
            <div id="example-datatable_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <div class="col-sm-6 col-xs-5">
                        <div class="dataTables_length" id="example-datatable_length"><label><select
                                    name="example-datatable_length" aria-controls="example-datatable"
                                    class="form-control">
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="-1">All</option>
                                </select></label></div>
                    </div>
                    <div class="col-sm-6 col-xs-7">
                        <div id="example-datatable_filter" class="dataTables_filter"><label>
                                <div class="input-group"><input type="search" class="form-control"
                                                                aria-controls="example-datatable"
                                                                placeholder="Search"><span class="input-group-addon"><i
                                            class="fa fa-search"></i></span></div>
                            </label></div>
                    </div>
                </div>
                <table id="example-datatable"
                       class="table table-vcenter table-condensed table-bordered dataTable no-footer" role="grid"
                       aria-describedby="example-datatable_info">
                    <thead>
                    <tr role="row">
                        <th class="text-center sorting_asc" tabindex="0" aria-controls="example-datatable" rowspan="1"
                            colspan="1" aria-sort="ascending" aria-label="ID: activate to sort column ascending"
                            style="width: 57px;">Serial
                        </th>
                        <th class="text-center sorting_disabled" rowspan="1" colspan="1" aria-label=""
                            style="width: 76px;"><i class="gi gi-user"></i></th>
                        <th class="sorting" tabindex="0" aria-controls="example-datatable" rowspan="1" colspan="1"
                            aria-label="Client: activate to sort column ascending" style="width: 114px;">Name
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="example-datatable" rowspan="1" colspan="1"
                            aria-label="Email: activate to sort column ascending" style="width: 219px;">Email
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="example-datatable" rowspan="1" colspan="1"
                            aria-label="Phone: activate to sort column ascending" style="width: 217px;">Phone
                        </th>
                        <th class="sorting" tabindex="0" aria-controls="example-datatable" rowspan="1" colspan="1"
                            aria-label="Phone: activate to sort column ascending" style="width: 217px;">Gender
                        </th>
                        <th class="text-center sorting_disabled" rowspan="1" colspan="1" aria-label="Actions"
                            style="width: 141px;">Actions
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    if (isset($datas) && !empty($datas)){
                    $serial = 0;
                    foreach ($datas as $data){
                    $serial++
                    ?>

                    <tr role="row" class="odd">
                        <td class="text-center sorting_1"><?php echo $serial ?></td>
                        <td class="text-center"><img src="<?php if (isset($data['profilepic']) && !empty($data['profilepic'])){echo "../assets/img/users/".$data['profilepic'];}else{echo "../assets/img/users/user1.jpg";} ?>" width="50" height="50"
                                                     alt="avatar" class="img-circle"></td>
                        <td><a href="javascript:void(0)"><?php echo $data['first_name'] ." ". $data['last_name']; ?></a></td>
                        <td><?php echo $data['email']; ?></td>
                        <td><?php echo $data['phone']; ?></td>
                        <td><?php echo $data['gender']; ?></td>
                        <td class="text-center">
                            <div class="btn-group">
                                <a href="show_user.php?id=<?php echo $data['id'] ?>" data-toggle="tooltip" title=""
                                   class="btn btn-xs btn-default" data-original-title="View"><i
                                        class="hi hi-eye-open"></i></a>
                                <a href="edit_user.php?id=<?php echo $data['id'] ?>" data-toggle="tooltip" title=""
                                   class="btn btn-xs btn-default" data-original-title="Edit"><i
                                        class="fa fa-pencil"></i></a>
                                <a href="trash_user.php?id=<?php echo $data['id'] ?>" data-toggle="tooltip" title=""
                                   class="btn btn-xs btn-danger" data-original-title="Delete"><i
                                        class="fa fa-times"></i></a>
                            </div>
                        </td>

                        <?php } }else{ echo"nothing found"; }?>

                    </tbody>
                </table>
                <div class="row">
                    <div class="col-sm-5 hidden-xs">
                        <div class="dataTables_info" id="example-datatable_info" role="status" aria-live="polite">
                            <strong>1</strong>-<strong>10</strong> of <strong>60</strong></div>
                    </div>
                    <div class="col-sm-7 col-xs-12 clearfix">
                        <div class="dataTables_paginate paging_bootstrap" id="example-datatable_paginate">
                            <ul class="pagination pagination-sm remove-margin">
                                <li class="prev disabled"><a href="javascript:void(0)"><i
                                            class="fa fa-chevron-left"></i> </a></li>
                                <li class="active"><a href="javascript:void(0)">1</a></li>
                                <li><a href="javascript:void(0)">2</a></li>
                                <li><a href="javascript:void(0)">3</a></li>
                                <li><a href="javascript:void(0)">4</a></li>
                                <li><a href="javascript:void(0)">5</a></li>
                                <li class="next"><a href="javascript:void(0)"> <i class="fa fa-chevron-right"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Datatables Content -->
    <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

    <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
    <script src="../assets/js/vendor/bootstrap.min.js"></script>
    <script src="../assets/js/plugins.js"></script>
    <script src="../assets/js/app.js"></script>

    <!-- Load and execute javascript code used only in this page -->
    <script src="../assets/js/pages/tablesDatatables.js"></script>
    <script>$(function () {
            TablesDatatables.init();
        });</script>
</div>
</body>
</html>