<?php
session_start();
if (isset($_SESSION['User'])) {

} else {
    header('location:login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Neon Admin Panel"/>
    <meta name="author" content=""/>

    <title>Neon | Advanced Plugins</title>

    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script>$.noConflict();</script>

    <!--[if lt IE 9]>
    <script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body class="page-body" data-url="http://neon.dev">

<div class="page-container horizontal-menu">
    <!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

    <header class="navbar navbar-fixed-top"><!-- set fixed position by adding class "navbar-fixed-top" -->

        <div class="navbar-inner">

            <!-- logo -->
            <div class="navbar-brand">
                <a href="index.html">
                    <img src="assets/images/logo@2x.png" width="88" alt=""/>
                </a>
            </div>


            <!-- main menu -->

            <ul class="navbar-nav">
                <li>
                    <a href="deshboard.php">
                        <i class="entypo-gauge"></i>
                        <span class="title">Dashboard</span>
                    </a>
                </li>
                <li class="opened active">
                    <a>
                        <i class="entypo-layout"></i>
                        <span class="title">Contacts</span>
                    </a>
                    <ul>
                        <li>
                            <a href="contact_create.php">
                                <span class="title">Create New</span>
                            </a>
                        </li>
                        <li>
                            <a href="contact_list.php">
                                <span class="title">Contact List</span>
                            </a>
                        </li>
                        <li>
                            <a href="favourite.php">
                                <span class="title">Favourit</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a>
                        <i class="entypo-newspaper"></i>
                        <span class="title">Group</span>
                    </a>
                    <ul>
                        <li>
                            <a href="group_create.php">
                                <span class="title">Create New</span>
                            </a>
                        </li>
                        <li>
                            <a href="group_list.php">
                                <span class="title">Group List</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a>
                        <i class="entypo-bag"></i>
                        <span class="title">Help</span>
                        <span class="badge badge-secondary">!</span>
                    </a>
                    <ul>
                        <li>
                            <a>
                                <span class="title">Document</span>
                                <span class="badge badge-success">3</span>
                            </a>
                            <ul>
                                <li>
                                    <a href="help_doc.php">
                                        <span class="title">Online</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="title">PDF</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="title">Video</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="support.php">
                                <span class="title">Support</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>


            <!-- notifications and other links -->
            <ul class="nav navbar-right pull-right">

                <!-- raw links -->
                <li class="dropdown">
                <li>
                    <a href="account.php"><i class="entypo-globe"></i>My Accaunt</a>
                </li>
                </li>

                <li class="sep"></li>

                <li>
                    <a href="actions/logout.php">
                        Log Out <i class="entypo-logout right"></i>
                    </a>
                </li>


                <!-- mobile only -->
                <li class="visible-xs">

                    <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                    <div class="horizontal-mobile-menu visible-xs">
                        <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                            <i class="entypo-menu"></i>
                        </a>
                    </div>

                </li>

            </ul>

        </div>

    </header>

    <div class="main-content">


        <ol class="breadcrumb bc-3">
            <li>
                <a href="deshboard.php"><i class="fa-home"></i>Home</a>
            </li>
            <li>

                <a href="contact_list.php">Contacts</a>
            </li>
            <li class="active">

                <strong>Create new contact</strong>
            </li>
        </ol>

        <h2 align="center">Save new contact</h2>
        <br/>

        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-primary" data-collapsed="0">

                    <div class="panel-heading">
                        <div class="panel-title">
                            Contact Form
                        </div>
                    </div>

                    <div class="panel-body">

                        <form role="form"  action="actions/store.php" method="post" enctype="multipart/form-data" class="form-horizontal form-groups-bordered">

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">First name</label>
                                <div class="col-sm-5">
                                    <div class="input-group minimal">
                                        <span class="input-group-addon"><i class="entypo-user"></i></span>
                                        <input type="text" name="first_name" class="form-control" id="field-1" placeholder="First Part of name">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Last name</label>
                                <div class="col-sm-5">
                                    <div class="input-group minimal">
                                        <span class="input-group-addon"><i class="entypo-user"></i></span>
                                    <input type="text" name="last_name" class="form-control" id="field-1" placeholder="Another part of name">
                                </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Mobile Number</label>
                                <div class="col-sm-5">
                                    <div class="input-group minimal">
                                        <span class="input-group-addon"><i class="entypo-mobile"></i></span>
                                    <input type="text" name="mobile" class="form-control" id="field-1" placeholder="+8801** *******">
                                </div></div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Phone number</label>
                                <div class="col-sm-5">
                                    <div class="input-group minimal">
                                        <span class="input-group-addon"><i class="entypo-phone"></i></span>
                                    <input type="text" name="phone" class="form-control" id="field-1" placeholder="02 *********">
                                </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">E-mail</label>
                                <div class="col-sm-5">
                                    <div class="input-group minimal">
                                        <span class="input-group-addon"><i class="entypo-mail"></i></span>
                                        <input type="text" name="email" class="form-control" placeholder="user@example.com">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Gender</label>

                                <div class="col-sm-6">

                                    <ul class="icheck-list">
                                        <li>
                                            <input class="icheck-2" type="radio" id="minimal-radio-1-2"
                                                   name="gender" value="mail">
                                            <label for="minimal-radio-1-2">Mail</label>
                                        </li>
                                        <li>
                                            <input class="icheck-2" type="radio" id="minimal-radio-1-2"
                                                   name="gender" value="femail">
                                            <label for="minimal-radio-1-2">Femail</label>
                                        </li>

                                    </ul>

                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Select Birthday</label>
                                <div class="col-sm-5">
                                    <div class="input-group minimal">
                                        <span class="input-group-addon"><i class="entypo-calendar"></i></span>
                                    <input type="text" name="birthday" class="form-control datepicker" data-start-view="2">
                                </div></div>
                            </div>

                            <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-5">
                                    <div class="input-group minimal">
                                        <span class="input-group-addon"><i class="entypo-address"></i></span>
                                    <input type="text" name="address" class="form-control" id="field-1" placeholder="Home location">
                                </div></div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Contact Group</label>
                                <div class="col-sm-6">

                                    <ul class="icheck-list">
                                        <li>
                                            <input tabindex="5" type="checkbox" name="group[]" class="icheck-2"
                                                   id="minimal-checkbox-1-2">
                                            <label for="minimal-checkbox-1-2">Checkbox 1</label>
                                        </li>
                                        <li>
                                            <input tabindex="5" type="checkbox" name="group[]" class="icheck-2"
                                                   id="minimal-checkbox-1-2">
                                            <label for="minimal-checkbox-1-2">Checkbox 1</label>
                                        </li>
                                        <li>
                                            <input tabindex="5" type="checkbox" name="group[]" class="icheck-2"
                                                   id="minimal-checkbox-1-2">
                                            <label for="minimal-checkbox-1-2">Checkbox 1</label>
                                        </li>
                                        <li>
                                            <input tabindex="5" type="checkbox" name="group[]" class="icheck-2"
                                                   id="minimal-checkbox-1-2">
                                            <label for="minimal-checkbox-1-2">Checkbox 1</label>
                                        </li>
                                        <li>
                                            <input tabindex="5" type="checkbox" name="group[]" class="icheck-2"
                                                   id="minimal-checkbox-1-2">
                                            <label for="minimal-checkbox-1-2">Checkbox 1</label>
                                        </li>
                                    </ul>

                                </div>

                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Image Upload</label>

                                <div class="col-sm-5">

                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"
                                             data-trigger="fileinput">
                                            <img src="http://placehold.it/200x150" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 200px; max-height: 150px"></div>
                                        <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="..." accept="image/*">
											</span>
                                            <a href="#" class="btn btn-orange fileinput-exists"
                                               data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-10" align="center">
                                    <button type="submit" class="btn btn-success">Save Data</button>
                                    <span>               </span>
                                    <button type="reset" class="btn btn-orange">Reset Form</button>
                                </div>
                            </div>

                        </form>

                    </div>

                </div>

            </div>
        </div>

        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $('input.icheck').iCheck({
                    checkboxClass: 'icheckbox_minimal',
                    radioClass: 'iradio_minimal'
                });

                $('input.icheck-2').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                });
            });


            jQuery(document).ready(function ($) {
                var icheck_skins = $(".icheck-skins a");

                icheck_skins.click(function (ev) {
                    ev.preventDefault();

                    icheck_skins.removeClass('current');
                    $(this).addClass('current');

                    updateiCheckSkinandStyle();
                });

                $("#icheck-style").change(updateiCheckSkinandStyle);
            });

            function updateiCheckSkinandStyle() {
                var skin = $(".icheck-skins a.current").data('color-class'),
                    style = $("#icheck-style").val();

                var cb_class = 'icheckbox_' + style + (skin.length ? ("-" + skin) : ''),
                    rd_class = 'iradio_' + style + (skin.length ? ("-" + skin) : '');

                if (style == 'futurico' || style == 'polaris') {
                    cb_class = cb_class.replace('-' + skin, '');
                    rd_class = rd_class.replace('-' + skin, '');
                }

                $('input.icheck-2').iCheck('destroy');
                $('input.icheck-2').iCheck({
                    checkboxClass: cb_class,
                    radioClass: rd_class
                });
            }
        </script>


        <!-- Footer -->
        <footer class="main">

            &copy; 2014 <strong>Neon</strong> Admin Theme by <a href="http://laborator.co" target="_blank">Laborator</a>

        </footer>
    </div>


    <div id="chat" class="fixed" data-current-user="Art Ramadani" data-order-by-status="1" data-max-chat-history="25">

        <div class="chat-inner">


            <h2 class="chat-header">
                <a href="#" class="chat-close"><i class="entypo-cancel"></i></a>

                <i class="entypo-users"></i>
                Chat
                <span class="badge badge-success is-hidden">0</span>
            </h2>


            <div class="chat-group" id="group-1">
                <strong>Favorites</strong>

                <a href="#" id="sample-user-123" data-conversation-history="#sample_history"><span
                        class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
                <a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
                <a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
                <a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
                <a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
            </div>


            <div class="chat-group" id="group-2">
                <strong>Work</strong>

                <a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
                <a href="#" data-conversation-history="#sample_history_2"><span class="user-status is-offline"></span>
                    <em>Daniel A. Pena</em></a>
                <a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
            </div>


            <div class="chat-group" id="group-3">
                <strong>Social</strong>

                <a href="#"><span class="user-status is-busy"></span> <em>Velma G. Pearson</em></a>
                <a href="#"><span class="user-status is-offline"></span> <em>Margaret R. Dedmon</em></a>
                <a href="#"><span class="user-status is-online"></span> <em>Kathleen M. Canales</em></a>
                <a href="#"><span class="user-status is-offline"></span> <em>Tracy J. Rodriguez</em></a>
            </div>

        </div>

        <!-- conversation template -->
        <div class="chat-conversation">

            <div class="conversation-header">
                <a href="#" class="conversation-close"><i class="entypo-cancel"></i></a>

                <span class="user-status"></span>
                <span class="display-name"></span>
                <small></small>
            </div>

            <ul class="conversation-body">
            </ul>

            <div class="chat-textarea">
                <textarea class="form-control autogrow" placeholder="Type your message"></textarea>
            </div>

        </div>

    </div>


    <!-- Chat Histories -->
    <ul class="chat-history" id="sample_history">
        <li>
            <span class="user">Art Ramadani</span>
            <p>Are you here?</p>
            <span class="time">09:00</span>
        </li>

        <li class="opponent">
            <span class="user">Catherine J. Watkins</span>
            <p>This message is pre-queued.</p>
            <span class="time">09:25</span>
        </li>

        <li class="opponent">
            <span class="user">Catherine J. Watkins</span>
            <p>Whohoo!</p>
            <span class="time">09:26</span>
        </li>

        <li class="opponent unread">
            <span class="user">Catherine J. Watkins</span>
            <p>Do you like it?</p>
            <span class="time">09:27</span>
        </li>
    </ul>


    <!-- Chat Histories -->
    <ul class="chat-history" id="sample_history_2">
        <li class="opponent unread">
            <span class="user">Daniel A. Pena</span>
            <p>I am going out.</p>
            <span class="time">08:21</span>
        </li>

        <li class="opponent unread">
            <span class="user">Daniel A. Pena</span>
            <p>Call me when you see this message.</p>
            <span class="time">08:27</span>
        </li>
    </ul>


</div>


<!-- Imported styles on this page -->
<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="assets/js/select2/select2.css">
<link rel="stylesheet" href="assets/js/selectboxit/jquery.selectBoxIt.css">
<link rel="stylesheet" href="assets/js/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="assets/js/icheck/skins/minimal/_all.css">
<link rel="stylesheet" href="assets/js/icheck/skins/square/_all.css">
<link rel="stylesheet" href="assets/js/icheck/skins/flat/_all.css">
<link rel="stylesheet" href="assets/js/icheck/skins/futurico/futurico.css">
<link rel="stylesheet" href="assets/js/icheck/skins/polaris/polaris.css">

<!-- Bottom scripts (common) -->
<script src="assets/js/gsap/main-gsap.js"></script>
<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/joinable.js"></script>
<script src="assets/js/resizeable.js"></script>
<script src="assets/js/neon-api.js"></script>


<!-- Imported scripts on this page -->
<script src="assets/js/select2/select2.min.js"></script>
<script src="assets/js/bootstrap-tagsinput.min.js"></script>
<script src="assets/js/typeahead.min.js"></script>
<script src="assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script src="assets/js/bootstrap-timepicker.min.js"></script>
<script src="assets/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/js/daterangepicker/moment.min.js"></script>
<script src="assets/js/daterangepicker/daterangepicker.js"></script>
<script src="assets/js/jquery.multi-select.js"></script>
<script src="assets/js/icheck/icheck.min.js"></script>


<!-- JavaScripts initializations and stuff -->
<script src="assets/js/neon-custom.js"></script>


<!-- Imported scripts on this page -->
<script src="assets/js/fileinput.js"></script>
<script src="assets/js/dropzone/dropzone.js"></script>

<!--@@@@@@@@@@@@@@@@@@@@@@-->
</body>
</html>